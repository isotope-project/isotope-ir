/*!
A simple AST for the isotope intermediate representation
*/
use smallvec::{smallvec, SmallVec};
use smol_str::SmolStr;
use std::borrow::Borrow;

pub mod decl;
pub mod expr;
pub mod primitive;
pub mod source;
pub mod typing;

pub use decl::*;
pub use expr::*;
pub use primitive::*;
pub use source::*;
pub use typing::*;

/// An identifier
pub type Ident = SmolStr;

/// A path to a definition
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Path(pub SmallVec<[SmolStr; 1]>);

impl Path {
    /// Create a path from an identifier
    ///
    /// # Examples
    /// ```rust
    /// # use isotope_core_parser::ast::*;
    /// assert_eq!(Path::ident("x"), "x");
    /// ```
    #[inline]
    pub fn ident(id: impl Into<SmolStr>) -> Path {
        Path(smallvec![id.into()])
    }
}

impl<T: Borrow<str>> PartialEq<T> for Path {
    #[inline]
    fn eq(&self, other: &T) -> bool {
        self.0.len() == 1 && self.0[0] == other.borrow()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn path_ident_construction() {
        for ident in [
            "x",
            "not::really::an::ident",
            "#really::not=really:an;[ident})",
        ] {
            assert_eq!(Path::ident(ident), ident)
        }
    }

    #[test]
    fn eq_mod_sanity() {
        let x_b64 = Names::Annot(Box::new((
            Spanned::new(Names::Ident("x".into()), 20..21),
            Spanned::new(Type::Bitvector(64), 22..26),
        )));
        assert!(!x_b64.eq_mod(&Names::Ident("x".into())));
        let sig_entry_empty = Signature {
            name: Spanned::new("entry".into(), 0..5),
            args: None,
        };
        let sig_entry_b64 = Signature {
            name: Spanned::new("entry".into(), 0..5),
            args: Some(Spanned::new(x_b64.clone(), 20..26)),
        };
        assert!(!Expr::ident("x").eq_mod(&Expr::int(5)));
        assert!(!sig_entry_empty.eq_mod(&sig_entry_b64));
        let x_body = Body {
            insts: vec![],
            term: Spanned::new(Label::Return(Expr::ident("x")), 40..41),
        };
        let anon_block = BasicBlock {
            sig: None,
            body: x_body.clone(),
        };
        let entry_block = BasicBlock {
            sig: Some(Spanned::new(sig_entry_b64.clone(), 0..27)),
            body: x_body.clone(),
        };
        assert!(!anon_block.eq_mod(&entry_block));
        let entry_block_ = BasicBlock {
            sig: Some(Spanned::new(sig_entry_b64, 0..28)),
            body: x_body,
        };
        assert_ne!(entry_block, entry_block_);
        assert!(entry_block.eq_mod(&entry_block_));
        assert!(!Type::Bitvector(32).eq_mod(&Type::Path(Path::ident("A"))))
    }

    #[test]
    fn eq_mod_regression() {
        let left_args = Names::Annot(Box::new((
            Spanned::new(Names::Ident("d".into()), 9..10),
            Spanned::new(Type::Bitvector(64), 12..16),
        )));

        let right_args = Names::Annot(Box::new((
            Spanned::new(Names::Ident("d".into()), 9..10),
            Spanned::new(Type::Bitvector(64), 13..17),
        )));

        assert!(left_args.eq_mod(&right_args));

        let left_sig = Signature {
            name: Spanned::new("entry".into(), 41..46),
            args: None,
        };

        let right_sig = Signature {
            name: Spanned::new("entry".into(), 30..35),
            args: None,
        };

        assert!(left_sig.eq_mod(&right_sig));

        let left_body = Label::Jump(Jump {
            target: Spanned::new("entry".into(), 70..75),
            args: vec![],
        });

        let right_body = Label::Jump(Jump {
            target: Spanned::new("entry".into(), 47..52),
            args: vec![],
        });

        assert!(left_body.eq_mod(&right_body));

        let left_def = Expr::Theta(Theta {
            blocks: vec![BasicBlock {
                sig: Some(Spanned::new(left_sig, 40..47)),
                body: Body {
                    insts: vec![],
                    term: Spanned::new(left_body, 64..75),
                },
            }],
        });

        let right_def = Expr::Theta(Theta {
            blocks: vec![BasicBlock {
                sig: Some(Spanned::new(right_sig, 29..36)),
                body: Body {
                    insts: vec![],
                    term: Spanned::new(right_body, 39..52),
                },
            }],
        });

        assert!(left_def.eq_mod(&right_def));

        let left = Decl::Function(Function {
            name: Spanned::new("loop".into(), 4..8),
            args: Spanned::new(left_args, 8..17),
            ret: Spanned::new(Type::Bitvector(64), 21..25),
            def: Spanned::new(left_def, 26..89),
        });

        let right = Decl::Function(Function {
            name: Spanned::new("loop".into(), 4..8),
            args: Spanned::new(right_args, 8..18),
            ret: Spanned::new(Type::Bitvector(64), 22..26),
            def: Spanned::new(right_def, 27..57),
        });

        assert!(left.eq_mod(&right));
    }
}
