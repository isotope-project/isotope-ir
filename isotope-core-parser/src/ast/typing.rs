/*!
An AST for `isotope-core` types
*/
use super::*;

/// An `isotope-core` type
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Type {
    /// A named type
    Path(Path),
    /// A union of types
    Union(Vec<Spanned<Type>>),
    /// A product of types
    Product(Vec<Spanned<Type>>),
    /// An array type, consisting of an array with `n` elements
    Array(Box<Spanned<Type>>, u32),
    /// Bitvectors of length `n`
    Bitvector(u32),
}

impl Type {
    /// Compare modulo span
    #[inline]
    pub fn eq_mod(&self, other: &Type) -> bool {
        match (self, other) {
            (Type::Path(this), Type::Path(other)) => this == other,
            (Type::Union(this), Type::Union(other)) => {
                this.len() == other.len()
                    && this
                        .iter()
                        .zip(other.iter())
                        .all(|(this, other)| this.inner.eq_mod(&other.inner))
            }
            (Type::Product(this), Type::Product(other)) => {
                this.len() == other.len()
                    && this
                        .iter()
                        .zip(other.iter())
                        .all(|(this, other)| this.inner.eq_mod(&other.inner))
            }
            (Type::Array(this, this_l), Type::Array(other, other_l)) => {
                this_l == other_l && this.inner.eq_mod(&other.inner)
            }
            (Type::Bitvector(this), Type::Bitvector(other)) => this == other,
            _ => false,
        }
    }
}
