/*!
Utilities for dealing with source information
*/
use std::fmt::Debug;
use std::ops::Range;

/// A spanned node
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, PartialOrd, Ord)]
pub struct Spanned<T, S = Bytes> {
    /// The spanned node
    pub inner: T,
    /// The node's span
    pub span: S,
}

/// A byte span
#[derive(Copy, Clone, Eq, PartialEq, Hash, PartialOrd, Ord)]
pub struct Bytes(pub usize, pub usize);

impl From<Range<usize>> for Bytes {
    #[inline]
    fn from(r: Range<usize>) -> Self {
        Bytes(r.start, r.end)
    }
}

impl Debug for Bytes {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}..{}", self.0, self.1)
    }
}

impl<T> Spanned<T> {
    /// Create a new span with a given byte range
    #[inline]
    pub fn new(inner: T, span: impl Into<Bytes>) -> Spanned<T> {
        Spanned {
            inner,
            span: span.into(),
        }
    }
}

impl<T, S> Spanned<T, S> {
    /// Map this spanned object to another
    ///
    /// # Examples
    /// ```rust
    /// # use isotope_core_parser::ast::source::*;
    /// assert_eq!(Spanned::new(3, 0..1).map(Some), Spanned::new(Some(3), 0..1));
    /// ```
    #[inline]
    pub fn map<U>(self, f: impl FnOnce(T) -> U) -> Spanned<U, S> {
        Spanned {
            inner: f(self.inner),
            span: self.span,
        }
    }

    /// Map this spanned object's span to another
    ///
    /// # Examples
    /// ```rust
    /// # use isotope_core_parser::ast::source::*;
    /// assert_eq!(
    ///     Spanned::new(3, 0..1).map_span(Some),
    ///     Spanned {
    ///         inner: 3,
    ///         span: Some(Bytes(0, 1))
    ///     }
    ///);
    /// ```
    #[inline]
    pub fn map_span<U>(self, f: impl FnOnce(S) -> U) -> Spanned<T, U> {
        Spanned {
            inner: self.inner,
            span: f(self.span),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn map_sanity() {
        assert_eq!(Spanned::new(3, 0..1).map(Some), Spanned::new(Some(3), 0..1));
        assert_eq!(
            Spanned::new(3, 0..1).map_span(Some),
            Spanned {
                inner: 3,
                span: Some(Bytes(0, 1))
            }
        );
    }

    #[test]
    fn debug_bytes() {
        assert_eq!(format!("{:?}", Bytes(56, 93)), "56..93");
    }
}
