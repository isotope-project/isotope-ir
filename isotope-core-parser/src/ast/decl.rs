/*!
`isotope-core` declarations
*/
use super::*;

/// A declaration
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Decl {
    /// A constant definition
    Constant(Constant),
    /// A type definition
    Typedef(Typedef),
    /// A function definition
    Function(Function),
}

/// A constant definition
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Constant {
    /// The defined constants
    pub names: Spanned<Names>,
    /// The constant's value
    pub def: Spanned<Expr>,
}

/// A type definition
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Typedef {
    /// The name of the type
    pub name: Spanned<Ident>,
    /// The definition of the type
    pub def: Spanned<Type>,
}

/// A function definition
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Function {
    /// This function's name
    pub name: Spanned<Ident>,
    /// This function's arguments
    pub args: Spanned<Names>,
    /// This function's return type
    pub ret: Spanned<Type>,
    /// This function's definition
    pub def: Spanned<Expr>,
}

impl Decl {
    /// Compare modulo span
    #[inline]
    pub fn eq_mod(&self, other: &Decl) -> bool {
        match (self, other) {
            (Decl::Constant(this), Decl::Constant(other)) => this.eq_mod(other),
            (Decl::Typedef(this), Decl::Typedef(other)) => this.eq_mod(other),
            (Decl::Function(this), Decl::Function(other)) => this.eq_mod(other),
            _ => false,
        }
    }
}

impl Constant {
    /// Compare modulo span
    #[inline]
    pub fn eq_mod(&self, other: &Constant) -> bool {
        self.names.inner.eq_mod(&other.names.inner) && self.def.inner.eq_mod(&other.def.inner)
    }
}

impl Typedef {
    /// Compare modulo span
    #[inline]
    pub fn eq_mod(&self, other: &Typedef) -> bool {
        self.name.inner == other.name.inner && self.def.inner.eq_mod(&other.def.inner)
    }
}

impl Function {
    /// Compare modulo span
    #[inline]
    pub fn eq_mod(&self, other: &Function) -> bool {
        self.name.inner == other.name.inner
            && self.args.inner.eq_mod(&other.args.inner)
            && self.ret.inner.eq_mod(&other.ret.inner)
            && self.def.inner.eq_mod(&other.def.inner)
    }
}
