/*!
`isotope-core` intermediate representation expressions
*/
use ibig::IBig;

use super::*;

/// An expression
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Expr {
    // S-expressions
    /// An identifier
    Path(Path),
    /// An application
    App(Vec<Spanned<Expr>>),

    // Control flow
    /// A theta node
    Theta(Theta),
    /// An infinite loop: may also raise a "nontermination" exception, which is implementation-defined
    Abort,
    /// An undefined value
    Undef,

    // Basic literals
    /// A tuple literal
    Tuple(Vec<Spanned<Expr>>),
    /// An array literal
    Array(Vec<Spanned<Expr>>),

    // Primitive literals
    /// A bitvector literal
    Bitvector(Bitvector),

    // Errors
    /// A general error
    Error(SmolStr),
}

impl Expr {
    /// Construct a scope expression from a vector of assignments and a result value
    #[inline]
    pub fn scope(defs: Vec<Spanned<Assign>>, result: Spanned<Expr>) -> Expr {
        Expr::Theta(Theta::scope(defs, result))
    }
}

/// A switch statement
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Switch {
    /// The discriminant of this switch statement
    pub disc: Box<Spanned<Expr>>,
    /// The branches of this switch statement
    pub branches: Vec<Spanned<Branch>>,
}

/// A (generalized) theta node, representing control flow through a CFG
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Theta {
    /// The basic blocks making up the theta node's CFG
    ///
    /// The first block is the entry block
    pub blocks: Vec<BasicBlock>,
}

impl Theta {
    /// Construct a scope expression from a vector of assignments and a result value
    #[inline]
    pub fn scope(defs: Vec<Spanned<Assign>>, result: Spanned<Expr>) -> Theta {
        Theta {
            blocks: vec![BasicBlock::scope(defs, result)],
        }
    }
}

/// A basic block
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct BasicBlock {
    /// This block's signature
    pub sig: Option<Spanned<Signature>>,
    /// This block's body
    pub body: Body,
}

impl BasicBlock {
    /// Construct a scope basic block from a vector of assignments and a result value
    #[inline]
    pub fn scope(defs: Vec<Spanned<Assign>>, result: Spanned<Expr>) -> BasicBlock {
        BasicBlock {
            sig: None,
            body: Body::scope(defs, result),
        }
    }
}

/// A basic block's signature
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Signature {
    /// This block's name
    pub name: Spanned<Ident>,
    /// The arguments of this basic block
    pub args: Option<Spanned<Names>>,
}

/// The body of a basic block
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Body {
    /// The named instructions in this block
    pub insts: Vec<Spanned<Assign>>,
    /// This block's terminator
    pub term: Spanned<Label>,
}

impl Body {
    /// Construct a scope block from a vector of assignments and a result value
    #[inline]
    pub fn scope(defs: Vec<Spanned<Assign>>, result: Spanned<Expr>) -> Body {
        Body {
            insts: defs,
            term: result.map(Label::Return),
        }
    }
}

/// A label
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Label {
    /// A return of a value
    Return(Expr),
    /// A switch statement
    Switch(Switch),
    /// A jump to a basic block
    Jump(Jump),
}

/// A jump to a basic block
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Jump {
    /// The target basic block
    pub target: Spanned<Ident>,
    /// The arguments
    pub args: Vec<Spanned<Expr>>,
}

impl Switch {
    /// Compare modulo span
    #[inline]
    pub fn eq_mod(&self, other: &Switch) -> bool {
        self.branches.len() == other.branches.len()
            && self.disc.inner.eq_mod(&other.disc.inner)
            && self
                .branches
                .iter()
                .zip(other.branches.iter())
                .all(|(this, other)| this.inner.eq_mod(&other.inner))
    }
}

/// A branch of a switch statement
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Branch(pub Spanned<Pattern>, pub Spanned<Body>);

/// A pattern to match
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Pattern {
    /// A bitvector pattern
    Bitvector(Bitvector),
    /// A wildcard pattern
    Wildcard,
}

/// An assignment in a basic block
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Assign(pub Spanned<Names>, pub Spanned<Expr>);

/// An assignment pattern
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Names {
    /// A named variable
    Ident(Ident),
    /// A product destructure
    Product(Vec<Spanned<Names>>),
    /// An annotated assignment pattern
    Annot(Box<(Spanned<Names>, Spanned<Type>)>),
}

impl Expr {
    /// Create a path from an identifier
    ///
    /// # Examples
    /// ```rust
    /// # use isotope_core_parser::ast::*;
    /// assert_eq!(Expr::ident("x"), Path::ident("x"));
    /// ```
    #[inline]
    pub fn ident(id: impl Into<SmolStr>) -> Expr {
        Expr::Path(Path::ident(id))
    }

    /// Create a (decimal) integer expression
    #[inline]
    pub fn int(value: impl Into<IBig>) -> Expr {
        Expr::Bitvector(Bitvector::int(value))
    }
}

impl Expr {
    /// Compare modulo span
    #[inline]
    pub fn eq_mod(&self, other: &Expr) -> bool {
        match (self, other) {
            (Expr::Path(this), Expr::Path(other)) => this == other,
            (Expr::App(this), Expr::App(other)) => {
                this.len() == other.len()
                    && this
                        .iter()
                        .zip(other.iter())
                        .all(|(this, other)| this.inner.eq_mod(&other.inner))
            }
            (Expr::Theta(this), Expr::Theta(other)) => this.eq_mod(other),
            (Expr::Abort, Expr::Abort) => true,
            (Expr::Undef, Expr::Undef) => true,
            (Expr::Tuple(this), Expr::Tuple(other)) => {
                this.len() == other.len()
                    && this
                        .iter()
                        .zip(other.iter())
                        .all(|(this, other)| this.inner.eq_mod(&other.inner))
            }
            (Expr::Array(this), Expr::Array(other)) => {
                this.len() == other.len()
                    && this
                        .iter()
                        .zip(other.iter())
                        .all(|(this, other)| this.inner.eq_mod(&other.inner))
            }
            (Expr::Bitvector(this), Expr::Bitvector(other)) => this == other,
            _ => false,
        }
    }
}

impl Theta {
    /// Compare modulo span
    #[inline]
    pub fn eq_mod(&self, other: &Theta) -> bool {
        self.blocks.len() == other.blocks.len()
            && self
                .blocks
                .iter()
                .zip(other.blocks.iter())
                .all(|(this, other)| this.eq_mod(&other))
    }

    /// If this theta node is a switch statement, return the underlying switch
    #[inline]
    pub fn switch(&self) -> Option<&Switch> {
        if self.blocks.len() == 1
            && self.blocks[0].sig.is_none()
            && self.blocks[0].body.insts.is_empty()
        {
            if let Label::Switch(switch) = &self.blocks[0].body.term.inner {
                return Some(switch);
            }
        }
        None
    }
}

impl BasicBlock {
    /// Compare modulo span
    #[inline]
    pub fn eq_mod(&self, other: &BasicBlock) -> bool {
        (match (&self.sig, &other.sig) {
            (None, None) => true,
            (Some(this), Some(other)) => this.inner.eq_mod(&other.inner),
            _ => false,
        }) && self.body.eq_mod(&other.body)
    }
}

impl Signature {
    /// Compare modulo span
    #[inline]
    pub fn eq_mod(&self, other: &Signature) -> bool {
        self.name.inner == other.name.inner
            && (match (&self.args, &other.args) {
                (None, None) => true,
                (Some(this), Some(other)) => this.inner.eq_mod(&other.inner),
                _ => false,
            })
    }
}

impl Body {
    /// Compare modulo span
    #[inline]
    pub fn eq_mod(&self, other: &Body) -> bool {
        self.insts.len() == other.insts.len()
            && self.term.inner.eq_mod(&other.term.inner)
            && self
                .insts
                .iter()
                .zip(other.insts.iter())
                .all(|(this, other)| this.inner.eq_mod(&other.inner))
    }
}

impl Label {
    /// Compare modulo span
    #[inline]
    pub fn eq_mod(&self, other: &Label) -> bool {
        match (self, other) {
            (Label::Return(this), Label::Return(other)) => this.eq_mod(other),
            (Label::Switch(this), Label::Switch(other)) => this.eq_mod(other),
            (Label::Jump(this), Label::Jump(other)) => this.eq_mod(other),
            _ => false,
        }
    }
}

impl Jump {
    /// Compare modulo span
    #[inline]
    pub fn eq_mod(&self, other: &Jump) -> bool {
        self.args.len() == other.args.len()
            && self.target.inner == other.target.inner
            && self
                .args
                .iter()
                .zip(other.args.iter())
                .all(|(this, other)| this.inner.eq_mod(&other.inner))
    }
}

impl Branch {
    /// Compare modulo span
    #[inline]
    pub fn eq_mod(&self, other: &Branch) -> bool {
        self.0.inner == other.0.inner && self.1.inner.eq_mod(&other.1.inner)
    }
}

impl Assign {
    /// Compare modulo span
    #[inline]
    pub fn eq_mod(&self, other: &Assign) -> bool {
        self.0.inner.eq_mod(&other.0.inner) && self.1.inner.eq_mod(&other.1.inner)
    }
}

impl Names {
    /// Compare modulo span
    #[inline]
    pub fn eq_mod(&self, other: &Names) -> bool {
        match (self, other) {
            (Names::Ident(this), Names::Ident(other)) => this == other,
            (Names::Product(this), Names::Product(other)) => {
                this.len() == other.len()
                    && this
                        .iter()
                        .zip(other.iter())
                        .all(|(this, other)| this.inner.eq_mod(&other.inner))
            }
            (Names::Annot(this), Names::Annot(other)) => {
                this.0.inner.eq_mod(&other.0.inner) && this.1.inner.eq_mod(&other.1.inner)
            }
            _ => false,
        }
    }
}

impl From<Path> for Expr {
    #[inline]
    fn from(p: Path) -> Self {
        Expr::Path(p)
    }
}

impl From<Theta> for Expr {
    fn from(t: Theta) -> Self {
        Expr::Theta(t)
    }
}

impl From<Bitvector> for Expr {
    fn from(b: Bitvector) -> Self {
        Expr::Bitvector(b)
    }
}

impl PartialEq<Path> for Expr {
    #[inline]
    fn eq(&self, other: &Path) -> bool {
        match self {
            Expr::Path(this) => this == other,
            _ => false,
        }
    }
}

impl PartialEq<Expr> for Path {
    #[inline]
    fn eq(&self, other: &Expr) -> bool {
        other.eq(self)
    }
}

impl PartialEq<Theta> for Expr {
    #[inline]
    fn eq(&self, other: &Theta) -> bool {
        match self {
            Expr::Theta(this) => this == other,
            _ => false,
        }
    }
}

impl PartialEq<Expr> for Theta {
    #[inline]
    fn eq(&self, other: &Expr) -> bool {
        other.eq(self)
    }
}

impl PartialEq<Bitvector> for Expr {
    #[inline]
    fn eq(&self, other: &Bitvector) -> bool {
        match self {
            Expr::Bitvector(this) => this == other,
            _ => false,
        }
    }
}

impl PartialEq<Expr> for Bitvector {
    #[inline]
    fn eq(&self, other: &Expr) -> bool {
        other.eq(self)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn from_sanity() {
        let empty_theta = || Theta { blocks: vec![] };
        assert_eq!(Expr::ident("x"), Expr::from(Path::ident("x")));
        assert_eq!(Expr::ident("x"), Path::ident("x"));
        assert_ne!(Path::ident("y"), Expr::ident("x"));
        assert_ne!(Path::ident("y"), Expr::Theta(empty_theta()));
        assert_eq!(Expr::Theta(empty_theta()), Expr::from(empty_theta()));
        assert_eq!(Expr::Theta(empty_theta()), empty_theta());
        assert_ne!(Expr::ident("y"), empty_theta());
        assert_ne!(empty_theta(), Expr::int(5));
        assert_ne!(Expr::int(5), Bitvector::int(7));
        assert_ne!(Expr::ident("y"), Bitvector::int(7));
        assert_eq!(
            Radix::Hex.with_bits(9, 0xA),
            Expr::from(Radix::Hex.with_bits(9, 0xA))
        );
    }
}
