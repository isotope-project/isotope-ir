/*!
Representation of primitive constants
*/
use std::fmt::Display;

use ibig::{ibig, ops::Abs, IBig};

/// A bitvector constant
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Bitvector {
    /// The bits, represented as a signed integer
    pub bits: IBig,
    /// The width of this bitvector, or 0 for unspecified
    pub width: u32,
    /// The default display radix of this bitvector
    pub radix: Radix,
}

impl Display for Bitvector {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let sign = if self.bits < ibig!(0) { "-" } else { "" };
        //OPTIMIZE: if possible, remove abs() call, which may otherwise result in an allocation
        if self.width == 0 {
            write!(
                f,
                "{sign}{}{}",
                self.radix.prefix_str(),
                (&self.bits).abs().in_radix(self.radix as u32)
            )
        } else {
            write!(
                f,
                "{sign}{}'{}{}",
                self.width,
                self.radix.as_str(),
                (&self.bits).abs().in_radix(self.radix as u32)
            )
        }
    }
}

impl Bitvector {
    /// Create a new decimal integer constant of unspecified width
    ///
    /// # Examples
    /// ```rust
    /// # use isotope_core_parser::ast::primitive::*;
    /// assert_eq!(
    ///     Bitvector::int(35),
    ///     Bitvector {
    ///         width: 0,
    ///         radix: Radix::Dec,
    ///         bits: 35.into()
    ///     }
    /// );
    /// ```
    #[inline]
    pub fn int(value: impl Into<IBig>) -> Bitvector {
        Radix::Dec.int(value)
    }
}

/// Default display radix for a bitvector
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Radix {
    /// Binary
    Bin = 2,
    /// Octal
    Oct = 8,
    /// Decimal
    Dec = 10,
    /// Hexadecimal
    Hex = 16,
}

impl Radix {
    /// Get this radix as a string
    #[inline]
    pub fn as_str(self) -> &'static str {
        match self {
            Radix::Bin => "b",
            Radix::Oct => "o",
            Radix::Dec => "d",
            Radix::Hex => "h",
        }
    }

    /// Get the integer prefix string for this radix
    #[inline]
    pub fn prefix_str(self) -> &'static str {
        match self {
            Radix::Bin => "0b",
            Radix::Oct => "0o",
            Radix::Dec => "",
            Radix::Hex => "0x",
        }
    }

    /// Create a new bitvector literal with the given radix, width, and value
    ///
    /// # Examples
    /// ```rust
    /// # use isotope_core_parser::ast::primitive::*;
    /// assert_eq!(
    ///     Radix::Hex.with_bits(32, 64),
    ///     Bitvector {
    ///         width: 32,
    ///         radix: Radix::Hex,
    ///         bits: 64.into()
    ///     }
    /// );
    /// ```
    #[inline]
    pub fn with_bits(self, width: u32, value: impl Into<IBig>) -> Bitvector {
        Bitvector {
            bits: value.into(),
            width,
            radix: self,
        }
    }

    /// Create a new integer literal with the given radix and value
    ///
    /// # Examples
    /// ```rust
    /// # use isotope_core_parser::ast::primitive::*;
    /// assert_eq!(
    ///     Radix::Oct.int(28),
    ///     Bitvector {
    ///         width: 0,
    ///         radix: Radix::Oct,
    ///         bits: 28.into()
    ///     }
    /// );
    /// ```
    #[inline]
    pub fn int(self, value: impl Into<IBig>) -> Bitvector {
        self.with_bits(0, value)
    }
}
