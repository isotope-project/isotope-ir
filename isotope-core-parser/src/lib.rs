/*!
A parser for the `isotope-core` intermediate representation
*/
#![forbid(unsafe_code, missing_debug_implementations, missing_docs)]

pub mod ast;
pub mod parser;
pub mod pretty;
