/*!
Utility parsers for whitespace, identifiers and comments
*/
use super::*;
use crate::ast::{Ident, Path};
use smallvec::smallvec;
use smol_str::SmolStr;

/// A list of characters treated specially by `isotope`, including whitespace
pub const SPECIAL_CHARS: &'static str = "\t\n\r \"#(),./:;=[]{}";

/// A list of characters which may not be included at the beginning of an `isotope` identifier, including whitespace
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::parser::*;
/// for char in NON_PREFIX_CHARS.chars() {
///     SPECIAL_CHARS.contains(char);
/// }
/// ```
pub const NON_PREFIX_CHARS: &'static str = "\t\n\r \"#\'(),-./0123456789:;=[]{}";

/// Parser combinator to wrap a parsed value with `Spanned`
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::parser::*;
/// # use isotope_core_parser::ast::*;
/// # use isotope_core_parser::assert_parse;
/// # use nom::sequence::*;
/// assert_parse!(spanned(bitwidth), "53'h", "'h", Spanned::new(53, 0..2));
/// assert_parse!(preceded(ws, spanned(bitwidth)), "   53'h", "'h", Spanned::new(53, 3..5));
/// ```
pub fn spanned<'a, T>(
    mut f: impl FnMut(Span<'a>) -> IResult<'a, T>,
) -> impl FnMut(Span<'a>) -> IResult<'a, Spanned<T>> {
    move |input: Span| {
        let (rest, parse) = f(input)?;
        Ok((
            rest,
            Spanned::new(parse, input.location_offset()..rest.location_offset()),
        ))
    }
}

/// Parse whitespace, including line comments and block comments
///
/// Accepts the empty input; use [`ws`] for nonempty whitespace
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::parser::utils::*;
/// # use isotope_core_parser::assert_parse;
/// assert_parse!(
///     ws_,
///      "
///         // A line comment
///         /* Followed by a /*nested*/ block comment */
///         // And another line comment just for good measure
///
///         // 一点儿 UTF-8
///
///         // ال Left to right
///
///     ",
///     "", ()
/// );
/// assert_parse!(ws_, "", "", ());
/// ```
pub fn ws_(input: Span) -> IResult<()> {
    value(
        (),
        many0_count(alt((multispace1, line_comment, block_comment))),
    )(input)
}

/// Parse whitespace, including line comments and block comments
///
/// Does *not* accept the empty input; use [`ws_`] for potentially empty whitespace
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::parser::utils::*;
/// # use isotope_core_parser::assert_parse;
/// assert_parse!(
///     ws,
///      "
///         // A line comment
///         /* Followed by a /*nested*/ block comment */
///         // And another line comment just for good measure
///
///         // 一点儿 UTF-8
///
///         // ال Left to right
///
///     ",
///     "", ()
/// );
/// assert!(ws("".into()).is_err());
/// ```
pub fn ws(input: Span) -> IResult<()> {
    value(
        (),
        many1_count(alt((multispace1, line_comment, block_comment))),
    )(input)
}

/// Parse a line comment
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::parser::utils::*;
/// # use isotope_core_parser::assert_parse_mod;
/// assert_parse_mod!(line_comment, "//A line comment\n5", "\n5", "A line comment");
/// ```
pub fn line_comment(input: Span) -> IResult<Span> {
    preceded(tag("//"), is_not("\n"))(input)
}

/// Parse a block comment, which may be nested
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::parser::utils::*;
/// # use isotope_core_parser::assert_parse_mod;
/// assert_parse_mod!(block_comment, "/*a block comment*/8", "8", "a block comment");
/// assert_parse_mod!(block_comment, "/*a *block* comment*/8", "8", "a *block* comment");
/// assert_parse_mod!(block_comment, "/*//not line*/8", "8", "//not line");
/// assert_parse_mod!(
///     block_comment,
///     "/*a /*nested /*block*/ */ /*comment*/ *//*hello",
///     "/*hello",
///     "a /*nested /*block*/ */ /*comment*/ "
/// );
/// assert!(block_comment("/*incomplete block comment".into()).is_err());
/// assert!(block_comment("/*incomplete /*nested*/ comment".into()).is_err());
/// ```
pub fn block_comment(input: Span) -> IResult<Span> {
    delimited(
        tag("/*"),
        recognize(many0_count(alt((
            is_not("/*"),
            tag("//"),
            terminated(tag("*"), none_of("/")),
            block_comment,
        )))),
        tag("*/"),
    )(input)
}

/// Parse an `isotope` path
///
/// This is defined to be a `.` separated list of `isotope` identifiers, with optional whitespace between periods.
/// This does not consume either leading or trailing whitespace.
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::parser::utils::*;
/// # use isotope_core_parser::ast::Path;
/// # use smallvec::smallvec;
/// # use isotope_core_parser::assert_parse;
/// assert_parse!(path, "x.x", "", Path(smallvec!["x".into(), "x".into()]));
/// assert_parse!(path, "x  . y", "", Path(smallvec!["x".into(), "y".into()]));
/// assert_parse!(path, "你好", "", Path(smallvec!["你好".into()]));
/// assert_parse!(path, "x:x", ":x", Path(smallvec!["x".into()]));
/// ```
pub fn path(input: Span) -> IResult<Path> {
    let (rest, first) = ident(input)?;
    fold_many0(
        preceded(delimited(ws_, tag("."), ws_), ident),
        move || Path(smallvec![first.clone()]),
        |mut path, ident: SmolStr| {
            path.0.push(ident.into());
            path
        },
    )(rest)
}

/// Parse an `isotope` identifier
///
/// This is defined to be any string of non-whitespace Unicode characters not containing one of [`SPECIAL_CHARS`] which does not start with one of [`NON_PREFIX_CHARS`]
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::parser::utils::*;
/// # use isotope_core_parser::assert_parse;
/// assert_parse!(ident, "x", "", "x");
/// assert_parse!(ident, "var1", "", "var1");
/// assert_parse!(ident, "事情", "", "事情");
/// assert_parse!(ident, "事情-", "", "事情-");
/// assert_parse!(ident, "事情#x", "#x", "事情");
/// assert_parse!(ident, "事情'x", "", "事情'x");
/// assert!(ident("-var".into()).is_err());
/// assert!(ident("'var".into()).is_err());
/// assert!(ident(":var".into()).is_err());
/// assert!(ident("1var".into()).is_err());
/// assert!(ident("".into()).is_err());
/// ```
pub fn ident(input: Span) -> IResult<Ident> {
    map(
        recognize(pair(none_of(NON_PREFIX_CHARS), opt(is_not(SPECIAL_CHARS)))),
        |span: Span| span.fragment().into(),
    )(input)
}

/// Compare a parse is equal to a given value with a given remainder
#[macro_export]
macro_rules! assert_parse {
    ($parse:expr, $input:expr, $rest:expr, _) => {{
        let parse = ($parse(Into::into($input))).expect("expected valid parse");
        let input = $input;
        assert_eq!(*parse.0.fragment(), $rest, "invalid remainder for {input}",);
    }};
    ($parse:expr, $input:expr, _, $value:expr) => {{
        let parse = ($parse(Into::into($input))).expect("expected valid parse");
        let input = $input;
        assert_eq!(parse.1, $value, "invalid parsed value; input = {input}");
    }};
    ($parse:expr, $input:expr, $rest:expr, $value:expr) => {{
        let parse = ($parse(Into::into($input))).expect("expected valid parse");
        let input = $input;
        assert_eq!(*parse.0.fragment(), $rest, "invalid remainder for {input}",);
        assert_eq!(parse.1, $value, "invalid parsed value; input = {input}");
    }};
}

/// Compare a parse is equal to a given value, modulo spans, with a given remainder
#[macro_export]
macro_rules! assert_parse_mod {
    ($parse:expr, $input:expr, $rest:expr, $value:literal) => {{
        let parse = ($parse(Into::into($input))).expect("expected valid parse");
        let input = $input;
        assert_eq!(*parse.0.fragment(), $rest, "invalid remainder for {input}");
        assert_eq!(
            *parse.1.fragment(),
            $value,
            "invalid parsed value; input = {input}"
        );
    }};
    ($parse:expr, $input:expr, $rest:expr, $value:expr) => {{
        let parse = ($parse(Into::into($input))).expect("expected valid parse");
        let input = $input;
        assert_eq!(*parse.0.fragment(), $rest, "invalid remainder for {input}");
        assert!(
            parse.1.eq_mod(&$value),
            "invalid parsed value; input = {input}"
        );
    }};
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parse_idents() {
        let idents = ["x", "y", "z"];
        for id in idents {
            assert_parse!(ident, id, "", id);
            assert_parse!(path, id, "", id);
        }
    }

    #[test]
    fn bad_idents() {
        let idents = [
            "=",
            ";",
            "(bad)",
            "-",
            "-5",
            "38",
            "0x53",
            "'c'",
            "\"hello\"",
            "   starts_with_whitespace",
        ];
        for id in idents {
            ident(id.into()).unwrap_err();
            path(id.into()).unwrap_err();
        }
    }

    #[test]
    fn parse_comments() {
        let comments = [
            "// a comment",
            "/* nested /*multiline*\n\n*/ comment */ /*and/*/*another*/*/\n*/",
        ];
        for comment in comments {
            assert_parse!(ws, comment, "", ());
            assert_parse!(ws_, comment, "", ());
        }
    }
}
