/*!
A parser for the `isotope-core` intermediate representation
*/
use crate::ast::*;

use nom::branch::*;
use nom::bytes::complete::*;
use nom::character::complete::*;
use nom::combinator::*;
use nom::multi::*;
use nom::sequence::*;
use nom_locate::LocatedSpan;

/// A spanned string, forming a parser input
pub type Span<'a> = LocatedSpan<&'a str>;

/// A parsing result of type `T`
pub type IResult<'a, T> = nom::IResult<Span<'a>, T>;

pub mod primitive;
pub mod utils;

pub use primitive::*;
pub use utils::*;

/// Parse a type
pub fn ty(input: Span) -> IResult<Type> {
    alt((
        map(path, Type::Path),
        map(
            delimited(
                preceded(tag("{"), ws_),
                separated_list0(delimited(ws_, tag(","), ws_), spanned(ty)),
                preceded(ws_, tag("}")),
            ),
            Type::Union,
        ),
        map(
            delimited(
                preceded(tag("("), ws_),
                separated_list0(delimited(ws_, tag(","), ws_), spanned(ty)),
                preceded(ws_, tag(")")),
            ),
            Type::Product,
        ),
        map(
            tuple((
                tag("["),
                ws_,
                spanned(ty),
                ws_,
                tag(";"),
                ws_,
                bitwidth,
                ws_,
                tag("]"),
            )),
            |(_, _, a, _, _, _, n, _, _)| Type::Array(Box::new(a), n),
        ),
        map(preceded(tag("#b"), bitwidth), Type::Bitvector),
    ))(input)
}

/// Parse a declaration
pub fn decl(input: Span) -> IResult<Decl> {
    alt((
        map(constant, Decl::Constant),
        map(typedef, Decl::Typedef),
        map(function, Decl::Function),
    ))(input)
}

/// Parse a constant definition
pub fn constant(input: Span) -> IResult<Constant> {
    map(
        tuple((
            tag("#const"),
            ws,
            spanned(unbracketed_names),
            ws_,
            tag("="),
            ws_,
            spanned(expr),
            ws_,
            tag(";"),
        )),
        |(_, _, names, _, _, _, def, _, _)| Constant { names, def },
    )(input)
}

/// Parse a type definition
pub fn typedef(input: Span) -> IResult<Typedef> {
    map(
        tuple((
            tag("#type"),
            ws,
            spanned(ident),
            ws_,
            tag("="),
            ws_,
            spanned(ty),
            ws_,
            tag(";"),
        )),
        |(_, _, name, _, _, _, def, _, _)| Typedef { name, def },
    )(input)
}

/// Parse a function definition
pub fn function(input: Span) -> IResult<Function> {
    map(
        tuple((
            tag("#fn"),
            ws,
            spanned(ident),
            ws_,
            spanned(bracketed_names),
            ws_,
            tag("->"),
            ws_,
            spanned(ty),
            ws_,
            alt((
                delimited(
                    preceded(tag("="), ws_),
                    spanned(expr),
                    preceded(ws_, tag(";")),
                ),
                spanned(map(theta, Expr::Theta)),
            )),
        )),
        |(_, _, name, _, args, _, _, _, ret, _, def)| Function {
            name,
            args,
            ret,
            def,
        },
    )(input)
}

/// Parse an expression
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::ast::*;
/// # use isotope_core_parser::parser::*;
/// # use isotope_core_parser::*;
/// assert_parse!(expr, "5", "", Expr::int(5));
/// assert_parse!(
///     expr,
///     "f x y",
///     "",
///     Expr::App(vec![
///         Spanned::new(Expr::ident("f"), 0..1),
///         Spanned::new(Expr::ident("x"), 2..3),
///         Spanned::new(Expr::ident("y"), 4..5),
///     ])
/// );
/// assert_parse!(
///     expr,
///     "f x y, g x",
///     "",
///     Expr::Tuple(vec![
///         Spanned::new(
///             Expr::App(vec![
///                 Spanned::new(Expr::ident("f"), 0..1),
///                 Spanned::new(Expr::ident("x"), 2..3),
///                 Spanned::new(Expr::ident("y"), 4..5),
///             ]),
///             0..5
///         ),
///         Spanned::new(
///             Expr::App(vec![
///                 Spanned::new(Expr::ident("g"), 7..8),
///                 Spanned::new(Expr::ident("x"), 9..10),
///             ]),
///             7..10
///         )
///     ])
/// );
/// assert!(expr("".into()).is_err())
/// ```
pub fn expr(input: Span) -> IResult<Expr> {
    let (rest, first) = spanned(app)(input)?;
    let mut first = Some(first);
    let mut result = vec![];
    let (rest, _) = fold_many0(
        preceded(delimited(ws_, tag(","), ws_), spanned(app)),
        || (),
        |_, atom| {
            if let Some(first) = first.take() {
                result = vec![first, atom]
            } else {
                result.push(atom)
            }
        },
    )(rest)?;
    let (rest, terminator) = opt(preceded(ws_, tag(",")))(rest)?;
    let result = if let Some(first) = first {
        if terminator.is_some() {
            Expr::Tuple(vec![first])
        } else {
            first.inner
        }
    } else {
        Expr::Tuple(result)
    };
    Ok((rest, result))
}

/// Parse an application
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::ast::*;
/// # use isotope_core_parser::parser::*;
/// # use isotope_core_parser::*;
/// assert_parse!(app, "5", "", Expr::int(5));
/// assert_parse!(
///     app,
///     "f x y",
///     "",
///     Expr::App(vec![
///         Spanned::new(Expr::ident("f"), 0..1),
///         Spanned::new(Expr::ident("x"), 2..3),
///         Spanned::new(Expr::ident("y"), 4..5),
///     ])
/// );
/// assert_parse!(
///     app,
///     "f x y, g x",
///     ", g x",
///     Expr::App(vec![
///         Spanned::new(Expr::ident("f"), 0..1),
///         Spanned::new(Expr::ident("x"), 2..3),
///         Spanned::new(Expr::ident("y"), 4..5),
///     ])
/// );
/// assert!(app("".into()).is_err())
/// ```
pub fn app(input: Span) -> IResult<Expr> {
    let (rest, first) = spanned(atom)(input)?;
    let mut first = Some(first);
    let mut result = vec![];
    let (rest, _) = fold_many0(
        preceded(ws_, spanned(atom)),
        || (),
        |_, atom| {
            if let Some(first) = first.take() {
                result = vec![first, atom]
            } else {
                result.push(atom)
            }
        },
    )(rest)?;
    Ok((
        rest,
        first
            .take()
            .map(|first| first.inner)
            .unwrap_or(Expr::App(result)),
    ))
}

/// Parse an atom
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::ast::*;
/// # use isotope_core_parser::parser::*;
/// # use isotope_core_parser::*;
/// assert_parse!(atom, "5", "", Expr::int(5));
/// assert_parse!(atom, "f x y", " x y", Expr::ident("f"));
/// ```
pub fn atom(input: Span) -> IResult<Expr> {
    alt((
        map(path, Expr::Path),
        map(
            delimited(
                preceded(tag("("), ws_),
                spanned(expr),
                preceded(ws_, tag(")")),
            ),
            |bracketed| match bracketed {
                bracketed @ Spanned {
                    inner: Expr::Tuple(_) | Expr::App(_),
                    ..
                } => bracketed.inner,
                bracketed => Expr::App(vec![bracketed.into()]),
            },
        ),
        map(spanned(switch), |switch| {
            Expr::Theta(Theta {
                blocks: vec![BasicBlock {
                    sig: None,
                    body: Body {
                        insts: vec![],
                        term: switch.map(Label::Switch),
                    },
                }],
            })
        }),
        map(theta, Expr::Theta),
        value(Expr::Abort, tag("#abort")),
        value(Expr::Undef, tag("#undef")),
        delimited(
            preceded(tag("["), ws_),
            map(
                separated_list0(delimited(ws_, tag(","), ws_), spanned(expr)),
                Expr::Array,
            ),
            terminated(preceded(ws_, opt(preceded(tag(","), ws))), tag("]")),
        ),
        map(bitvector, Expr::Bitvector),
    ))(input)
}

/// Parse a switch statement
pub fn switch(input: Span) -> IResult<Switch> {
    map(
        tuple((
            tag("#switch"),
            ws,
            spanned(atom),
            ws_,
            tag("{"),
            ws_,
            separated_list0(delimited(ws_, tag(","), ws_), spanned(branch)),
            ws_,
            opt(terminated(tag(","), ws_)),
            tag("}"),
        )),
        |(_, _, disc, _, _, _, branches, _, _, _)| Switch {
            disc: Box::new(disc),
            branches,
        },
    )(input)
}

/// Parse a theta node
pub fn theta(input: Span) -> IResult<Theta> {
    delimited(
        preceded(tag("{"), ws_),
        map(separated_list0(ws, basic_block), |blocks| Theta { blocks }),
        preceded(ws_, tag("}")),
    )(input)
}

/// Parse a basic block
pub fn basic_block(input: Span) -> IResult<BasicBlock> {
    map(
        separated_pair(opt(spanned(bb_signature)), ws_, body),
        |(sig, body)| BasicBlock { sig, body },
    )(input)
}

/// Parse a basic block signature
pub fn bb_signature(input: Span) -> IResult<Signature> {
    map(
        tuple((
            tag("'"),
            spanned(ident),
            ws_,
            opt(terminated(spanned(bracketed_names), ws_)),
            tag(":"),
        )),
        |(_, name, _, args, _)| Signature { name, args },
    )(input)
}

/// Parse a basic block body
pub fn body(input: Span) -> IResult<Body> {
    map(
        pair(many0(terminated(spanned(assign), ws_)), spanned(label)),
        |(insts, term)| Body { insts, term },
    )(input)
}

/// Parse a label
pub fn label(input: Span) -> IResult<Label> {
    alt((
        map(jump, Label::Jump),
        map(switch, Label::Switch),
        map(app, Label::Return),
    ))(input)
}

/// Parse a jump
pub fn jump(input: Span) -> IResult<Jump> {
    map(
        tuple((
            tag("#jmp"),
            ws,
            tag("'"),
            spanned(ident),
            many0(preceded(ws, spanned(atom))),
        )),
        |(_, _, _, target, args)| Jump { target, args },
    )(input)
}

/// Parse a branch
pub fn branch(input: Span) -> IResult<Branch> {
    map(
        tuple((spanned(pattern), ws_, tag("=>"), ws_, spanned(body))),
        |(pattern, _, _, _, result)| Branch(pattern, result),
    )(input)
}

/// Parse a switch pattern
pub fn pattern(input: Span) -> IResult<Pattern> {
    alt((
        map(bitvector, Pattern::Bitvector),
        value(Pattern::Wildcard, tag("_")),
    ))(input)
}

/// Parse an assignment
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::ast::*;
/// # use isotope_core_parser::parser::*;
/// # use isotope_core_parser::*;
/// assert_parse!(
///     assign,
///     "x = 5;",
///     "",
///     Assign(
///         Spanned::new(Names::Ident("x".into()), 0..1),
///         Spanned::new(Expr::Bitvector(Bitvector::int(5)), 4..5),
///     )
/// );
/// assert!(assign("".into()).is_err())
pub fn assign(input: Span) -> IResult<Assign> {
    map(
        tuple((
            spanned(unbracketed_names),
            ws_,
            tag("="),
            ws_,
            spanned(expr),
            ws_,
            tag(";"),
        )),
        |(names, _, _, _, def, _, _)| Assign(names, def),
    )(input)
}

/// Parse a bracketed set of names
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::ast::*;
/// # use isotope_core_parser::parser::*;
/// # use isotope_core_parser::*;
/// assert_parse!(bracketed_names, "(x)", "", Names::Ident("x".into()));
/// assert_parse!(
///     bracketed_names,
///     "(x, y)",
///     "",
///     Names::Product(vec![
///         Spanned::new(Names::Ident("x".into()), 1..2),
///         Spanned::new(Names::Ident("y".into()), 4..5),
///     ])
/// );
/// assert!(bracketed_names("x".into()).is_err())
/// ```
pub fn bracketed_names(input: Span) -> IResult<Names> {
    delimited(
        preceded(tag("("), ws_),
        unbracketed_names,
        preceded(ws_, tag(")")),
    )(input)
}

/// Parse an unbracketed set of names
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::ast::*;
/// # use isotope_core_parser::parser::*;
/// # use isotope_core_parser::*;
/// assert_parse!(unbracketed_names, "x", "", Names::Ident("x".into()));
/// assert_parse!(
///     unbracketed_names,
///     "(x, y)",
///     "",
///     Names::Product(vec![
///         Spanned::new(Names::Ident("x".into()), 1..2),
///         Spanned::new(Names::Ident("y".into()), 4..5),
///     ])
/// );
/// assert_parse!(
///     unbracketed_names,
///     "x, y",
///     "",
///     Names::Product(vec![
///         Spanned::new(Names::Ident("x".into()), 0..1),
///         Spanned::new(Names::Ident("y".into()), 3..4),
///     ])
/// );
/// assert_parse!(
///     unbracketed_names,
///     "x, y,",
///     "",
///     Names::Product(vec![
///         Spanned::new(Names::Ident("x".into()), 0..1),
///         Spanned::new(Names::Ident("y".into()), 3..4),
///     ])
/// );
/// ```
pub fn unbracketed_names(input: Span) -> IResult<Names> {
    map(
        pair(
            separated_list1(delimited(ws_, tag(","), ws_), spanned(annot)),
            opt(preceded(ws_, tag(","))),
        ),
        |(mut result, trailing)| {
            if result.len() == 1 && trailing.is_none() {
                result.remove(0).inner
            } else {
                Names::Product(result)
            }
        },
    )(input)
}

/// Parse an atomic set of names
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::ast::*;
/// # use isotope_core_parser::parser::*;
/// # use isotope_core_parser::*;
/// assert_parse!(names, "x", "", Names::Ident("x".into()));
/// assert_parse!(names, "(x)", "", Names::Ident("x".into()));
/// assert_parse!(
///     names,
///     "(x, y)",
///     "",
///     Names::Product(vec![
///         Spanned::new(Names::Ident("x".into()), 1..2),
///         Spanned::new(Names::Ident("y".into()), 4..5),
///     ])
/// );
/// assert_parse!(
///     names,
///     "(x, y,)",
///     "",
///     Names::Product(vec![
///         Spanned::new(Names::Ident("x".into()), 1..2),
///         Spanned::new(Names::Ident("y".into()), 4..5),
///     ])
/// );
/// assert_parse!(names, "x, y", ", y", Names::Ident("x".into()));
/// assert!(names("".into()).is_err())
/// ```
pub fn names(input: Span) -> IResult<Names> {
    alt((map(ident, Names::Ident), bracketed_names))(input)
}

/// Parse an optionally annotated set of names
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::ast::*;
/// # use isotope_core_parser::parser::*;
/// # use isotope_core_parser::*;
/// assert_parse!(names, "x", "", Names::Ident("x".into()));
/// assert_parse!(
///     annot,
///     "x: A",
///     "",
///     Names::Annot(Box::new((
///         Spanned::new(Names::Ident("x".into()), 0..1),
///         Spanned::new(Type::Path(Path::ident("A")), 3..4),
///     )))
/// );
/// assert!(annot("".into()).is_err());
/// ```
pub fn annot(input: Span) -> IResult<Names> {
    map(
        pair(
            spanned(names),
            opt(preceded(delimited(ws_, tag(":"), ws_), spanned(ty))),
        ),
        |(names, ty)| {
            if let Some(ty) = ty {
                Names::Annot(Box::new((names, ty)))
            } else {
                names.inner
            }
        },
    )(input)
}

#[cfg(test)]
mod test {
    use crate::assert_parse;

    use super::*;

    #[test]
    fn basic_name_parsing() {
        let n = [
            ("x", false),
            ("(x)", true),
            ("x: A", false),
            ("(x: A)", true),
        ];
        for (s, b) in n {
            let p = names(s.into()).unwrap();
            if b {
                assert_eq!(bracketed_names(s.into()).unwrap(), p)
            } else {
                bracketed_names(s.into()).unwrap_err();
            }
        }
    }

    #[test]
    fn tuple_edge_cases() {
        assert_parse!(
            expr,
            "(x)   ",
            "   ",
            Expr::App(vec![Spanned::new(Expr::ident("x"), 1..2)])
        );
        assert_parse!(
            expr,
            "(x,)   ",
            "   ",
            Expr::Tuple(vec![Spanned::new(Expr::ident("x"), 1..2)])
        );
    }

    #[test]
    fn basic_decl_parsing() {
        let decls = [
            "#fn id(x: A) -> A = x;",
            "#fn pi0(x: A, y: B) -> A = x;",
            "#fn idb(x: A) -> A { x }",
            "
            #fn useless_blocks(x: A) -> A {
                'entry:
                    x
                'other:
                    x
                'otherother(x: A):
                    x
            }",
            "
            #fn useless_blocks'(x: A) -> A {
                'entry:
                    y = x;
                    y
                'other:
                    x
                'otherother(x: A):
                    x
            }",
            "
            #fn useless_blocks''(x: A) -> A {
                'entry:
                    y = x;
                    y
                'other:
                    x
                'otherother(x: A, y: #b64):
                    x
            }",
            "#fn max(x: #b64, y: #b64) -> #b64 = #switch (lt x y) {
                0 => x,
                1 => y,
            };",
            "#fn swap(x: #b64, y: #b64) -> (#b64, #b64) {
                z = x;
                x = y;
                y = z;
                (x, y)
            }",
            "#fn fallthrough(x: #b64, y: #b64) -> (#b64, #b64) {
                z = x;
                x = y;
                #jmp 'other x
            'other(x: A):
                x
            }",
            "#fn if(b: #b1, y: #b64) -> (#b64, #b64) {
                #switch b {
                    0 => #jmp 'add1,
                    1 => #jmp 'add2,
                }
            'add1:
                add y 1
            'add2:
                add y 2
            }",
            "#fn fact(n: #b64) -> #b64 {
                #switch (le 1 n) {
                    0 => #jmp 'loop 2 1,
                    1 => 1
                }
            'loop (i: #b64, a: #b64):
                a = mul a i;
                #switch (lt i n) {
                    0 => a,
                    1 => #jmp 'loop (add i 1) a
                }
            }",
        ];
        let mut decl_ws = delimited(ws_, decl, ws_);
        for d in decls {
            assert_eq!(*decl_ws(d.into()).unwrap().0.fragment(), "");
        }
    }
}
