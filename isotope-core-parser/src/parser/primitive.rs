/*!
Parsers for primitive `isotope` constants
*/
use super::*;
use ibig::IBig;

/// Parse a bitvector
///
/// As in Verilog, this is represented by an optional sign, followed by a bitwidth, followed by a radix, followed by a string of digits
///
/// ```rust
/// # use isotope_core_parser::parser::utils::*;
/// # use isotope_core_parser::parser::primitive::*;
/// # use isotope_core_parser::ast::*;
/// # use isotope_core_parser::assert_parse;
/// assert_parse!(bitvector, "8'b01111110", "", Radix::Bin.with_bits(8, 0b01111110));
/// assert_parse!(bitvector, "35", "", Radix::Dec.with_bits(0, 35));
/// assert_parse!(bitvector, "-0o52", "", Radix::Oct.with_bits(0, -0o52));
/// assert_parse!(bitvector, "-7'h0f", "", Radix::Hex.with_bits(7, -0xf));
/// ```
pub fn bitvector(input: Span) -> IResult<Bitvector> {
    map_res(
        pair(bitvector_prefix, hex_digit1),
        |((sign, width, radix), digits)| {
            IBig::from_str_radix(digits.fragment(), radix as u32).map(|bits| Bitvector {
                bits: bits * sign,
                radix,
                width,
            })
        },
    )(input)
}

/// Parse a valid prefix for a bitvector
///
/// This is made up of a combination of (optional) sign, bitwidth and radix.
/// Accepts the empty string, which is taken to have sign 1, bitwidth 0 and radix decimal.
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::parser::utils::*;
/// # use isotope_core_parser::parser::primitive::*;
/// # use isotope_core_parser::ast::*;
/// # use isotope_core_parser::assert_parse;
/// assert_parse!(bitvector_prefix, "0x", "", (1, 0, Radix::Hex));
/// assert_parse!(bitvector_prefix, "32'h", "", (1, 32, Radix::Hex));
/// assert_parse!(bitvector_prefix, "-64'b", "", (-1, 64, Radix::Bin));
/// assert_parse!(bitvector_prefix, "", "", (1, 0, Radix::Dec));
/// assert_parse!(bitvector_prefix, "-", "", (-1, 0, Radix::Dec));
/// assert_parse!(bitvector_prefix, "0'h", "", (1, 0, Radix::Hex));
/// assert_parse!(bitvector_prefix, "0", "0", (1, 0, Radix::Dec));
/// assert_parse!(bitvector_prefix, "52", "52", (1, 0, Radix::Dec));
/// assert_parse!(bitvector_prefix, "-38", "38", (-1, 0, Radix::Dec));
/// ```
pub fn bitvector_prefix(input: Span) -> IResult<(i8, u32, Radix)> {
    map(
        pair(
            opt(tag("-")),
            alt((
                map(int_prefix, |radix| (0, radix)),
                separated_pair(bitwidth, tag("'"), radix),
                success((0, Radix::Dec)),
            )),
        ),
        |(sign, (width, radix))| (if sign.is_some() { -1 } else { 1 }, width, radix),
    )(input)
}

/// Parse a valid bitwidth for a bitvector type
///
/// This must be a decimal integer which fits in 32 bits.
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::parser::utils::*;
/// # use isotope_core_parser::parser::primitive::*;
/// # use isotope_core_parser::assert_parse;
/// assert_parse!(bitwidth, "1", "", 1);
/// assert_parse!(bitwidth, "8", "", 8);
/// assert_parse!(bitwidth, "16", "", 16);
/// assert_parse!(bitwidth, "32", "", 32);
/// assert_parse!(bitwidth, "64", "", 64);
/// assert_parse!(bitwidth, "128", "", 128);
/// assert_parse!(bitwidth, "256", "", 256);
/// assert_parse!(bitwidth, "57", "", 57);
/// assert!(bitwidth("4294967296".into()).is_err());
/// ```
pub fn bitwidth(input: Span) -> IResult<u32> {
    map_res(digit1, |s: Span| u32::from_str_radix(s.fragment(), 10))(input)
}

/// Parse a prefix for an integer literal
///
/// This can be either `0b` for binary, `0o` for octal, `0d` for decimal, or `0h`/`0x` for hexadecimal.
///
/// # Examples
/// ```
/// # use isotope_core_parser::parser::utils::*;
/// # use isotope_core_parser::parser::primitive::*;
/// # use isotope_core_parser::ast::*;
/// # use isotope_core_parser::assert_parse;
/// assert_parse!(int_prefix, "0b", "", Radix::Bin);
/// assert_parse!(int_prefix, "0o", "", Radix::Oct);
/// assert_parse!(int_prefix, "0d", "", Radix::Dec);
/// assert_parse!(int_prefix, "0h", "", Radix::Hex);
/// assert_parse!(int_prefix, "0x", "", Radix::Hex);
/// assert_parse!(int_prefix, "0B", "", Radix::Bin);
/// assert_parse!(int_prefix, "0O", "", Radix::Oct);
/// assert_parse!(int_prefix, "0D", "", Radix::Dec);
/// assert_parse!(int_prefix, "0H", "", Radix::Hex);
/// assert_parse!(int_prefix, "0X", "", Radix::Hex);
/// assert!(int_prefix("td".into()).is_err());
/// ```
pub fn int_prefix(input: Span) -> IResult<Radix> {
    preceded(
        tag("0"),
        alt((
            value(Radix::Bin, tag("b")),
            value(Radix::Oct, tag("o")),
            value(Radix::Dec, tag("d")),
            value(Radix::Hex, tag("x")),
            value(Radix::Hex, tag("h")),
            value(Radix::Bin, tag("B")),
            value(Radix::Oct, tag("O")),
            value(Radix::Dec, tag("D")),
            value(Radix::Hex, tag("X")),
            value(Radix::Hex, tag("H")),
        )),
    )(input)
}

/// Parse a radix for a bitvector
///
/// This can be either `b` for binary, `o` for octal, `d` for decimal, or `h` for hexadecimal.
///
/// # Examples
/// ```
/// # use isotope_core_parser::parser::utils::*;
/// # use isotope_core_parser::parser::primitive::*;
/// # use isotope_core_parser::ast::*;
/// # use isotope_core_parser::assert_parse;
/// assert_parse!(radix, "b", "", Radix::Bin);
/// assert_parse!(radix, "o", "", Radix::Oct);
/// assert_parse!(radix, "d", "", Radix::Dec);
/// assert_parse!(radix, "h", "", Radix::Hex);
/// assert_parse!(radix, "B", "", Radix::Bin);
/// assert_parse!(radix, "O", "", Radix::Oct);
/// assert_parse!(radix, "D", "", Radix::Dec);
/// assert_parse!(radix, "H", "", Radix::Hex);
/// assert!(radix("td".into()).is_err());
/// ```
pub fn radix(input: Span) -> IResult<Radix> {
    alt((
        value(Radix::Bin, alt((tag("b"), tag("B")))),
        value(Radix::Oct, alt((tag("o"), tag("O")))),
        value(Radix::Dec, alt((tag("d"), tag("D")))),
        value(Radix::Hex, alt((tag("h"), tag("H")))),
    ))(input)
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::assert_parse;
    use rand::{seq::SliceRandom, Rng, SeedableRng};
    use rand_xoshiro::Xoshiro256StarStar;

    #[test]
    fn decimal_int_parsing() {
        const ITERS: usize = 1000;
        let mut rng = Xoshiro256StarStar::seed_from_u64(4563);

        for _ in 0..ITERS {
            let i: i128 = rng.gen();
            let b = Bitvector::int(i);
            let f = format!("{i}");
            assert_parse!(bitvector, &*f, "", b);
            assert_eq!(format!("{b}"), f);
        }
    }

    #[test]
    fn bitvector_roundtrip() {
        const ITERS: usize = 1000;
        const PROP_INT: f64 = 0.3;
        let mut rng = Xoshiro256StarStar::seed_from_u64(4563);

        for _ in 0..ITERS {
            let i: i128 = rng.gen();
            let r = *[Radix::Bin, Radix::Oct, Radix::Hex, Radix::Dec]
                .choose(&mut rng)
                .unwrap();
            let w: u32 = if rng.gen_bool(PROP_INT) { 0 } else { rng.gen() };
            let b = r.with_bits(w, i);
            let f = format!("{b}");
            assert_parse!(bitvector, &*f, "", b);
        }
    }
}
