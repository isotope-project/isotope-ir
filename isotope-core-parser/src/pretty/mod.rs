/*!
Pretty-printing for &'a `isotope` intermediate representation ASTs
*/
use super::ast::*;
use pretty::{DocAllocator, DocBuilder, Pretty, RcAllocator};

/// Prettyprint something atomically
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Atom<T>(pub T);

impl<'a, D, A, P> Pretty<'a, D, A> for &'a Spanned<P>
where
    P: 'a,
    &'a P: Pretty<'a, D, A>,
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    #[inline]
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        self.inner.pretty(allocator)
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Type
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        match self {
            Type::Path(this) => this.pretty(allocator),
            Type::Union(arr) => allocator
                .intersperse(arr, allocator.text(",") + allocator.line())
                .nest(2)
                .group()
                .braces(),
            Type::Product(arr) => allocator
                .intersperse(arr, allocator.text(",") + allocator.line())
                .nest(2)
                .group()
                .parens(),
            Type::Array(ty, n) => {
                (allocator.text("[") + &**ty + ";" + allocator.line() + format!("{n}") + "]")
                    .group()
            }
            Type::Bitvector(n) => allocator.text(format!("#b{n}")),
        }
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Decl
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        match self {
            Decl::Constant(this) => this.pretty(allocator),
            Decl::Typedef(this) => this.pretty(allocator),
            Decl::Function(this) => this.pretty(allocator),
        }
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Constant
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        allocator
            .text("#const")
            .append(allocator.line())
            .append(&self.names.inner)
            .append(allocator.line())
            .append(allocator.text("="))
            .append(allocator.line())
            .append(&self.def.inner)
            .append(allocator.text(";"))
            .group()
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Typedef
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        allocator
            .text("#type")
            .append(allocator.line())
            .append(allocator.text(&*self.name.inner))
            .append(allocator.line())
            .append(allocator.text("="))
            .append(allocator.line())
            .append(&self.def.inner)
            .append(allocator.text(";"))
            .group()
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Function
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        let signature = allocator
            .text("#fn")
            .append(allocator.line())
            .append(allocator.text(&*self.name.inner))
            .append(Atom(&self.args.inner))
            .append(allocator.line())
            .append(allocator.text("->"))
            .append(allocator.line())
            .append(&self.ret.inner)
            .append(allocator.line())
            .group();
        match &self.def.inner {
            Expr::Theta(theta) if theta.switch().is_none() => signature.append(theta),
            _ => signature
                .append("=")
                .append(allocator.line())
                .append(&self.def)
                .append(";")
                .group(),
        }
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Expr
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        match self {
            Expr::Path(path) => path.pretty(allocator),
            Expr::App(app) => allocator
                .intersperse(app, allocator.line())
                .nest(2)
                .group()
                .parens(),
            Expr::Theta(theta) => theta.pretty(allocator),
            Expr::Abort => allocator.text("#abort"),
            Expr::Undef => allocator.text("#undef"),
            Expr::Tuple(tup) => allocator
                .intersperse(tup, allocator.text(",") + allocator.line())
                .append(if tup.len() == 1 {
                    allocator.text(",")
                } else {
                    allocator.nil()
                })
                .nest(2)
                .group()
                .parens(),
            Expr::Array(arr) => allocator
                .intersperse(arr, allocator.text(",") + allocator.line())
                .nest(2)
                .group()
                .brackets(),
            Expr::Bitvector(bv) => bv.pretty(allocator),
            Expr::Error(err) => {
                if err.is_empty() {
                    allocator.text("#error")
                } else {
                    allocator.text("#error[") + allocator.line_() + &**err + allocator.line_() + "]"
                }
            }
        }
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Path
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        allocator
            .intersperse(
                //TODO: identifier escaping
                self.0.iter().map(|ident| allocator.text(&**ident)),
                allocator.line_() + allocator.text("."),
            )
            .nest(2)
            .group()
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Theta
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        if let Some(switch) = self.switch() {
            return switch.pretty(allocator);
        }
        allocator
            .line()
            .append(allocator.intersperse(&self.blocks, allocator.hardline()))
            .append(allocator.line())
            .braces()
            .group()
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a BasicBlock
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        let body = self.body.pretty(allocator).group().indent(2);
        if let Some(sig) = &self.sig {
            sig.pretty(allocator)
                .append(allocator.hardline())
                .append(body)
        } else {
            body
        }
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Signature
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        allocator
            .text("'")
            .append(&*self.name.inner)
            .append(if let Some(args) = &self.args {
                Atom(&args.inner).pretty(allocator)
            } else {
                allocator.nil()
            })
            .append(":")
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Body
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        if self.insts.len() == 0 {
            self.term.pretty(allocator)
        } else {
            allocator
                .intersperse(self.insts.iter(), allocator.hardline())
                .append(allocator.hardline())
                .append(&self.term)
        }
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Label
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        match self {
            Label::Return(ret) => ret.pretty(allocator),
            Label::Jump(jump) => jump.pretty(allocator),
            Label::Switch(switch) => switch.pretty(allocator),
        }
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Jump
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        allocator
            .text("#jmp")
            .append(allocator.line())
            .append("'")
            .append(&*self.target.inner)
            .append(allocator.line())
            .append(allocator.intersperse(&self.args, allocator.line()))
            .group()
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Switch
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        let switch_body = allocator
            .line()
            .append(allocator.intersperse(&self.branches, allocator.text(",") + allocator.line()))
            .nest(2);
        allocator
            .text("#switch")
            .append(allocator.line())
            .append(&*self.disc)
            .append(allocator.space())
            .group()
            .append("{")
            .append(switch_body.append(allocator.line()).append("}").group())
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Branch
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        let pattern_arrow = (self.0.pretty(allocator) + allocator.line() + "=>").group();
        let result = self.1.pretty(allocator).group();
        pattern_arrow
            .append(allocator.line())
            .append(result)
            .nest(2)
            .group()
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Pattern
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        match self {
            Pattern::Bitvector(b) => b.pretty(allocator),
            Pattern::Wildcard => allocator.text("_"),
        }
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Assign
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        (self.0.pretty(allocator) + allocator.line() + "=" + allocator.line() + &self.1 + ";")
            .group()
    }
}

impl<'a, D, A> Pretty<'a, D, A> for Atom<&'a Names>
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        match self.0 {
            Names::Product(_) => self.0.pretty(allocator),
            _ => self.0.pretty(allocator).parens().group(),
        }
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Names
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        match self {
            //TODO: identifier escaping
            Names::Ident(ident) => allocator.text(&**ident),
            Names::Product(prod) => allocator
                .intersperse(prod, allocator.text(",") + allocator.line())
                .parens()
                .nest(2)
                .group(),
            Names::Annot(annot) => {
                (annot.0.pretty(allocator) + allocator.line() + ":" + allocator.line() + &annot.1)
                    .group()
            }
        }
    }
}

impl<'a, D, A> Pretty<'a, D, A> for &'a Bitvector
where
    D: DocAllocator<'a, A>,
    D::Doc: Clone,
    A: 'a + Clone,
{
    fn pretty(self, allocator: &'a D) -> DocBuilder<'a, D, A> {
        allocator.text(format!("{self}"))
    }
}

mod hack {
    use pretty::{DocAllocator, DocBuilder, PrettyFmt};

    /// Get an object implementing `Display` to prettyprint a document with a given width
    ///
    /// This hack is necessary because the `Pretty` trait shadows the `pretty` method on `DocBuilder`
    pub fn pretty_fmt<'a, 'd, A, D>(
        d: &'d DocBuilder<'a, D, A>,
        width: usize,
    ) -> PrettyFmt<'a, 'd, D::Doc, A>
    where
        D: DocAllocator<'a, A>,
        D::Doc: Clone,
        A: 'a + Clone,
    {
        d.pretty(width)
    }
}

pub use hack::pretty_fmt;

/// Prettyprint an object into a string of a given width
pub fn pretty_string<'a, P>(obj: P, width: usize) -> String
where
    P: Pretty<'a, RcAllocator, ()>,
{
    let doc = obj.pretty(&RcAllocator);
    let pretty = pretty_fmt(&doc, width);
    format!("{pretty}")
}

#[cfg(test)]
mod test {
    use nom::sequence::delimited;

    use super::*;
    use crate::parser::{decl, ws_};

    #[test]
    fn tuple_edge_cases() {
        // Single tuples
        let single_tuple = Expr::Tuple(vec![Spanned::new(Expr::ident("x"), 0..1)]);
        assert_eq!(pretty_string(&single_tuple, 4), "(x,)");
        // Paired tuples
        let paired_tuple = Expr::Tuple(vec![
            Spanned::new(Expr::ident("x"), 0..1),
            Spanned::new(Expr::ident("y"), 2..3),
        ]);
        assert_eq!(pretty_string(&paired_tuple, 6), "(x, y)"); // Note: spacing and no trailing comma!
        assert_eq!(pretty_string(&paired_tuple, 4), "(x,\n  y)");
    }

    #[test]
    fn pretty_round_trip() {
        let decls = [
            "#const x: #b64 = 354;",
            "#type u64 = #b64;",
            "#type vec3 = (#b64, b64, b64);",
            "#const v: vec3 = (30, 64, 6443);",
            "#type u64or8 = {#b64, #b8};",
            "#type vec3' = [#b64; 3];",
            "#const u: vec3' = [30, 64, 6443];",
            "#fn f(d: #b64) -> #b64 {
                #switch d { 0 => 45, 1 => 435, 3 => #abort }
            }
            ",
            "#fn f(d: #b64) -> #b64 =
                #switch d { 0 => 45, 1 => 435, 3 => #abort };
            ",
            "#fn f(d: #b64) -> #b64 =
                #switch d { 0 => g d, 1 => 435, 3 => #undef };
            ",
            "#fn loop(d: #b64) -> #b64 {
            'entry:
                #jmp 'entry
            }
            ",
            "#fn fact(n: #b64) -> #b64 {
                #jmp 'loop 2 1
            'loop(i: #b64, a: #b64):
                #switch (le i n) {
                    0 => a,
                    1 => #jmp 'loop (add i 1) (mul i a)
                }
            }
            ",
            "#fn fact'(n: #b64) -> #b64 {
                #jmp 'loop 2 1
            'loop(i: #b64, a: #b64):
                #switch (le i n) {
                    0 => a,
                    1 =>         
                        a' = mul i a;
                        i' = add i 1; 
                        #jmp 'loop i' a'
                }
            }
            ",
            "#fn is_zero(b: #b64) -> #b64 {
                #switch b {
                    0 => 1,
                    _ => 0
                }
            }
            ",
        ];
        let widths = [1, 10, 80, 120];
        for d0 in decls {
            let (rest, d1) = delimited(ws_, decl, ws_)(d0.into()).unwrap();
            assert_eq!(*rest.fragment(), "");
            for width in widths {
                let d2 = pretty_string(&d1, width);
                let (rest, d3) = delimited(ws_, decl, ws_)((&*d2).into()).expect(&*d2);
                assert_eq!(*rest.fragment(), "");
                let d4 = pretty_string(&d3, width);
                assert_eq!(d2, d4);
                assert!(d1.eq_mod(&d3), "\n\n{d1:#?}\n\n{d3:#?}");
            }
        }
    }
}
