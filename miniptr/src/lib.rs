/*!
Data-structures based on small, safe indices into arenas ("mini pointers")
*/
#![forbid(unsafe_code, missing_debug_implementations, missing_docs)]
use either::Either;

pub mod index;
pub mod list;
pub mod map;
pub mod slab;
pub mod slot;

pub use index::*;
pub use list::*;
pub use map::*;
pub use slab::*;
pub use slot::*;
