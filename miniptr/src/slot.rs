/*!
Traits for slots in arena-based data structures
*/
use either::Either;

/// A value type which can be freed
pub trait Freeable<K>: Sized {
    /// This slot's value type
    type Value;

    /// Create a slot from a slot value
    fn from_slot_value(value: Self::Value) -> Self;

    /// Create a slot from a key
    fn from_key(key: K) -> Self;

    /// Get this slot's value, if it has one
    ///
    /// Panics or returns an arbitrary value otherwise
    fn into_slot_value(self) -> Self::Value;

    /// Get a reference to this slot's value, if it has one
    ///
    /// Panics or returns an arbitrary value otherwise
    fn slot_value(&self) -> &Self::Value;

    /// Get a mutable reference to this slot's value, if it has one
    ///
    /// Panics or returns an arbitrary value otherwise
    fn slot_value_mut(&mut self) -> &mut Self::Value;

    /// Get next slot, if any, in the free list if this value is a free slot
    ///
    /// Panic or return an arbitrary value otherwise
    fn next_free(&self) -> K;

    /// Create a slot from either a slot value or key
    #[inline]
    fn from_either(value: Either<Self::Value, K>) -> Self {
        value.either(Self::from_slot_value, Self::from_key)
    }

    /// Set this slot, returning the old value
    #[inline]
    fn set_slot(&mut self, new: Either<Self::Value, K>) -> Self {
        let mut new = Self::from_either(new);
        std::mem::swap(self, &mut new);
        new
    }

    /// Set this slot to a value, returning the old value
    #[inline]
    fn set_value(&mut self, new: Self::Value) -> Self {
        let mut new = Self::from_slot_value(new);
        std::mem::swap(self, &mut new);
        new
    }

    /// Set this slot to a key, returning the old value
    #[inline]
    fn set_key(&mut self, new: K) -> Self {
        let mut new = Self::from_key(new);
        std::mem::swap(self, &mut new);
        new
    }
}

impl<V, K> Freeable<K> for Either<V, K>
where
    K: Clone,
{
    type Value = V;

    #[inline(always)]
    fn from_slot_value(value: Self::Value) -> Self {
        Self::Left(value)
    }

    #[inline(always)]
    fn from_key(key: K) -> Self {
        Self::Right(key)
    }

    #[inline(always)]
    fn into_slot_value(self) -> Self::Value {
        self.left().unwrap()
    }

    #[inline(always)]
    fn slot_value(&self) -> &Self::Value {
        self.as_ref().left().unwrap()
    }

    #[inline(always)]
    fn slot_value_mut(&mut self) -> &mut Self::Value {
        self.as_mut().left().unwrap()
    }

    #[inline(always)]
    fn next_free(&self) -> K {
        self.as_ref().right().unwrap().clone()
    }    
}
