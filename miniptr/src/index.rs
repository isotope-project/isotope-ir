/*!
Traits for indices in arena-based data structures
*/
/// A type wrapping an integer index, for use in an arena-based data structure
pub trait EntityIx: Copy + Eq {
    /// Create a new index from a small integer in a contiguous range `0..n`.
    ///
    /// This should return `None` if the index is not representable.
    fn try_new(ix: usize) -> Option<Self>;
    /// Get the index used to create this entity reference
    ///
    /// If `Self::try_new(n) = Some(i)`, then `i.index()` should return `n`
    fn index(self) -> usize;
    /// Create a new index from a small integer in a contiguous range `0..n`.
    ///
    /// This should return crash if the index is not representable.
    #[inline(always)]
    fn new(ix: usize) -> Self {
        Self::try_new(ix).expect("index not representable")
    }
    /// Whether this index is zero
    ///
    /// Should be equivalent to `self == K::new(0)`
    #[inline(always)]
    fn is_zero(self) -> bool {
        self == Self::new(0)
    }
}

/// A type wrapping an integer index with a reserved value
pub trait ReservedValue {
    /// Get this type's reserved value
    /// 
    /// In particular, this should *not* be possible to construct with [`EntityIx`]
    fn reserved() -> Self;

    /// Check whether this value is a reserved value
    fn is_reserved(&self) -> bool;
}

impl EntityIx for u8 {
    #[inline(always)]
    fn try_new(ix: usize) -> Option<Self> {
        //NOTE: we do *not* reserve the `u8::MAX` bit pattern in this implementation
        if ix <= u8::MAX as usize {
            Some(ix as u8)
        } else {
            None
        }
    }

    #[inline(always)]
    fn index(self) -> usize {
        self as usize
    }
}

impl EntityIx for u16 {
    #[inline(always)]
    fn try_new(ix: usize) -> Option<Self> {
        //NOTE: we do *not* reserve the `u16::MAX` bit pattern in this implementation
        if ix <= u16::MAX as usize {
            Some(ix as u16)
        } else {
            None
        }
    }

    #[inline(always)]
    fn index(self) -> usize {
        self as usize
    }
}

impl EntityIx for u32 {
    #[inline(always)]
    fn try_new(ix: usize) -> Option<Self> {
        //NOTE: we do *not* reserve the `u32::MAX` bit pattern in this implementation
        if ix <= u32::MAX as usize {
            Some(ix as u32)
        } else {
            None
        }
    }

    #[inline(always)]
    fn index(self) -> usize {
        self as usize
    }
}

impl EntityIx for u64 {
    #[inline(always)]
    fn try_new(ix: usize) -> Option<Self> {
        //NOTE: we do *not* reserve the `u64::MAX` bit pattern in this implementation
        if ix <= u64::MAX as usize {
            Some(ix as u64)
        } else {
            None
        }
    }

    #[inline(always)]
    fn index(self) -> usize {
        self as usize
    }
}

impl EntityIx for usize {
    #[inline(always)]
    fn try_new(ix: usize) -> Option<Self> {
        //NOTE: we do *not* reserve the `usize::MAX` bit pattern in this implementation
        Some(ix)
    }

    #[inline(always)]
    fn index(self) -> usize {
        self
    }
}

/// A type wrapping entity indices which are small primitive integers
pub trait SmallInt: EntityIx {}

impl SmallInt for u16 {}

impl SmallInt for u32 {}

impl SmallInt for u64 {}

impl SmallInt for usize {}

/// Macro which provides the common implementation of an n-bit entity reference
///
/// Based on [`cranelift_entity`'s `entity_impl!`](https://docs.rs/cranelift-entity/0.89.2/cranelift_entity/macro.entity_impl.html)
#[macro_export]
macro_rules! entity_impl {
    ($entity:ident, $backing:ty, $reserved_max:expr) => {
        impl $crate::EntityIx for $entity {
            #[inline(always)]
            fn try_new(ix: usize) -> Option<Self> {
                if 
                ($reserved_max && ix < (<$backing>::MAX as usize)) ||
                (!$reserved_max && ix <= (<$backing>::MAX as usize)) 
                || (<$backing>::BITS) > usize::BITS {
                    Some($entity(ix as $backing))
                } else {
                    None
                }
            }

            #[inline(always)]
            fn index(self) -> usize {
                self.0 as usize
            }
        }
    };
}

/// Macro which marks the maximum value of an [`EntityIx`] as reserved
#[macro_export]
macro_rules! reserved_max_impl {
    ($entity:ident, $backing:ty) => {
        impl $crate::ReservedValue for $entity {
            #[inline(always)]
            fn reserved() -> Self {
                Self(<$backing>::MAX)
            }

            #[inline(always)]
            fn is_reserved(&self) -> bool {
                self.0 == <$backing>::MAX
            }
        }
    };
}
