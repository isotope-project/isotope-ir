/*!
Maps represented by small indices into a memory pool
*/
use std::{fmt::Debug, hash::Hash, marker::PhantomData};

use crate::*;

/// A map of `T` allocated from a pool
pub struct EntityMap<K, V, I = u32> {
    ix: I,
    data: PhantomData<(K, V)>,
}

impl<K, V, I> EntityMap<K, V, I> {}

impl<K, V, I: Debug> Debug for EntityMap<K, V, I> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("MIx").field(&self.ix).finish()
    }
}

impl<K, V, I: EntityIx> Default for EntityMap<K, V, I> {
    #[inline(always)]
    fn default() -> Self {
        Self {
            ix: I::new(0),
            data: Default::default(),
        }
    }
}

impl<K, V, I: Copy> Copy for EntityMap<K, V, I> {}

impl<K, V, I: Clone> Clone for EntityMap<K, V, I> {
    #[inline(always)]
    fn clone(&self) -> Self {
        Self {
            ix: self.ix.clone(),
            data: self.data.clone(),
        }
    }
}

impl<K, V, I: PartialEq> PartialEq for EntityMap<K, V, I> {
    #[inline(always)]
    fn eq(&self, other: &Self) -> bool {
        self.ix == other.ix
    }
}

impl<K, V, I: Eq> Eq for EntityMap<K, V, I> {}

impl<K, V, I: PartialOrd> PartialOrd for EntityMap<K, V, I> {
    #[inline(always)]
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.ix.partial_cmp(&other.ix)
    }
}

impl<K, V, I: Ord> Ord for EntityMap<K, V, I> {
    #[inline(always)]
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.ix.cmp(&other.ix)
    }
}

impl<K, V, I: Hash> Hash for EntityMap<K, V, I> {
    #[inline(always)]
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.ix.hash(state);
    }
}

/// A memory pool for storing maps from `K` to `V` with keys `I`
///
/// We maintain the invariant that, if `K` implements [`EntityIx`], `K::new(0)` stands for an instance of the empty map which cannot be de-allocated
pub trait MapPool<K, V, I: Copy> {
    /// Allocate a new, empty map with a given size hint
    ///
    /// Note that the result is *not* generally guaranteed to have `size_hint` capacity, though a particular implementor may do so.
    ///
    /// Return an error if there is no new capacity
    fn allocate(&mut self, size_hint: usize) -> Result<K, ()>;

    /// Allocate a new, *unique* empty map with a given size hint
    ///
    /// Note that the result is *not* generally guaranteed to have `size_hint` capacity, though a particular implementor may do so.
    ///
    /// Return an error if there is no new capacity, or if this allocator does not support unique empty allocations
    /// (e.g., if the invariant is maintained that all empty maps are represented by `K::new(0)`).
    fn allocate_unique(&mut self, size_hint: usize) -> Result<K, ()>;

    /// Re-allocate a map, aiming for a given size.
    ///
    /// On success, return's the map's new index.
    ///
    /// The resulting capacity should be greater than `capacity`, but this is *not* guaranteed.
    ///
    /// Return an error on allocation failure
    fn reallocate(&mut self, id: I, capacity: usize) -> Result<K, ()>;

    /// Try to re-allocate a map, aiming for a given size, without performing any copying.
    ///
    /// The resulting capacity should be greater than `capacity`, but this is *not* guaranteed.
    ///
    /// Return an error on allocation failure
    fn try_reallocate(&mut self, id: I, capacity: usize) -> Result<(), ()>;

    /// Reserve `capacity` additional capacity for this map.
    ///
    /// On success, return's the map's new index.
    ///
    /// Return an error on allocation failure
    #[inline]
    fn reserve(&mut self, id: I, capacity: usize) -> Result<K, ()> {
        self.reallocate(id, self.get_len(id) + capacity)
    }

    /// Try to reserve `capacity` additional capacity for this map, without performing any copying.
    ///
    /// Return an error on allocation failure
    #[inline]
    fn try_reserve(&mut self, id: I, capacity: usize) -> Result<(), ()> {
        self.try_reallocate(id, self.get_len(id) + capacity)
    }

    /// De-allocate a map
    ///
    /// This is a no-op on `K::new(0)`
    ///
    /// Is never UB, but may return an unspecified but valid `MapPool` or panic if:
    /// - `id` has already been `deallocate`'d
    /// - `id` was not returned by `allocate`
    fn deallocate(&mut self, id: I);

    /// Get the `ix`'th element of the list with the given `id`
    ///
    /// Returns `None` if `ix` is out of bounds.
    ///
    /// Is never UB, but may return an unspecified value or panic if:
    /// - `id` has already been `deallocate`'d
    /// - `id` was not returned by `allocate`
    fn get_elem(&self, id: I, key: &K) -> Option<&V>;

    /// Get the `ix`'th element of the list with the given `id`
    ///
    /// Is never UB, but may return an unspecified value or panic if:
    /// - `ix` is out of bounds
    /// - `id` has already been `deallocate`'d
    /// - `id` was not returned by `allocate`
    #[inline(always)]
    fn get_elem_unchecked(&self, id: I, key: &K) -> &V {
        self.get_elem(id, key).unwrap()
    }

    /// Get a mutable reference to the `ix`'th element of the list with the given `id`
    ///
    /// Returns `None` if `ix` is out of bounds.
    ///
    /// Is never UB, but may return an unspecified value or panic if:
    /// - `id` has already been `deallocate`'d
    /// - `id` was not returned by `allocate`
    fn get_elem_mut(&mut self, id: I, key: &K) -> Option<&mut V>;

    /// Get a mutable reference to the `ix`'th element of the allocation with the given `id`
    ///
    /// Is never UB, but may return an unspecified value or panic if:
    /// - `ix` is out of bounds
    /// - `id` has already been `deallocate`'d
    /// - `id` was not returned by `allocate`
    #[inline(always)]
    fn get_elem_unchecked_mut(&mut self, id: I, key: &K) -> &mut V {
        self.get_elem_mut(id, key).unwrap()
    }

    /// Get the length of the list with the given `id`
    ///
    /// Is never UB, but may return an unspecified value or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    fn get_len(&self, id: I) -> usize;

    /// Get the capacity of the list with the given `id`
    ///
    /// Is never UB, but may return an unspecified value or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    fn get_capacity(&self, id: I) -> usize {
        self.get_len(id)
    }

    /// Empty a map, *potentially* preserving it's capacity, and return an index to an empty list
    ///
    /// Is never UB, but may return an unspecified but valid `MapPool` or panic if:
    /// - `id` has already been `deallocate`'d
    /// - `id` was not returned by `allocate`
    fn clear_map(&mut self, id: I, preserve_capacity: bool) -> K;

    /// Empty a map, *potentially* preserving it's capacity, without moving it. Returns an error if the list must be moved.
    ///
    /// Is never UB, but may return an unspecified but valid `MapPool` or panic if:
    /// - `id` has already been `deallocate`'d
    /// - `id` was not returned by `allocate`
    fn try_clear(&mut self, id: I, preserve_capacity: bool) -> Result<(), ()>;

    /// Insert an element into this map, returning the new allocation index and the old value, if any.
    ///
    /// Returns the provided `(key, value)` pair on failure
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `MapPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    fn insert(&mut self, id: I, key: K, value: V) -> Result<(I, Option<(K, V)>), (K, V)>;

    /// Insert an element into this map without moving it, returning the old value, if any
    ///
    /// Returns the provided `(key, value)` pair on failure
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `MapPool`, or panic if:
    /// - `id` was not returned by `allocate`
    fn try_insert(&mut self, id: I, key: K, value: V) -> Result<Option<(K, V)>, (K, V)>;

    /// Removes and returns the element with then supplied key, returning the new index.
    ///
    /// Returns `None` if the map does not contain an element with the desired key
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    fn remove(&mut self, id: I, key: &K) -> (I, Option<(K, V)>);

    /// Tries to remove and returns the element with the supplied key, without moving the map
    ///
    /// Fails if it is necessary to move the allocation.
    ///
    /// Returns `Ok(None)` if the map does not contain an element with the desired key
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    fn try_remove(&mut self, id: I, key: &K) -> Result<Option<(K, V)>, ()>;
}
