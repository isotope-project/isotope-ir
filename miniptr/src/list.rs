/*!
Lists represented by small indices into a memory pool
*/
use std::{fmt::Debug, hash::Hash, marker::PhantomData, ops::RangeBounds};

use crate::*;

/// A list of `T` allocated from a pool
#[repr(transparent)]
pub struct EntityList<T, K = u32> {
    ix: K,
    data: PhantomData<T>,
}

impl<T, K: Debug> Debug for EntityList<T, K> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("LIx").field(&self.ix).finish()
    }
}

impl<K: EntityIx, T> Default for EntityList<T, K> {
    #[inline(always)]
    fn default() -> Self {
        Self {
            ix: K::new(0),
            data: Default::default(),
        }
    }
}

impl<K: EntityIx, T> EntityList<T, K> {
    /// Create a new, empty list
    #[inline(always)]
    pub fn new() -> Self {
        EntityList {
            ix: K::new(0),
            data: PhantomData,
        }
    }
}

impl<K: Copy, T> EntityList<T, K> {
    /// Create a new, empty list with the given capacity
    ///
    /// Note that the capacity is not guaranteed!
    ///
    /// Returns an error on allocation failure
    #[inline]
    pub fn with_capacity(size_hint: usize, pool: &mut impl ListPool<T, K>) -> Result<Self, ()> {
        Ok(EntityList {
            ix: pool.allocate(size_hint)?,
            data: PhantomData,
        })
    }

    /// Reserve space for at least `capacity` elements total
    ///
    /// Note that the capacity is not guaranteed!
    ///
    /// Returns an error on allocation failure
    #[inline(always)]
    pub fn reallocate(&mut self, capacity: usize, pool: &mut impl ListPool<T, K>) -> Result<(), ()> {
        self.ix = pool.reallocate(self.ix, capacity)?;
        Ok(())
    }

    /// Try to reserve space for at least `capacity` elements total without moving this allocation
    ///
    /// Note that the capacity is not guaranteed!
    ///
    /// Returns an error on allocation failure
    #[inline(always)]
    pub fn try_reallocate(&self, capacity: usize, pool: &mut impl ListPool<T, K>) -> Result<(), ()> {
        pool.reallocate(self.ix, capacity)?;
        Ok(())
    }

    /// Reserve space for `capacity` additional elements
    ///
    /// Note that the capacity is not guaranteed!
    ///
    /// Returns an error on allocation failure
    #[inline(always)]
    pub fn reserve(&mut self, capacity: usize, pool: &mut impl ListPool<T, K>) -> Result<(), ()> {
        self.ix = pool.reserve(self.ix, capacity)?;
        Ok(())
    }

    /// Try to reserve space for `capacity` additional elements without moving this allocation
    ///
    /// Note that the capacity is not guaranteed!
    ///
    /// Returns an error on allocation failure
    #[inline(always)]
    pub fn try_reserve(&self, capacity: usize, pool: &mut impl ListPool<T, K>) -> Result<(), ()> {
        pool.try_reserve(self.ix, capacity)
    }

    /// Create a new list, with the contents initialized from an iterator
    ///
    /// Returns what is left of the iterator on allocation failure
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// let v = EntityList::from_iter([5, 6, 7, 8], &mut pool).unwrap();
    /// assert_eq!(v.as_slice(&pool), [5, 6, 7, 8])
    /// ```
    #[inline]
    pub fn from_iter<I: IntoIterator<Item = T>>(
        iter: I,
        pool: &mut impl ListPool<T, K>,
    ) -> Result<Self, I::IntoIter> {
        Ok(EntityList {
            ix: pool.allocate_from_iter(iter.into_iter())?,
            data: PhantomData,
        })
    }

    /// Create a new list, with the contents initialized from a slice
    ///
    /// Returns an error on allocation failure
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// let v = EntityList::from_slice(&[4, 3, 2, 1], &mut pool).unwrap();
    /// assert_eq!(v.as_slice(&pool), [4, 3, 2, 1])
    /// ```
    #[inline]
    pub fn from_slice(slice: &[T], pool: &mut impl ListPool<T, K>) -> Result<Self, ()>
    where
        T: Clone,
    {
        Ok(EntityList {
            ix: pool.allocate_from_slice(slice)?,
            data: PhantomData,
        })
    }

    /// Check whether this list is empty
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// assert!(EntityList::new().is_empty(&pool));
    /// let v = EntityList::from_slice(&[1], &mut pool).unwrap();
    /// assert!(!v.is_empty(&pool));
    /// let e = EntityList::from_slice(&[], &mut pool).unwrap();
    /// assert!(e.is_empty(&pool));
    /// ```
    #[inline(always)]
    pub fn is_empty(&self, pool: &impl ListPool<T, K>) -> bool {
        pool.is_empty(self.ix)
    }

    /// Get the number of elements in this list
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// assert_eq!(EntityList::new().len(&pool), 0);
    /// assert_eq!(
    ///     EntityList::from_slice(&[0, 1, 2], &mut pool).unwrap().len(&pool),
    ///     3
    /// );
    /// ```
    #[inline(always)]
    pub fn len(&self, pool: &impl ListPool<T, K>) -> usize {
        pool.get_len(self.ix)
    }

    /// Get the capacity of this list
    #[inline(always)]
    pub fn get_capacity(&self, pool: &impl ListPool<T, K>) -> usize {
        pool.get_capacity(self.ix)
    }

    /// Get a reference to an element of this list, returning `None` if `ix` is out of bounds
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// let v = EntityList::from_slice(&[4, 3, 2, 1], &mut pool).unwrap();
    /// assert_eq!(v.get(1, &pool), Some(&3));
    /// assert_eq!(v.get(4, &pool), None);
    /// ```
    #[inline(always)]
    pub fn get<'a>(&self, ix: usize, pool: &'a impl ListPool<T, K>) -> Option<&'a T> {
        pool.get_elem(self.ix, ix)
    }

    /// Get a mutable reference to an element of this list, returning `None` if `ix` is out of bounds
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// let v = EntityList::from_slice(&[4, 3, 2, 1], &mut pool).unwrap();
    /// assert_eq!(v.get_mut(1, &mut pool), Some(&mut 3));
    /// *v.get_mut(1, &mut pool).unwrap() = 7;
    /// assert_eq!(v.get_mut(4, &mut pool), None);
    /// assert_eq!(v.get(1, &pool), Some(&7));
    /// ```
    #[inline(always)]
    pub fn get_mut<'a>(&self, ix: usize, pool: &'a mut impl ListPool<T, K>) -> Option<&'a mut T> {
        pool.get_elem_mut(self.ix, ix)
    }

    /// Get an element of this list, returning an unspecified value or panicking if `ix` is out of bounds
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// let v = EntityList::from_slice(&[4, 3, 2, 1], &mut pool).unwrap();
    /// assert_eq!(v.get_unchecked(1, &pool), &3);
    /// ```
    #[inline(always)]
    pub fn get_unchecked<'a>(&self, ix: usize, pool: &'a impl ListPool<T, K>) -> &'a T {
        pool.get_elem_unchecked(self.ix, ix)
    }

    /// Get a mutable reference to an element of this list, returning an unspecified value or panicking if `ix` is out of bounds
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// let v = EntityList::from_slice(&[4, 3, 2, 1], &mut pool).unwrap();
    /// assert_eq!(v.get_unchecked_mut(1, &mut pool), &mut 3);
    /// ```
    #[inline(always)]
    pub fn get_unchecked_mut<'a>(&self, ix: usize, pool: &'a mut impl ListPool<T, K>) -> &'a mut T {
        pool.get_elem_unchecked_mut(self.ix, ix)
    }

    /// Get this list as a slice
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// assert_eq!(EntityList::new().as_slice(&pool), &[]);
    /// let v = EntityList::from_slice(&[4, 3, 2, 1], &mut pool).unwrap();
    /// assert_eq!(v.as_slice(&pool),  &[4, 3, 2, 1]);
    /// ```
    #[inline(always)]
    pub fn as_slice<'a>(&self, pool: &'a impl ListPool<T, K>) -> &'a [T] {
        pool.get_slice(self.ix)
    }

    /// Get this list as a mutable slice
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// assert_eq!(EntityList::new().as_slice_mut(&mut pool), &mut []);
    /// let v = EntityList::from_slice(&[4, 3, 2, 1], &mut pool).unwrap();
    /// assert_eq!(v.as_slice_mut(&mut pool),  &mut [4, 3, 2, 1]);
    /// v.as_slice_mut(&mut pool).reverse();
    /// assert_eq!(v.as_slice(&pool),  &[1, 2, 3, 4]);
    /// ```
    #[inline]
    pub fn as_slice_mut<'a>(&self, pool: &'a mut impl ListPool<T, K>) -> &'a mut [T] {
        pool.get_slice_mut(self.ix)
    }

    /// Create a deep clone of the list, which does not alias the original list
    ///
    /// Should have behaviour equivalent to `Self::from_slice(self.as_slice(pool), pool)` would theoretically have.
    /// This does not compile since `self.as_slice` needs to borrow `pool` immutably, whereas `self.from_slice` needs
    /// to borrow `pool` mutably.
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// assert_eq!(EntityList::new().as_slice_mut(&mut pool), &mut []);
    /// let v = EntityList::from_slice(&[4, 3, 2, 1], &mut pool).unwrap();
    /// let u = v.deep_clone(&mut pool).unwrap();
    /// assert_eq!(v.as_slice(&pool), &[4, 3, 2, 1]);
    /// assert_eq!(u.as_slice(&pool),  &[4, 3, 2, 1]);
    /// assert_ne!(v.as_ptr(&pool), u.as_ptr(&pool));
    /// ```
    #[inline]
    pub fn deep_clone(&self, pool: &mut impl ListPool<T, K>) -> Result<Self, ()>
    where
        T: Clone,
    {
        Ok(EntityList {
            ix: pool.deep_clone(self.ix)?,
            data: PhantomData,
        })
    }

    /// Extend the array with the contents of an iterator
    ///
    /// On failure, return what is left of the input iterator
    ///
    /// May spuriously fail, panic or over-allocate if the iterator's size hint is incorrect, but will never return a wrong result.
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// assert_eq!(EntityList::new().as_slice_mut(&mut pool), &mut []);
    /// let mut v = EntityList::from_slice(&[1, 2, 3], &mut pool).unwrap();
    /// assert_eq!(v.as_slice(&pool), &[1, 2, 3]);
    /// v.extend(4..=8, &mut pool);
    /// assert_eq!(v.as_slice(&pool), &[1, 2, 3, 4, 5, 6, 7, 8]);
    /// ```
    #[inline(always)]
    pub fn extend<I: IntoIterator<Item = T>>(
        &mut self,
        iter: I,
        pool: &mut impl ListPool<T, K>,
    ) -> Result<(), I::IntoIter> {
        match pool.extend(self.ix, iter.into_iter()) {
            Ok(ix) => {
                self.ix = ix;
                Ok(())
            }
            Err((ix, iter)) => {
                self.ix = ix;
                Err(iter)
            }
        }
    }

    /// Try to extend the array with the contents of an iterator without moving it
    ///
    /// On failure, return what is left of the input iterator
    ///
    /// May spuriously fail, panic or over-allocate if the iterator's size hint is incorrect, but will never return a wrong result.
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// assert_eq!(EntityList::new().as_slice_mut(&mut pool), &mut []);
    /// let mut v = EntityList::from_slice(&[1, 2, 3], &mut pool).unwrap();
    /// assert_eq!(v.as_slice(&pool), &[1, 2, 3]);
    /// v.reserve(5, &mut pool).unwrap();
    /// v.try_extend(4..=8, &mut pool).unwrap();
    /// assert_eq!(v.as_slice(&pool), &[1, 2, 3, 4, 5, 6, 7, 8]);
    /// ```
    #[inline(always)]
    pub fn try_extend<I: IntoIterator<Item = T>>(
        &self,
        iter: I,
        pool: &mut impl ListPool<T, K>,
    ) -> Result<(), I::IntoIter> {
        pool.try_extend(self.ix, iter.into_iter())
    }

    /// Extend the array with the contents of a slice
    ///
    /// On failure, return what is left of the input iterator
    ///
    /// May spuriously fail, panic or over-allocate if the iterator's size hint is incorrect, but will never return a wrong result.
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// assert_eq!(EntityList::new().as_slice_mut(&mut pool), &mut []);
    /// let mut v = EntityList::from_slice(&[1, 2, 3], &mut pool).unwrap();
    /// assert_eq!(v.as_slice(&pool), &[1, 2, 3]);
    /// v.extend_from_slice(&[4, 5, 6, 7, 8], &mut pool);
    /// assert_eq!(v.as_slice(&pool), &[1, 2, 3, 4, 5, 6, 7, 8]);
    /// ```
    #[inline(always)]
    pub fn extend_from_slice(
        &mut self,
        slice: &[T],
        pool: &mut impl ListPool<T, K>,
    ) -> Result<(), ()>
    where
        T: Clone,
    {
        match pool.extend_from_slice(self.ix, slice) {
            Ok(ix) => {
                self.ix = ix;
                Ok(())
            }
            Err(ix) => {
                self.ix = ix;
                Err(())
            }
        }
    }

    /// Try to extend the array with the contents of a slice without moving it
    ///
    /// On failure, return what is left of the input iterator
    ///
    /// May spuriously fail, panic or over-allocate if the iterator's size hint is incorrect, but will never return a wrong result.
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// assert_eq!(EntityList::new().as_slice_mut(&mut pool), &mut []);
    /// let mut v = EntityList::from_slice(&[1, 2, 3], &mut pool).unwrap();
    /// assert_eq!(v.as_slice(&pool), &[1, 2, 3]);
    /// v.reserve(5, &mut pool).unwrap();
    /// v.try_extend_from_slice(&[4, 5, 6, 7, 8], &mut pool).unwrap();
    /// assert_eq!(v.as_slice(&pool), &[1, 2, 3, 4, 5, 6, 7, 8]);
    /// ```
    #[inline(always)]
    pub fn try_extend_from_slice(
        &self,
        slice: &[T],
        pool: &mut impl ListPool<T, K>,
    ) -> Result<(), ()>
    where
        T: Clone,
    {
        pool.try_extend_from_slice(self.ix, slice)
    }

    /// Copy elements from the `src` range of `other` to the end of this list
    ///
    /// May spuriously fail, panic or over-allocate if the iterator's size hint is incorrect, but will never return a wrong result.
    #[inline(always)]
    pub fn extend_from_range(
        &mut self,
        other: Self,
        src: impl RangeBounds<usize>,
        pool: &mut impl ListPool<T, K>,
    ) -> Result<(), ()>
    where
        T: Clone,
    {
        let begin = match src.start_bound() {
            std::ops::Bound::Included(n) => *n,
            std::ops::Bound::Excluded(n) => *n,
            std::ops::Bound::Unbounded => 0,
        };
        let end = match src.end_bound() {
            std::ops::Bound::Included(n) => *n + 1,
            std::ops::Bound::Excluded(n) => *n,
            std::ops::Bound::Unbounded => self.len(pool),
        };
        match pool.extend_from_range(self.ix, other.ix, begin, end) {
            Ok(ix) => {
                self.ix = ix;
                Ok(())
            }
            Err(ix) => {
                self.ix = ix;
                Err(())
            }
        }
    }

    /// Try to copy elements from the `src` range of `other` the end of the list without moving the list
    ///
    /// May spuriously fail, panic or over-allocate if the iterator's size hint is incorrect, but will never return a wrong result.
    #[inline(always)]
    pub fn try_extend_from_range(
        &self,
        other: Self,
        src: impl RangeBounds<usize>,
        pool: &mut impl ListPool<T, K>,
    ) -> Result<(), ()>
    where
        T: Clone,
    {
        let begin = match src.start_bound() {
            std::ops::Bound::Included(n) => *n,
            std::ops::Bound::Excluded(n) => *n,
            std::ops::Bound::Unbounded => 0,
        };
        let end = match src.end_bound() {
            std::ops::Bound::Included(n) => *n + 1,
            std::ops::Bound::Excluded(n) => *n,
            std::ops::Bound::Unbounded => self.len(pool),
        };
        pool.try_extend_from_range(self.ix, other.ix, begin, end)
    }

    /// Copy elements from the `src` range to the end of the list
    ///
    /// May spuriously fail, panic or over-allocate if the iterator's size hint is incorrect, but will never return a wrong result.
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// assert_eq!(EntityList::new().as_slice_mut(&mut pool), &mut []);
    /// let mut v = EntityList::from_slice(&[1, 2, 3], &mut pool).unwrap();
    /// assert_eq!(v.as_slice(&pool), &[1, 2, 3]);
    /// v.extend_from_within(1..3, &mut pool);
    /// assert_eq!(v.as_slice(&pool), &[1, 2, 3, 2, 3]);
    /// ```
    #[inline(always)]
    pub fn extend_from_within(
        &mut self,
        src: impl RangeBounds<usize>,
        pool: &mut impl ListPool<T, K>,
    ) -> Result<(), ()>
    where
        T: Clone,
    {
        self.extend_from_range(*self, src, pool)
    }

    /// Try to copy elements from the `src` range to the end of the list without moving the list
    ///
    /// May spuriously fail, panic or over-allocate if the iterator's size hint is incorrect, but will never return a wrong result.
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// assert_eq!(EntityList::new().as_slice_mut(&mut pool), &mut []);
    /// let mut v = EntityList::from_slice(&[1, 2, 3], &mut pool).unwrap();
    /// assert_eq!(v.as_slice(&pool), &[1, 2, 3]);
    /// v.reserve(2, &mut pool).unwrap();
    /// v.try_extend_from_within(1..3, &mut pool).unwrap();
    /// assert_eq!(v.as_slice(&pool), &[1, 2, 3, 2, 3]);
    /// ```
    #[inline(always)]
    pub fn try_extend_from_within(
        &self,
        src: impl RangeBounds<usize>,
        pool: &mut impl ListPool<T, K>,
    ) -> Result<(), ()>
    where
        T: Clone,
    {
        self.try_extend_from_range(*self, src, pool)
    }

    /// Insert an element to a specific index in the array
    ///
    /// Returns the element on failure, either due to index out of bounds or due to pool memory exhaustion
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// assert_eq!(EntityList::new().as_slice_mut(&mut pool), &mut []);
    /// let mut v = EntityList::from_slice(&[1, 2, 3], &mut pool).unwrap();
    /// assert_eq!(v.as_slice(&pool), &[1, 2, 3]);
    /// v.insert(1, 3, &mut pool);
    /// assert_eq!(v.as_slice(&pool), &[1, 3, 2, 3]);
    /// ```
    #[inline(always)]
    pub fn insert(&mut self, ix: usize, item: T, pool: &mut impl ListPool<T, K>) -> Result<(), T> {
        self.ix = pool.insert(self.ix, ix, item)?;
        Ok(())
    }

    /// Insert an element to a specific index in the array
    ///
    /// Returns the element on failure, either due to index out of bounds or due to list capacity exhaustion
    #[inline(always)]
    pub fn try_insert(&self, ix: usize, item: T, pool: &mut impl ListPool<T, K>) -> Result<(), T> {
        pool.try_insert(self.ix, ix, item)
    }

    /// Get the array as a raw pointer
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// assert_eq!(EntityList::new().as_slice_mut(&mut pool), &mut []);
    /// let mut v = EntityList::from_slice(&[1, 2, 3], &mut pool).unwrap();
    /// assert_eq!(
    ///     v.as_slice(&pool) as *const _ as *const _,
    ///     v.as_ptr(&pool)
    /// );
    /// ```
    #[inline(always)]
    pub fn as_ptr(&self, pool: &impl ListPool<T, K>) -> *const T {
        self.as_slice(pool).as_ptr()
    }

    /// Get the array as a raw mutable pointer
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// assert_eq!(EntityList::new().as_slice_mut(&mut pool), &mut []);
    /// let mut v = EntityList::from_slice(&[1, 2, 3], &mut pool).unwrap();
    /// assert_eq!(
    ///     v.as_slice(&pool) as *const _ as *const _,
    ///     v.as_mut_ptr(&mut pool)
    /// );
    /// ```
    #[inline(always)]
    pub fn as_mut_ptr(&self, pool: &mut impl ListPool<T, K>) -> *mut T {
        self.as_slice_mut(pool).as_mut_ptr()
    }

    /// Removes and returns the element at the supplied index from the array, and shifts all elements after it to the left.
    ///
    /// Returns `None` if the array does not contain an element at the desired position
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// assert_eq!(EntityList::new().as_slice_mut(&mut pool), &mut []);
    /// let mut v = EntityList::from_slice(&[1, 2, 3], &mut pool).unwrap();
    /// assert_eq!(v.as_slice(&pool), &[1, 2, 3]);
    /// assert_eq!(v.remove(1, &mut pool), Some(2));
    /// assert_eq!(v.as_slice(&pool), &[1, 3]);
    /// ```
    #[inline(always)]
    pub fn remove(&mut self, ix: usize, pool: &mut impl ListPool<T, K>) -> Option<T> {
        let (ix, retv) = pool.remove(self.ix, ix)?;
        self.ix = ix;
        Some(retv)
    }

    /// Tries to remove and returns the element at the supplied index from the array, and shifts all elements after it to the left.
    ///
    /// Fails if it is necessary to move the allocation; otherwise has the same behaviour as `Self::remove`
    ///
    /// Returns `Ok(None)` if the array does not contain an element at the desired position
    #[inline(always)]
    pub fn try_remove(
        &mut self,
        ix: usize,
        pool: &mut impl ListPool<T, K>,
    ) -> Result<Option<T>, ()> {
        pool.try_remove(self.ix, ix)
    }

    /// Removes and returns the element at the supplied index from the array, and shifts all elements after it to the left.
    ///
    /// Should have exactly the same behaviour as `Self::remove`; the `StableRemovalPool` bound ensures that the allocation never needs to be moved.
    ///
    /// Returns `None` if the array does not contain an element at the desired position
    #[inline(always)]
    pub fn remove_inplace(&self, ix: usize, pool: &mut impl StableRemovalPool<T, K>) -> Option<T> {
        pool.remove_inplace(self.ix, ix)
    }

    /// Removes all elements from the list, returning the list's memory to the pool
    #[inline(always)]
    pub fn clear(&mut self, preserve_capacity: bool, pool: &mut impl ListPool<T, K>) {
        self.ix = pool.clear_list(self.ix, preserve_capacity);
    }

    /// Removes all elements from the list, returning the list's memory to the pool, without moving the list
    ///
    /// Fails if this cannot be done
    #[inline(always)]
    pub fn try_clear(
        &self,
        preserve_capacity: bool,
        pool: &mut impl ListPool<T, K>,
    ) -> Result<(), ()> {
        pool.try_clear(self.ix, preserve_capacity)
    }

    /// Removes all elements from the list, returning the list's memory to the pool
    #[inline(always)]
    pub fn clear_inplace(&self, preserve_capacity: bool, pool: &mut impl StableRemovalPool<T, K>) {
        pool.clear_inplace(self.ix, preserve_capacity)
    }

    /// Push an element to the back of this list.
    ///
    /// Return the element back as an error if the list *pool* is out of capacity (e.g., the list's length has overflowed `K`)
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::list::*;
    /// # let mut pool: IndexPool<u32> = IndexPool::new();
    /// assert_eq!(EntityList::new().as_slice_mut(&mut pool), &mut []);
    /// let mut v = EntityList::from_slice(&[1, 2, 3], &mut pool).unwrap();
    /// assert_eq!(v.as_slice(&pool), &[1, 2, 3]);
    /// v.push(4, &mut pool).unwrap();
    /// assert_eq!(v.as_slice(&pool), &[1, 2, 3, 4]);
    /// ```
    #[inline(always)]
    pub fn push(&mut self, element: T, pool: &mut impl ListPool<T, K>) -> Result<(), T> {
        self.ix = pool.push(self.ix, element)?;
        Ok(())
    }

    /// Try to push an element to the back of this list.
    ///
    /// Return the element back as an error if the *list* is out of capacity (i.e., we would need to change the index `K`)
    /// Note that this only requires an *immutable* borrow of `&self`, since the index never changes
    #[inline(always)]
    pub fn try_push(&self, element: T, pool: &mut impl ListPool<T, K>) -> Result<(), T> {
        pool.try_push(self.ix, element)
    }

    /// Pop the element at the back of this list, if any
    #[inline(always)]
    pub fn pop(&mut self, pool: &mut impl ListPool<T, K>) -> Option<T> {
        let (ix, retv) = pool.pop(self.ix)?;
        self.ix = ix;
        Some(retv)
    }

    /// Try to pop the element at the back of this list, if any, without moving the allocation
    #[inline(always)]
    pub fn try_pop(&self, pool: &mut impl ListPool<T, K>) -> Result<Option<T>, ()> {
        pool.try_pop(self.ix)
    }

    /// Truncate this list to the first `len` elements
    /// 
    /// If `len >= self.len(pool)`, this is a no-op
    #[inline(always)]
    pub fn truncate(&mut self, len: usize, pool: &mut impl ListPool<T, K>) {
        self.ix = pool.truncate(self.ix, len);
    }

    /// Try to truncate this list to the first `len` elements without moving it
    /// 
    /// If `len >= self.len(pool)`, this is a no-op
    #[inline(always)]
    pub fn try_truncate(&mut self, len: usize, pool: &mut impl ListPool<T, K>) -> Result<(), ()> {
        pool.try_truncate(self.ix, len)
    }

    /// Truncate this list to the first `len` elements without moving it
    /// 
    /// If `len >= self.len(pool)`, this is a no-op
    #[inline(always)]
    pub fn truncate_inplace(&mut self, len: usize, pool: &mut impl StableRemovalPool<T, K>) {
        pool.truncate_inplace(self.ix, len)
    }

    /// Pop the element at the back of this list, if any
    #[inline(always)]
    pub fn pop_inplace(&self, pool: &mut impl StableRemovalPool<T, K>) -> Option<T> {
        pool.pop_inplace(self.ix)
    }

    /// Check whether two lists are equal in the given pool
    #[inline(always)]
    pub fn eq_in(self, other: Self, pool: &mut impl ListPool<T, K>) -> bool
    where
        T: PartialEq,
    {
        pool.eq_in(self.ix, other.ix)
    }

    /// Shrink this list so as not to use any additional storage
    #[inline]
    pub fn shrink_to_fit(&mut self, pool: &mut impl ListPool<T, K>) {
        let _ = self.reallocate(self.len(pool), pool);
    }

    /// Try to shrink this list so as not to use any additional storage without moving it
    #[inline]
    pub fn try_shrink_to_fit(&mut self, pool: &mut impl ListPool<T, K>) -> Result<(), ()> {
        self.try_reallocate(self.len(pool), pool)
    }
}

impl<K: Clone, T> Clone for EntityList<T, K> {
    #[inline(always)]
    fn clone(&self) -> Self {
        Self {
            ix: self.ix.clone(),
            data: PhantomData,
        }
    }
}

impl<K: Copy, T> Copy for EntityList<T, K> {}

impl<K: PartialEq, T> PartialEq for EntityList<T, K> {
    #[inline(always)]
    fn eq(&self, other: &Self) -> bool {
        self.ix == other.ix
    }
}

impl<K: Eq, T> Eq for EntityList<T, K> {}

impl<K: PartialOrd, T> PartialOrd for EntityList<T, K> {
    #[inline(always)]
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.ix.partial_cmp(&other.ix)
    }
}

impl<K: Ord, T> Ord for EntityList<T, K> {
    #[inline(always)]
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.ix.cmp(&other.ix)
    }
}

impl<K: Hash, T> Hash for EntityList<T, K> {
    #[inline(always)]
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.ix.hash(state);
    }
}

impl<K: EntityIx, T> EntityIx for EntityList<T, K> {
    #[inline(always)]
    fn try_new(ix: usize) -> Option<Self> {
        if let Some(ix) = K::try_new(ix) {
            Some(EntityList {
                ix,
                data: PhantomData,
            })
        } else {
            None
        }
    }

    #[inline(always)]
    fn index(self) -> usize {
        self.ix.index()
    }

    #[inline(always)]
    fn new(ix: usize) -> Self {
        EntityList {
            ix: K::new(ix),
            data: PhantomData,
        }
    }
}

/// A memory pool for storing lists of `T` with keys `K`
///
/// We maintain the invariant that, if `K` implements [`EntityIx`], `K::new(0)` stands for an instance of the empty list which cannot be de-allocated
pub trait ListPool<T, K: Copy> {
    /// Allocate a new, empty list with a given size hint
    /// 
    /// Note that the result is *not* generally guaranteed to have `size_hint` capacity, though a particular implementor may do so.
    ///
    /// Return an error if there is no new capacity
    fn allocate(&mut self, size_hint: usize) -> Result<K, ()>;

    /// Allocate a new, *unique* empty list with a given size hint
    /// 
    /// Note that the result is *not* generally guaranteed to have `size_hint` capacity, though a particular implementor may do so.
    ///
    /// Return an error if there is no new capacity, or if this allocator does not support unique empty allocations
    /// (e.g., if the invariant is maintained that all empty lists are represented by `K::new(0)`).
    fn allocate_unique(&mut self, size_hint: usize) -> Result<K, ()>;

    /// Re-allocate a list, aiming for a given size.
    ///
    /// On success, return's the list's new index.
    ///
    /// The resulting capacity should be greater than `capacity`, but this is *not* guaranteed.
    ///
    /// Return an error on allocation failure
    fn reallocate(&mut self, id: K, capacity: usize) -> Result<K, ()>;

    /// Try to re-allocate a list, aiming for a given size, without performing any copying.
    ///
    /// The resulting capacity should be greater than `capacity`, but this is *not* guaranteed.
    ///
    /// Return an error on allocation failure
    fn try_reallocate(&mut self, id: K, capacity: usize) -> Result<(), ()>;

    /// Reserve `capacity` additional capacity for this list.
    ///
    /// On success, return's the list's new index.
    ///
    /// Return an error on allocation failure
    #[inline]
    fn reserve(&mut self, id: K, capacity: usize) -> Result<K, ()> {
        self.reallocate(id, self.get_len(id) + capacity)
    }

    /// Try to reserve `capacity` additional capacity for this list, without performing any copying.
    ///
    /// Return an error on allocation failure
    #[inline]
    fn try_reserve(&mut self, id: K, capacity: usize) -> Result<(), ()> {
        self.try_reallocate(id, self.get_len(id) + capacity)
    }

    /// De-allocate a list
    ///
    /// This is a no-op on `K::new(0)`
    ///
    /// Is never UB, but may return an unspecified but valid `ListPool` or panic if:
    /// - `id` has already been `deallocate`'d
    /// - `id` was not returned by `allocate`
    fn deallocate(&mut self, id: K);

    /// Empty a list, *potentially* preserving it's capacity, and return an index to an empty list
    ///
    /// Is never UB, but may return an unspecified but valid `ListPool` or panic if:
    /// - `id` has already been `deallocate`'d
    /// - `id` was not returned by `allocate`
    fn clear_list(&mut self, id: K, preserve_capacity: bool) -> K;

    /// Empty a list, *potentially* preserving it's capacity, without moving it. Returns an error if the list must be moved.
    ///
    /// Is never UB, but may return an unspecified but valid `ListPool` or panic if:
    /// - `id` has already been `deallocate`'d
    /// - `id` was not returned by `allocate`
    fn try_clear(&mut self, id: K, preserve_capacity: bool) -> Result<(), ()>;

    /// Get the list with the given `id` as a slice.
    ///
    /// Is never UB, but may return an unspecified value or panic if:
    /// - `id` has already been `deallocate`'d
    /// - `id` was not returned by `allocate`
    fn get_slice(&self, ix: K) -> &[T];

    /// Get the allocation with the given `id` as a slice.
    ///
    /// Is never UB, but may return an unspecified value or panic if:
    /// - `id` has already been `deallocate`'d
    /// - `id` was not returned by `allocate`
    fn get_slice_mut(&mut self, ix: K) -> &mut [T];

    /// Get the `ix`'th element of the list with the given `id`
    ///
    /// Returns `None` if `ix` is out of bounds.
    ///
    /// Is never UB, but may return an unspecified value or panic if:
    /// - `id` has already been `deallocate`'d
    /// - `id` was not returned by `allocate`
    #[inline(always)]
    fn get_elem(&self, id: K, ix: usize) -> Option<&T> {
        self.get_slice(id).get(ix)
    }

    /// Get the `ix`'th element of the list with the given `id`
    ///
    /// Is never UB, but may return an unspecified value or panic if:
    /// - `ix` is out of bounds
    /// - `id` has already been `deallocate`'d
    /// - `id` was not returned by `allocate`
    #[inline(always)]
    fn get_elem_unchecked(&self, id: K, ix: usize) -> &T {
        self.get_elem(id, ix).unwrap()
    }

    /// Get a mutable reference to the `ix`'th element of the list with the given `id`
    ///
    /// Returns `None` if `ix` is out of bounds.
    ///
    /// Is never UB, but may return an unspecified value or panic if:
    /// - `id` has already been `deallocate`'d
    /// - `id` was not returned by `allocate`
    #[inline(always)]
    fn get_elem_mut(&mut self, id: K, ix: usize) -> Option<&mut T> {
        self.get_slice_mut(id).get_mut(ix)
    }

    /// Get a mutable reference to the `ix`'th element of the allocation with the given `id`
    ///
    /// Is never UB, but may return an unspecified value or panic if:
    /// - `ix` is out of bounds
    /// - `id` has already been `deallocate`'d
    /// - `id` was not returned by `allocate`
    #[inline(always)]
    fn get_elem_unchecked_mut(&mut self, id: K, ix: usize) -> &mut T {
        self.get_elem_mut(id, ix).unwrap()
    }

    /// Get the length of the list with the given `id`
    ///
    /// Is never UB, but may return an unspecified value or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    fn get_len(&self, id: K) -> usize {
        self.get_slice(id).len()
    }

    /// Get the capacity of the list with the given `id`
    ///
    /// Is never UB, but may return an unspecified value or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    fn get_capacity(&self, id: K) -> usize {
        self.get_len(id)
    }

    /// Get whether the list with the given `id` is empty
    ///
    /// Is never UB, but may return an unspecified value or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    fn is_empty(&self, id: K) -> bool {
        self.get_len(id) == 0
    }

    /// Extend this list with a single element, returning the new index (which may be the same as the previous index)
    ///
    /// Returns the item as an error on failure (if the pool is out of capacity)
    ///
    /// Is never UB, but may return an unspecified value or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    fn push(&mut self, id: K, item: T) -> Result<K, T>;

    /// Try to extend this list with a single element without moving it
    ///
    /// Returns the item as an error on failure (if the list is out of capacty)
    ///
    /// Satisfies the invariant that, if `x` is the result of a successful `try_push`, `try_pop` always succeeds on `x`.
    /// Similarly, if `x` is the result of a successful `try_pop`, `try_push` always succeeds on `x`.
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    fn try_push(&mut self, id: K, item: T) -> Result<(), T>;

    /// Pop an element from this list, returning a pair of the new index (which may be the same as the previous index) and the popped item on success
    ///
    /// Returns `None` if the list is empty
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    fn pop(&mut self, id: K) -> Option<(K, T)>;

    /// Try to pop an element from this list without moving it
    ///
    /// Returns `None` if the list is empty
    ///
    /// Satisfies the invariant that, if `x` is the result of a successful `try_push`, `try_pop` always succeeds on `x`.
    /// Similarly, if `x` is the result of a successful `try_pop`, `try_push` always succeeds on `x`.
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    fn try_pop(&mut self, id: K) -> Result<Option<T>, ()>;

    /// Truncates the given list to the first `len` elements, preserving it's capacity. 
    /// 
    /// If `pool.get_len(id) <= len`, this is a no-op.
    ///
    /// Fails if it is necessary to move the allocation.
    ///
    /// Returns `None` if the array does not contain an element at the desired position
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    fn truncate(
        &mut self,
        id: K,
        len: usize,
    ) -> K
    {
        naive_list_pool::truncate(self, id, len)
    }

    /// Tries to truncate the given list to the first `len` elements without moving it.
    /// 
    /// If `pool.get_len(id) <= len`, this is a no-op.
    ///
    /// Correct for any valid [`ListPool`] implementation, but may be suboptimal.
    ///
    /// Fails if it is necessary to move the allocation.
    ///
    /// Returns `None` if the array does not contain an element at the desired position
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    fn try_truncate(
        &mut self,
        id: K,
        len: usize,
    ) -> Result<(), ()>;

    /// Extend this list with an iterator, returning the new index (which may be the same as the previous index).
    ///
    /// May spuriously fail, panic or over-allocate if the iterator's size hint is incorrect, but will never return a wrong result.
    ///
    /// Returns what is left of the iterator as an error on failure; the input list is returned unchanged.
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    fn extend<I: Iterator<Item = T>>(&mut self, id: K, iter: I) -> Result<K, (K, I)> {
        naive_list_pool::extend(self, id, iter)
    }

    /// Try to extend this list with an iterator without moving it
    ///
    /// May spuriously fail if the iterator's length is incorrect, but will never return a wrong result.
    /// Returns what is left of the iterator as an error on failure.
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    fn try_extend<I: Iterator<Item = T>>(&mut self, id: K, iter: I) -> Result<(), I> {
        naive_list_pool::try_extend(self, id, iter)
    }

    /// Extend this list with a range from another list, returning the new index (which may be the same as the previous index).
    ///
    /// Returns an error on allocation failure, or index out of bounds; the input list is returned unchanged.
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    fn extend_from_range(&mut self, id: K, src: K, begin: usize, end: usize) -> Result<K, K>
    where
        T: Clone,
    {
        naive_list_pool::extend_from_range(self, id, src, begin, end)
    }

    /// Extend this list with a range from another list, returning the new index (which may be the same as the previous index).
    ///
    /// Returns an error on allocation failure, or index out of bounds; the input list is unchanged.
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    fn try_extend_from_range(&mut self, id: K, src: K, begin: usize, end: usize) -> Result<(), ()>
    where
        T: Clone,
    {
        naive_list_pool::try_extend_from_range(self, id, src, begin, end)
    }

    /// Extend this list with the elements of a slice, returning the new index (which may be the same as the previous index).
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    fn extend_from_slice(&mut self, id: K, slice: &[T]) -> Result<K, K>
    where
        T: Clone,
    {
        naive_list_pool::extend_from_slice(self, id, slice)
    }

    /// Try to extend this list with the elements of a slice without moving it
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    fn try_extend_from_slice(&mut self, id: K, slice: &[T]) -> Result<(), ()>
    where
        T: Clone,
    {
        naive_list_pool::try_extend_from_slice(self, id, slice)
    }

    /// Create a deep clone of the list, which does not alias the original list
    #[inline(always)]
    fn deep_clone(&mut self, id: K) -> Result<K, ()>
    where
        T: Clone,
    {
        naive_list_pool::deep_clone(self, id)
    }

    /// Allocate a new vector with the elements from the given iterator.
    ///
    /// May spuriously fail or over-allocate if the iterator's length is incorrect, but will never return a wrong result.
    /// Returns what is left of the iterator as an error on failure.
    #[inline(always)]
    fn allocate_from_iter<I: Iterator<Item = T>>(&mut self, iterator: I) -> Result<K, I> {
        naive_list_pool::allocate_from_iter(self, iterator)
    }

    /// Allocate a new vector with the elements from the given slice.
    ///
    /// May spuriously fail or over-allocate if the iterator's length is incorrect, but will never return a wrong result.
    /// Returns what is left of the iterator as an error on failure.
    #[inline(always)]
    fn allocate_from_slice(&mut self, slice: &[T]) -> Result<K, ()>
    where
        T: Clone,
    {
        naive_list_pool::allocate_from_slice(self, slice)
    }

    /// Insert an element to a specific index in a list, returning the new index (which may be the same as the previous index).
    ///
    /// Returns the element on failure, either due to index out of bounds or due to pool memory exhaustion
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    fn insert(&mut self, id: K, ix: usize, item: T) -> Result<K, T> {
        naive_list_pool::insert(self, id, ix, item)
    }

    /// Insert an element to a specific index in the list
    ///
    /// Returns the element on failure, either due to index out of bounds or due to list capacity exhaustion
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    fn try_insert(&mut self, id: K, ix: usize, item: T) -> Result<(), T> {
        naive_list_pool::try_insert(self, id, ix, item)
    }

    /// Removes and returns the element at the supplied index from the list, and shifts all elements after it to the left.
    ///
    /// Returns `None` if the list does not contain an element at the desired position
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    fn remove(&mut self, id: K, ix: usize) -> Option<(K, T)> {
        naive_list_pool::remove(self, id, ix)
    }

    /// Tries to remove and returns the element at the supplied index from the list, and shifts all elements after it to the left.
    ///
    /// Fails if it is necessary to move the allocation.
    ///
    /// Returns `None` if the array does not contain an element at the desired position
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    fn try_remove(&mut self, id: K, ix: usize) -> Result<Option<T>, ()> {
        naive_list_pool::try_remove(self, id, ix)
    }

    /// Check whether two lists are equal in this pool
    ///     
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `left` or `right` was not returned by `allocate`
    /// - `left` or `right` has already been `deallocate`'d
    #[inline]
    fn eq_in(&self, left: K, right: K) -> bool
    where
        T: PartialEq,
    {
        //NOTE: cannot first check whether left == right, since `T` is only `PartialEq` and hence equal elements may compare as disequal!
        self.get_slice(left) == self.get_slice(right)
    }
}

/// Naive implementations of [`ListPool`] default methods, mainly for testing and benchmarking purposes
pub mod naive_list_pool {
    use super::*;

    /// Extend this allocation with an iterator, returning the new allocation index (which may be the same as the previous index)
    ///
    /// Correct for any valid [`ListPool`] implementation, but may be suboptimal.
    ///
    /// May spuriously fail, panic or over-allocate if the iterator's size hint is incorrect, but will never return a wrong result.
    ///
    /// Returns what is left of the iterator as an error on failure; the input list is returned unchanged.
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline]
    pub fn extend<T, K, I>(
        pool: &mut (impl ListPool<T, K> + ?Sized),
        id: K,
        mut iter: I,
    ) -> Result<K, (K, I)>
    where
        K: Copy,
        I: Iterator<Item = T>,
    {
        let Ok(mut id) = pool.reserve(id, iter.size_hint().0) else { return Err((id, iter)) };
        let mut pushed = 0;
        for elem in &mut iter {
            id = if let Ok(id) = pool.push(id, elem) {
                pushed += 1;
                id
            } else {
                for _ in 0..pushed {
                    id = pool.pop(id).expect("push succeeded, so pop should too").0
                }
                return Err((id, iter));
            };
        }
        Ok(id)
    }

    /// Try to extend this allocation with an iterator without making a new allocation
    ///
    /// Correct for any valid [`ListPool`] implementation, but may be suboptimal.
    ///
    /// May spuriously fail if the iterator's length is incorrect, but will never return a wrong result.
    /// Returns what is left of the iterator as an error on failure.
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline]
    pub fn try_extend<T, K, I>(
        pool: &mut (impl ListPool<T, K> + ?Sized),
        id: K,
        mut iter: I,
    ) -> Result<(), I>
    where
        K: Copy,
        I: Iterator<Item = T>,
    {
        let mut pushed = 0;
        for elem in &mut iter {
            let Ok(_) = pool.try_push(id, elem) else {
                for _ in 0..pushed {
                    let r = pool.try_pop(id);
                    debug_assert!(r.is_ok());
                }
                return Err(iter);
            };
            pushed += 1;
        }
        Ok(())
    }

    /// Extend this allocation with a range from another list, returning the new allocation index (which may be the same as the previous index)
    ///
    /// Correct for any valid [`ListPool`] implementation, but may be suboptimal.
    ///
    /// Returns an error on allocation failure, or index out of bounds; the input list is returned unchanged.
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline]
    pub fn extend_from_range<T, K>(
        pool: &mut (impl ListPool<T, K> + ?Sized),
        id: K,
        src: K,
        begin: usize,
        end: usize,
    ) -> Result<K, K>
    where
        K: Copy,
        T: Clone,
    {
        if end >= pool.get_len(src) {
            return Err(id);
        }
        let Ok(mut id) = pool.reserve(id, end.saturating_sub(begin)) else { return Err(id) };
        let mut pushed = 0;
        for elem in begin..end {
            let elem = pool
                .get_elem_unchecked(src, elem)
                .clone();
            id = if let Ok(id) = pool.push(id, elem) {
                pushed += 1;
                id
            } else {
                for _ in 0..pushed {
                    id = pool.pop(id).expect("push succeeded, so pop should too").0
                }
                return Err(id);
            };
        }
        Ok(id)
    }

    /// Extend this allocation with a range from another list, returning the new allocation index (which may be the same as the previous index)
    ///
    /// Correct for any valid [`ListPool`] implementation, but may be suboptimal.
    ///
    /// Returns an error on allocation failure, or index out of bounds; the input list is unchanged.
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline]
    pub fn try_extend_from_range<T, K>(
        pool: &mut (impl ListPool<T, K> + ?Sized),
        id: K,
        src: K,
        begin: usize,
        end: usize,
    ) -> Result<(), ()>
    where
        K: Copy,
        T: Clone,
    {
        if end >= pool.get_len(src) {
            return Err(());
        }
        pool.try_reserve(id, end.saturating_sub(begin))?;
        let mut pushed = 0;
        for elem in begin..end {
            let elem = pool
                .get_elem_unchecked(src, elem)
                .clone();
            if pool.try_push(id, elem).is_ok() {
                pushed += 1;
            } else {
                for _ in 0..pushed {
                    let r = pool.try_pop(id);
                    debug_assert!(r.is_ok());
                }
                return Err(());
            };
        }
        Ok(())
    }

    /// Extend this allocation with the elements of a slice, returning the new allocation index (which may be the same as the previous index)
    ///
    /// Correct for any valid [`ListPool`] implementation, but may be suboptimal.
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    pub fn extend_from_slice<T, K>(
        pool: &mut (impl ListPool<T, K> + ?Sized),
        id: K,
        slice: &[T],
    ) -> Result<K, K>
    where
        K: Copy,
        T: Clone,
    {
        pool.extend(id, slice.iter().cloned()).map_err(|(id, _)| id)
    }

    /// Try to extend this allocation with the elements of a slice without making a new allocation
    ///
    /// Correct for any valid [`ListPool`] implementation, but may be suboptimal.
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    pub fn try_extend_from_slice<K, T>(
        pool: &mut (impl ListPool<T, K> + ?Sized),
        id: K,
        slice: &[T],
    ) -> Result<(), ()>
    where
        K: Copy,
        T: Clone,
    {
        pool.try_extend(id, slice.iter().cloned()).map_err(|_| ())
    }

    /// Create a deep clone of the list, which does not alias the original list
    ///
    /// Correct for any valid [`ListPool`] implementation, but may be suboptimal.
    #[inline(always)]
    pub fn deep_clone<T, K>(pool: &mut (impl ListPool<T, K> + ?Sized), id: K) -> Result<K, ()>
    where
        K: Copy,
        T: Clone,
    {
        let len = pool.get_len(id);
        let result = pool.allocate(len)?;
        match pool.extend_from_range(result, id, 0, len) {
            Ok(result) => Ok(result),
            Err(result) => {
                pool.deallocate(result);
                Err(())
            }
        }
    }

    /// Allocate a new vector with the elements from the given iterator
    ///
    /// Correct for any valid [`ListPool`] implementation, but may be suboptimal.
    ///
    /// May spuriously fail or over-allocate if the iterator's length is incorrect, but will never return a wrong result.
    /// Returns what is left of the iterator as an error on failure.
    #[inline]
    pub fn allocate_from_iter<T, K, I>(
        pool: &mut (impl ListPool<T, K> + ?Sized),
        iterator: I,
    ) -> Result<K, I>
    where
        K: Copy,
        I: Iterator<Item = T>,
    {
        let Ok(new) = pool.allocate(iterator.size_hint().0) else { return Err(iterator) };
        match pool.extend(new, iterator) {
            Ok(result) => Ok(result),
            Err((id, iter)) => {
                pool.deallocate(id);
                Err(iter)
            }
        }
    }

    /// Allocate a new vector with the elements from the given slice
    ///
    /// Correct for any valid [`ListPool`] implementation, but may be suboptimal.
    ///
    /// May spuriously fail or over-allocate if the iterator's length is incorrect, but will never return a wrong result.
    /// Returns what is left of the iterator as an error on failure.
    #[inline]
    pub fn allocate_from_slice<T, K>(
        pool: &mut (impl ListPool<T, K> + ?Sized),
        slice: &[T],
    ) -> Result<K, ()>
    where
        K: Copy,
        T: Clone,
    {
        let Ok(new) = pool.allocate(slice.len()) else { return Err(()) };
        match pool.extend_from_slice(new, slice) {
            Ok(result) => Ok(result),
            Err(id) => {
                pool.deallocate(id);
                Err(())
            }
        }
    }

    /// Insert an element to a specific index in an allocation, returning the new allocation index (which may be the same as the previous index)
    ///
    /// Correct for any valid [`ListPool`] implementation, but may be suboptimal.
    ///
    /// Returns the element on failure, either due to index out of bounds or due to pool memory exhaustion
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline]
    pub fn insert<T, K>(
        pool: &mut (impl ListPool<T, K> + ?Sized),
        id: K,
        ix: usize,
        item: T,
    ) -> Result<K, T>
    where
        K: Copy,
    {
        if ix >= pool.get_len(id) {
            return Err(item);
        }
        let id = pool.push(id, item)?;
        pool.get_slice_mut(id)[ix..].rotate_right(1);
        Ok(id)
    }

    /// Insert an element to a specific index in the array
    ///
    /// Correct for any valid [`ListPool`] implementation, but may be suboptimal.
    ///
    /// Returns the element on failure, either due to index out of bounds or due to list capacity exhaustion
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline]
    pub fn try_insert<T, K>(
        pool: &mut (impl ListPool<T, K> + ?Sized),
        id: K,
        ix: usize,
        item: T,
    ) -> Result<(), T>
    where
        K: Copy,
    {
        let ixu = ix.index();
        if ixu >= pool.get_len(id) {
            return Err(item);
        }
        pool.try_push(id, item)?;
        pool.get_slice_mut(id)[ixu..].rotate_right(1);
        Ok(())
    }

    /// Removes and returns the element at the supplied index from the array, and shifts all elements after it to the left
    ///
    /// Correct for any valid [`ListPool`] implementation, but may be suboptimal.
    ///
    /// Returns `None` if the array does not contain an element at the desired position
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline]
    pub fn remove<T, K>(
        pool: &mut (impl ListPool<T, K> + ?Sized),
        id: K,
        ix: usize,
    ) -> Option<(K, T)>
    where
        K: Copy,
    {
        let slice = pool.get_slice_mut(id);
        if ix >= slice.len() {
            None
        } else {
            slice[ix..].rotate_left(1);
            pool.pop(id)
        }
    }

    /// Tries to remove and returns the element at the supplied index from the array, and shifts all elements after it to the left
    ///
    /// Correct for any valid [`ListPool`] implementation, but may be suboptimal.
    ///
    /// Fails if it is necessary to move the allocation.
    ///
    /// Returns `None` if the array does not contain an element at the desired position
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline]
    pub fn try_remove<T, K>(
        pool: &mut (impl ListPool<T, K> + ?Sized),
        id: K,
        ix: usize,
    ) -> Result<Option<T>, ()>
    where
        K: Copy,
    {
        let slice = pool.get_slice_mut(id);
        Ok(if ix >= slice.len() {
            None
        } else {
            slice[ix..].rotate_left(1);
            pool.try_pop(id)?
        })
    }

    /// Truncates the given list to the first `len` elements, preserving it's capacity. 
    /// 
    /// If `pool.get_len(id) <= len`, this is a no-op.
    ///
    /// Correct for any valid [`ListPool`] implementation, but may be suboptimal.
    ///
    /// Fails if it is necessary to move the allocation.
    ///
    /// Returns `None` if the array does not contain an element at the desired position
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline]
    pub fn truncate<T, K>(
        pool: &mut (impl ListPool<T, K> + ?Sized),
        mut id: K,
        len: usize,
    ) -> K
    where
        K: Copy,
    {
        let mut curr_len = pool.get_len(id);
        while curr_len > len {
            id = pool.pop(id).expect("pool has nonzero length, so pop should always succeed").0;
            curr_len -= 1;
            debug_assert_eq!(curr_len, pool.get_len(id));
        }
        id
    }
}

/// A `ListPool` supporting stable `pop`, `truncate`, and `remove` operations
pub trait StableRemovalPool<T, K: Copy>: ListPool<T, K> {
    /// Pop the element at the back of this list, if any, without moving the allocation
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    fn pop_inplace(&mut self, id: K) -> Option<T> {
        let (_new_id, retv) = self.pop(id)?;
        // Morally:
        // debug_assert!(
        //     new_id == id,
        //     "pool implements StableRemovalPool, but pop does not leave index invariant!"
        // );
        // Alas, we can't implement an assert like this w/out specialization. Alas!
        Some(retv)
    }

    /// Truncates the given list to the first `len` elements, preserving it's capacity, without moving the allocation
    /// 
    /// If `pool.get_len(id) <= len`, this is a no-op.
    ///
    /// Fails if it is necessary to move the allocation.
    ///
    /// Returns `None` if the array does not contain an element at the desired position
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline]
    fn truncate_inplace(
        &mut self,
        id: K,
        len: usize,
    )
    where
        K: Copy,
    {
        let _new_id = self.truncate(id, len);
        // Morally:
        // debug_assert!(
        //     new_id == id,
        //     "pool implements StableRemovalPool, but pop does not leave index invariant!"
        // );
        // Alas, we can't implement an assert like this w/out specialization. Alas!
    }

    /// Tries to remove and returns the element at the supplied index from the array, and shifts all elements after it to the left.
    ///
    /// Fails if it is necessary to move the allocation.
    ///
    /// Returns `None` if the array does not contain an element at the desired position
    ///
    /// Is never UB, but may return an unspecified value, leave behind an unspecified but valid `ListPool`, or panic if:
    /// - `id` was not returned by `allocate`
    /// - `id` has already been `deallocate`'d
    #[inline(always)]
    fn remove_inplace(&mut self, id: K, ix: usize) -> Option<T> {
        let (_new_id, retv) = self.remove(id, ix)?;
        // Morally:
        // debug_assert!(
        //     new_id == id,
        //     "pool implements StableRemovalPool, but pop does not leave index invariant!"
        // );
        // Alas, we can't implement an assert like this w/out specialization. Alas!
        Some(retv)
    }

    /// Empty a list, *potentially* preserving it's capacity, without moving it. Returns an error if the list must be moved.
    ///
    /// Is never UB, but may return an unspecified but valid `ListPool` or panic if:
    /// - `id` has already been `deallocate`'d
    /// - `id` was not returned by `allocate`
    #[inline(always)]
    fn clear_inplace(&mut self, id: K, preserve_capacity: bool) {
        let _new_id = self.clear_list(id, preserve_capacity);
        // Morally:
        // debug_assert!(
        //     new_id == id,
        //     "pool implements StableRemovalPool, but pop does not leave index invariant!"
        // );
        // Alas, we can't implement an assert like this w/out specialization. Alas!
    }
}

/// A memory pool for storing lists of `T`, where `T` is an `EntityIx`
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct IndexPool<T> {
    /// The underlying data
    ///
    /// Lists are stored as `len`-prefixed ranges of `T` of length `size_class = 2^(floor(log2(len)) + 2) - 1`, where `len` is stored as `T::new(len)`.
    /// Uninitialized `T` are stored first as `T::default()`, except in the case where `len == 0`, but can be anything.
    /// If `len == 0`, since the smallest size class is 4, the slice is of length at least 3. So the *next* elements store one of the following patterns:
    /// - Free blocks: [capacity, prev_free_block_id, next_free_block_id]
    /// - Reserved blocks: [capacity, 0, 0]
    /// - Back-reserved blocks: [capacity, capacity, 0]
    data: Vec<T>,
    /// The heads of the free list for each size class
    ///
    /// Empty free lists are stored with head zero.
    free_lists: Vec<T>,
    //TODO: optional statistics (e.g. % free memory, etc?)
}

/// Get the smallest size class greater than a given size
#[inline(always)]
const fn get_smallest_size_class_larger_than(size: usize) -> u32 {
    usize::BITS - (size / 4).leading_zeros()
}

/// Get the largest size class less than a given size
#[inline(always)]
const fn get_largest_size_class_smaller_than(size: usize) -> u32 {
    //TODO: optimize...
    debug_assert!(size >= round_up_to_size_class(0));
    let bigger_size_class = get_smallest_size_class_larger_than(size);
    if get_size_class_len(bigger_size_class) == size {
        bigger_size_class
    } else {
        bigger_size_class - 1
    }
}

/// Get the length for a given size class
#[inline(always)]
const fn get_size_class_len(size_class: u32) -> usize {
    (4 << size_class) - 1
}

/// Round up a length to the smallest size class containing it
#[inline(always)]
const fn round_up_to_size_class(size: usize) -> usize {
    get_size_class_len(get_smallest_size_class_larger_than(size))
}

/// Round up a length to the associated primary capacity
#[inline(always)]
const fn primary_capacity(len: usize) -> usize {
    if len == 0 {
        0
    } else {
        round_up_to_size_class(len)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum ListStatus {
    Free {
        capacity: usize,
        prev: usize,
        next: usize,
    },
    Reserved {
        capacity: usize,
    },
    Back {
        capacity: usize,
    },
    Occupied {
        len: usize,
    },
}

impl ListStatus {
    #[inline]
    fn capacity(&self) -> usize {
        match self {
            ListStatus::Free { capacity, .. }
            | ListStatus::Reserved { capacity }
            | ListStatus::Back { capacity } => *capacity,
            ListStatus::Occupied { len } => round_up_to_size_class(*len),
        }
    }

    #[inline]
    fn offset(&self) -> usize {
        match self {
            ListStatus::Free { capacity, .. }
            | ListStatus::Reserved { capacity }
            | ListStatus::Back { capacity } => *capacity + 1,
            ListStatus::Occupied { len } => round_up_to_size_class(*len) + 1,
        }
    }

    #[inline]
    fn prev(&self) -> usize {
        match self {
            ListStatus::Free { prev, .. } => *prev,
            _ => panic!("List status {self:?} does not have previous pointer"),
        }
    }

    #[inline]
    fn next(&self) -> usize {
        match self {
            ListStatus::Free { next, .. } => *next,
            _ => panic!("List status {self:?} does not have next pointer"),
        }
    }
}

impl<T: EntityIx> Default for IndexPool<T> {
    #[inline(always)]
    fn default() -> Self {Self::new()
    }
}

impl<T: EntityIx> IndexPool<T> {
    /// Create a new, empty `IndexPool`
    #[inline(always)]
    pub fn new() -> Self {
        IndexPool {
            data: Vec::new(),
            free_lists: Vec::new(),
        }
    }

    /// Get the capacity of a list in this pool, given it's index
    #[inline]
    pub fn get_capacity_ix(&self, ix: usize) -> usize {
        if ix == 0 {
            return 0;
        }
        let len = self.data[ix - 1].index();
        if len == 0 {
            self.data[ix].index()
        } else {
            let primary_capacity = round_up_to_size_class(len);
            let back_ix = ix + primary_capacity + 1;
            debug_assert_eq!(back_ix, self.next_block(ix));
            let status = self.get_status(back_ix);
            if let Some(ListStatus::Back { capacity }) = status {
                primary_capacity 
                + capacity // Back capacity 
                + 1 // Space used up by length of back
            } else {
                primary_capacity
            }
        }
    }

    /// Get the length of a list in this pool, given it's index
    #[inline(always)]
    pub fn get_len_ix(&self, ix: usize) -> usize {
        if ix == 0 {
            return 0;
        }
        debug_assert!(matches!(
            self.get_status(ix),
            Some(ListStatus::Occupied { .. } | ListStatus::Reserved { .. })
        ));
        self.data[ix - 1].index()
    }

    /// Get the minimum memory usage of this pool, in bytes
    #[inline(always)]
    pub fn min_memory_usage(&self) -> usize {
        //TODO: include size of struct itself?
        (self.data.len() + self.free_lists.len()) * std::mem::size_of::<T>()
    }

    /// Get the memory usage of this pool, in bytes
    #[inline(always)]
    pub fn memory_usage(&self) -> usize {
        //TODO: include size of struct itself?
        (self.data.capacity() + self.free_lists.capacity()) * std::mem::size_of::<T>()
    }

    /// Get the offset to the next block in this pool
    #[inline(always)]
    fn block_offset(&self, ix: usize) -> usize {
        self.get_status(ix)
            .expect("Only a block with valid status can have an offset!")
            .offset()
    }

    /// Get the total number of free blocks, as well as the number of free slots
    pub fn total_free_blocks(&mut self) -> (usize, usize) {
        let mut total_blocks = 0;
        let mut total_slots = 0;
        for size_class in 0..(self.free_lists.len() as u32) {
            let (blocks, slots) = self.free_list_len(size_class);
            total_blocks += blocks;
            total_slots += slots;
        }
        (total_blocks, total_slots)
    }

    /// Get the length of a given free list, as well as the number of slots in the list
    pub fn free_list_len(&mut self, size_class: u32) -> (usize, usize) {
        let Some(head) = self.free_lists.get(size_class as usize) else { return (0, 0) };
        let head = head.index();
        if head == 0 {
            (0, 0)
        } else {
            let mut prev = head;
            let mut blocks = 1;
            let mut slots = 0;
            loop {
                let status = self.get_status(prev).unwrap();
                slots += status.capacity();
                let next = status.next();
                debug_assert_eq!(self.get_status(next).unwrap().prev(), prev);
                if next == head {
                    return (blocks, slots);
                }
                blocks += 1;
                prev = next
            }
        }
    }

    /// Get whether the free list contains a given block
    #[allow(dead_code)]
    fn free_list_contains_block(&mut self, size_class: u32, ix: usize) -> bool {
        let Some(head) = self.free_lists.get(size_class as usize) else { return false };
        let head = head.index();
        if head == 0 {
            false
        } else if head == ix {
            true
        } else {
            let mut prev = head;
            loop {
                let next = self.next_free(prev);
                assert_eq!(self.prev_free(next), prev);
                if next == ix {
                    return true;
                }
                if next == head {
                    return false;
                }
                prev = next
            }
        }
    }

    /// Get the index of the next block, given the index of the current
    fn next_block(&self, ix: usize) -> usize {
        ix + self.block_offset(ix)
    }

    /// Get the previous element of a free list, given a current element
    fn prev_free(&self, curr: usize) -> usize {
        let prev = self.data[curr + 1].index();
        debug_assert_eq!(prev, self.get_status(curr).unwrap().prev());
        prev
    }

    /// Get the next element of a free list, given a current element
    fn next_free(&self, curr: usize) -> usize {
        let next = self.data[curr + 2].index();
        debug_assert_eq!(next, self.get_status(curr).unwrap().next());
        next
    }

    /// Get the head of the free list for a given size class
    #[inline(always)]
    fn free_list_head(&mut self, size_class: u32) -> usize {
        self.free_lists.extend(
            std::iter::repeat(T::new(0))
                .take((size_class as usize + 1).saturating_sub(self.free_lists.len())),
        );
        self.free_lists[size_class as usize].index()
    }

    /// Fixup the length and back allocation for a given *nonempty* block.
    #[inline]
    fn fixup_len_and_back(&mut self, ix: usize, len: usize, capacity: usize) {
        let l = T::new(len);
        let z = T::new(0);
        if len == 0 {
            self.data[ix - 1] = l;
            self.data[ix] = T::new(capacity);
            self.data[ix + 1] = z;
            self.data[ix + 2] = z;
            debug_assert_eq!(self.get_status(ix), Some(ListStatus::Reserved { capacity }))
        } else {
            self.data[ix - 1] = l;
            debug_assert_eq!(self.get_status(ix), Some(ListStatus::Occupied { len }));
            let primary_capacity = round_up_to_size_class(len);
            let back_ix = ix + primary_capacity + 1;
            debug_assert_eq!(self.next_block(ix), back_ix);
            if capacity != primary_capacity {
                let end_ix = ix + capacity + 1;
                let slack = end_ix - back_ix - 1;
                let s = T::new(slack);
                debug_assert_eq!(back_ix + s.index() + 1, end_ix);
                self.data[back_ix - 1] = z;
                self.data[back_ix] = s;
                self.data[back_ix + 1] = s;
                self.data[back_ix + 2] = z;
                debug_assert_eq!(
                    self.get_status(back_ix),
                    Some(ListStatus::Back { capacity: slack })
                );
                debug_assert_eq!(self.next_block(back_ix), end_ix);
            }
        }
        debug_assert_eq!(self.get_len(T::new(ix)), len);
        debug_assert_eq!(self.get_capacity(T::new(ix)), capacity);
    }

    /// Push a block onto the free list, where `ix` is the block's index
    ///
    /// Overwrites the old capacity information, and any data!
    #[inline(always)]
    fn push_to_free_list(&mut self, ix: usize, capacity: usize) {
        debug_assert_ne!(
            capacity, 0,
            "Cannot push block {ix} of capacity 0 onto free list!"
        );
        debug_assert_eq!(capacity % (1 + round_up_to_size_class(0)), 3);
        debug_assert_ne!(ix, 0, "Zero can never be placed onto the free list");
        let size_class = get_largest_size_class_smaller_than(capacity);
        debug_assert!(capacity >= get_size_class_len(size_class));
        debug_assert!(
            capacity == get_size_class_len(size_class)
                || size_class + 1 == get_smallest_size_class_larger_than(capacity)
        );
        debug_assert!(capacity <= get_size_class_len(size_class + 1));
        //TODO: deal with the fact that these debug assertions are *very* slow...
        debug_assert!(!self.free_list_contains_block(size_class, ix));
        self.data[ix - 1] = T::new(0);
        self.data[ix] = T::new(capacity);
        let head = self.free_list_head(size_class);
        let id = T::new(ix);
        if head != 0 {
            self.data[ix + 1] = T::new(head);
            let next = self.data[head + 2];
            self.data[ix + 2] = next;
            self.data[head + 2] = id;
            self.data[next.index() + 1] = id;
            debug_assert_eq!(
                self.get_status(head),
                Some(ListStatus::Free {
                    capacity: self.data[head].index(),
                    prev: self.data[head + 1].index(),
                    next: ix
                })
            );
            debug_assert_eq!(
                self.get_status(ix),
                Some(ListStatus::Free {
                    capacity: capacity,
                    prev: head,
                    next: next.index()
                })
            )
        } else {
            self.free_lists[size_class as usize] = id;
            self.data[ix + 1] = id;
            self.data[ix + 2] = id;
            debug_assert_eq!(
                self.get_status(ix),
                Some(ListStatus::Free {
                    capacity: capacity,
                    prev: ix,
                    next: ix
                })
            )
        }
    }

    /// Pop a block from the free list, where `ix` is the block's index
    ///
    /// Assumes the size class recorded in the block is valid
    #[inline(always)]
    fn pop_from_free_list(&mut self, ix: usize) {
        debug_assert_ne!(ix, 0, "Zero can never be on the free list");
        let capacity = self.data[ix].index();
        debug_assert_ne!(capacity, 0, "Free block can never have zero capacity");
        let size_class = get_largest_size_class_smaller_than(capacity);
        //TODO: deal with the fact that these debug assertions are *very* slow...
        debug_assert!(self.free_list_contains_block(size_class, ix));
        let prev = self.data[ix + 1];
        let next = self.data[ix + 2];
        self.data[prev.index() + 2] = next;
        self.data[next.index() + 1] = prev;
        self.free_lists[size_class as usize] = if next.index() != ix { next } else { T::new(0) };
    }

    /// Get the status of a list at an index
    ///
    /// May return an unspecified result or panic if the status is invalid
    #[inline(always)]
    fn get_status(&self, ix: usize) -> Option<ListStatus> {
        if ix == 0 {
            return Some(ListStatus::Occupied { len: 0 });
        }
        if let Some(len) = self.data.get(ix - 1) {
            let len = len.index();
            if len == 0 {
                let capacity = self.data[ix].index();
                let prev = self.data[ix + 1].index();
                let next = self.data[ix + 2].index();
                debug_assert_ne!(
                    capacity, 0,
                    "Block capacity should never be zero, ix = {ix}, prev = {prev}, next = {next}"
                );
                if prev == 0 {
                    debug_assert_eq!(next, 0, "Reserved block headers should be zero-terminated, ix = {ix}, cap = {capacity}");
                    Some(ListStatus::Reserved { capacity })
                } else if next == 0 {
                    debug_assert_eq!(
                        prev, capacity,
                        "Back block headers should have duplicated capacity, as a bug check"
                    );
                    Some(ListStatus::Back { capacity })
                } else {
                    debug_assert_eq!(
                        self.data[next + 1].index(),
                        ix,
                        "Current free block should be prev of next"
                    );
                    debug_assert_eq!(
                        self.data[prev + 2].index(),
                        ix,
                        "Current free block should be next of prev"
                    );
                    Some(ListStatus::Free {
                        capacity,
                        prev,
                        next,
                    })
                }
            } else {
                Some(ListStatus::Occupied { len })
            }
        } else {
            None
        }
    }

    #[inline(always)]
    fn get_slice_indices(&self, ix: T) -> std::ops::Range<usize> {
        let ix = ix.index();
        debug_assert!(matches!(
            self.get_status(ix),
            Some(ListStatus::Occupied { .. } | ListStatus::Reserved { .. })
        ));
        if ix == 0 {
            return 0..0;
        };
        let len = self.data[ix - 1].index();
        //NOTE: *not* `ix + 1`, since the length is stored at `ix - 1` (not `ix`), and `(ix - 1) + 1 = ix`
        ix..ix + len
    }

    /// Get a fresh clone of `id` with `item` appended to the end
    #[inline]
    pub fn clone_pushed(&mut self, id: T, item: T) -> Result<T, ()> {
        let len = self.get_len(id);
        let new_len = T::try_new(len + 1).ok_or(())?;
        let new_block = self.allocate(len + 1)?;
        let oix = id.index();
        let nix = new_block.index();
        debug_assert_eq!(
            self.get_status(nix),
            Some(ListStatus::Reserved {
                capacity: round_up_to_size_class(len + 1)
            })
        );
        self.data[nix - 1] = new_len;
        for i in 0..len {
            self.data[nix + i] = self.data[oix + i]
        }
        self.data[nix + len] = item;
        debug_assert_eq!(
            self.get_status(nix),
            Some(ListStatus::Occupied { len: len + 1 })
        );
        Ok(new_block)
    }

    #[inline]
    fn extend_slice_unchecked(&mut self, ix: usize, slice: &[T]) -> Result<(), ()>
    where
        T: Clone,
    {
        debug_assert!(matches!(
            self.get_status(ix),
            Some(ListStatus::Occupied { .. } | ListStatus::Reserved { .. })
        ));
        let len = self.data[ix - 1].index();
        let new_len = len + slice.len();
        let new_len_id = T::try_new(new_len).ok_or(())?;
        if new_len > primary_capacity(len) {
            self.fixup_len_and_back(ix, new_len, self.get_capacity_ix(ix))
        }
        self.data[ix - 1] = new_len_id;
        for (i, d) in slice.iter().copied().enumerate() {
            self.data[ix + len + i] = d
        }
        if slice.len() > 0 || len > 0 {
            debug_assert_eq!(
                self.get_status(ix),
                Some(ListStatus::Occupied {
                    len: len + slice.len()
                })
            );
        } else {
            debug_assert!(matches!(
                self.get_status(ix),
                Some(ListStatus::Reserved { .. })
            ));
        }
        Ok(())
    }

    #[inline]
    fn extend_from_range_unchecked(
        &mut self,
        ix: usize,
        src: usize,
        begin: usize,
        end: usize,
    ) -> Result<(), ()> {
        debug_assert!(matches!(
            self.get_status(ix),
            Some(ListStatus::Occupied { .. } | ListStatus::Reserved { .. })
        ));
        let len = self.data[ix - 1].index();
        let new_len = len + end.saturating_sub(begin);
        let new_len_id = T::try_new(new_len).ok_or(())?;
        if new_len > primary_capacity(len) {
            self.fixup_len_and_back(ix, new_len, self.get_capacity_ix(ix))
        }
        self.data[ix - 1] = new_len_id;
        for i in begin..end {
            self.data[ix + len + (i - begin)] = self.data[src + i]
        }
        if begin < end || len > 0 {
            debug_assert_eq!(
                self.get_status(ix),
                Some(ListStatus::Occupied {
                    len: len + end.saturating_sub(begin)
                })
            );
        } else {
            debug_assert!(matches!(
                self.get_status(ix),
                Some(ListStatus::Reserved { .. })
            ));
        }
        Ok(())
    }

    /// Get a list of all blocks and offsets in this index pool, for debugging purposes
    #[allow(dead_code)]
    fn debug_blocks(&self) -> Vec<(usize, Option<ListStatus>)> {
        let mut result = Vec::new();
        let mut curr = 1;
        loop {
            let status = self.get_status(curr);
            result.push((curr, status));
            if status.is_none() {
                return result;
            }
            curr = self.next_block(curr);
        }
    }

    /// Get a list of all active slices in this index pool, for debugging purposes
    #[allow(dead_code)]
    pub fn slices_in_pool(&self, insert_empty: bool) -> Vec<&[T]> {
        let mut result = Vec::new();
        let mut curr = 1;
        loop {
            let status = self.get_status(curr);
            match status {
                Some(ListStatus::Occupied { len }) => result.push(&self.data[curr..curr + len]),
                Some(ListStatus::Reserved { .. }) if insert_empty => result.push(&[]),
                Some(_) => {}
                None => return result,
            }
            curr = self.next_block(curr);
        }
    }

    /// Check whether this pool has the same set of active slices as another, for debugging purposes
    pub fn equal_active_slices(&self, other: &Self, insert_empty: bool, dedup: bool) -> bool
    where
        T: Ord,
    {
        let mut active_slices = self.slices_in_pool(insert_empty);
        let mut other_slices = other.slices_in_pool(insert_empty);
        active_slices.sort_unstable();
        other_slices.sort_unstable();
        if dedup {
            active_slices.dedup();
            other_slices.dedup();
        }
        active_slices == other_slices
    }
}

impl<T: EntityIx> ListPool<T, T> for IndexPool<T> {
    #[inline(always)]
    fn allocate(&mut self, size_hint: usize) -> Result<T, ()> {
        if size_hint == 0 {
            Ok(T::new(0))
        } else {
            self.allocate_unique(size_hint)
        }
    }

    #[inline]
    fn allocate_unique(&mut self, size_hint: usize) -> Result<T, ()> {
        let goal_capacity = round_up_to_size_class(size_hint);
        let capacity_id = T::try_new(goal_capacity).ok_or(())?;
        let size_class = get_smallest_size_class_larger_than(size_hint);
        let head = 'head: {
            let capacity = get_size_class_len(size_class);
            // Search the free list for an available block of appropriate size
            for free_block_size_class in
                self.free_lists.len().min(size_class as usize)..self.free_lists.len()
            {
                let head = self.free_lists[free_block_size_class];
                let free_block_ix = head.index();
                if free_block_ix != 0 {
                    let free_block_capacity = self.data[free_block_ix].index();
                    debug_assert!(
                        free_block_capacity >= capacity,
                        "capacity smaller than {capacity} = get_size_class_len({size_class}), block status:\n{:?}",
                        self.get_status(free_block_ix)
                    );
                    debug_assert!(
                        free_block_capacity >= get_size_class_len(free_block_size_class as u32),
                        "free block capacity too small for it's size class!"
                    );
                    debug_assert!(
                        free_block_capacity <= get_size_class_len(free_block_size_class as u32 + 1),
                        "free block capacity too large for it's size class!"
                    );
                    debug_assert!(matches!(
                        self.get_status(free_block_ix),
                        Some(ListStatus::Free { .. })
                    ));
                    debug_assert_eq!(
                        self.get_status(free_block_ix).unwrap().capacity(),
                        free_block_capacity
                    );
                    self.pop_from_free_list(free_block_ix);
                    // Push any remaining capacity back onto the free list
                    if free_block_capacity > capacity {
                        let remaining_capacity = free_block_capacity - capacity - 1;
                        let new_free_ix = free_block_ix + capacity + 1;
                        debug_assert!(
                            remaining_capacity >= 3,
                            "got remaining_capacity = {free_block_capacity} - {capacity} - 1 = {remaining_capacity} <= 3"
                        );
                        debug_assert_eq!(
                            (free_block_capacity - capacity) % (1 + round_up_to_size_class(0)), 0,
                            "{free_block_capacity} and {capacity} diff not a multiple of smallest size class!"
                        );
                        self.push_to_free_list(new_free_ix, remaining_capacity);
                        debug_assert!(matches!(
                            self.get_status(new_free_ix),
                            Some(ListStatus::Free { .. })
                        ));
                        debug_assert_eq!(
                            self.get_status(new_free_ix).unwrap().capacity(),
                            remaining_capacity
                        );
                    }
                    break 'head head;
                }
            }
            // On failure, allocate such a block at the end of the vector

            // Truncate trailing zeroes to save time searching later? Or just keep track of a length count?
            // self.free_lists.truncate(size_class as usize);
            let new_head_ix = self.data.len() + 1;
            let new_head = T::try_new(new_head_ix).ok_or(())?;
            self.data
                .extend(std::iter::repeat(T::new(0)).take(capacity + 1));
            debug_assert_eq!(self.data.len(), new_head_ix + capacity);
            new_head
        };
        let ix = head.index();
        self.data[ix - 1] = T::new(0);
        self.data[ix] = capacity_id;
        self.data[ix + 1] = T::new(0);
        self.data[ix + 2] = T::new(0);
        debug_assert_eq!(
            self.get_status(ix),
            Some(ListStatus::Reserved {
                capacity: goal_capacity
            })
        );
        Ok(head)
    }

    #[inline]
    fn reallocate(&mut self, id: T, capacity: usize) -> Result<T, ()> {
        if self.try_reallocate(id, capacity).is_ok() {
            return Ok(id);
        }
        let new_id = self.allocate_unique(capacity)?;
        self.extend_from_range_unchecked(new_id.index(), id.index(), 0, self.get_len(id))
            .unwrap();
        debug_assert!(self.total_free_blocks() >= (0, 0));
        Ok(new_id)
    }

    #[inline]
    fn try_reallocate(&mut self, id: T, capacity: usize) -> Result<(), ()> {
        let capacity = round_up_to_size_class(capacity);
        let curr_capacity = self.get_capacity(id);
        if curr_capacity >= capacity {
            return Ok(());
        }
        let ix = id.index();
        if ix == 0 {
            return Err(());
        }
        T::try_new(capacity).ok_or(())?;
        let end = ix + curr_capacity + 1;
        debug_assert!(end == self.next_block(ix) || end == self.next_block(self.next_block(ix)));
        let mut curr = end;
        let mut available_capacity = curr_capacity;
        while available_capacity < capacity {
            match self.get_status(curr) {
                Some(ListStatus::Free { capacity, .. }) => {
                    self.pop_from_free_list(curr);
                    available_capacity += capacity + 1;
                    curr = self.next_block(curr);
                }
                Some(other) => {
                    debug_assert!(
                        !matches!(other, ListStatus::Back { .. }),
                        "Cannot have a back-allocation directly after a free block"
                    );
                    if available_capacity > curr_capacity {
                        self.push_to_free_list(end, available_capacity - curr_capacity - 1)
                    }
                    return Err(());
                }
                None => {
                    self.data
                        .extend(std::iter::repeat(T::new(0)).take(capacity - available_capacity));
                    available_capacity = capacity;
                }
            }
        }
        let new_end = ix + capacity + 1;
        let remainder = available_capacity - capacity;
        if remainder > 0 {
            self.push_to_free_list(new_end, remainder - 1)
        }
        let len = self.data[ix - 1].index();
        self.fixup_len_and_back(ix, len, capacity);
        debug_assert_eq!(self.get_capacity(id), capacity);
        Ok(())
    }

    #[inline(always)]
    fn deallocate(&mut self, id: T) {
        let ix = id.index();
        if ix != 0 {
            self.push_to_free_list(id.index(), self.get_capacity(id))
        }
    }

    #[inline(always)]
    fn clear_list(&mut self, id: T, preserve_capacity: bool) -> T {
        if preserve_capacity {
            let ix = id.index();
            if ix != 0 {
                let capacity = self.get_capacity(id);
                self.data[ix - 1] = T::new(0);
                self.data[ix] = T::new(capacity);
                self.data[ix + 1] = T::new(0);
                self.data[ix + 2] = T::new(0);
                debug_assert_eq!(self.get_status(ix), Some(ListStatus::Reserved { capacity }));
            }
            id
        } else {
            self.deallocate(id);
            T::new(0)
        }
    }

    #[inline(always)]
    fn try_clear(&mut self, id: T, _preserve_capacity: bool) -> Result<(), ()> {
        let new_id = self.clear_list(id, true);
        debug_assert_eq!(new_id.index(), id.index(), "clearing a list while preserving capacity should never re-allocate or return a zero pointer in this implementation");
        Ok(())
    }

    #[inline]
    fn get_slice(&self, ix: T) -> &[T] {
        &self.data[self.get_slice_indices(ix)]
    }

    #[inline]
    fn get_slice_mut(&mut self, ix: T) -> &mut [T] {
        let ix = self.get_slice_indices(ix);
        &mut self.data[ix]
    }

    #[inline(always)]
    fn get_len(&self, id: T) -> usize {
        let ix = id.index();
        if ix == 0 {
            return 0;
        }
        debug_assert!(matches!(
            self.get_status(ix),
            Some(ListStatus::Occupied { .. } | ListStatus::Reserved { .. })
        ));
        self.data[ix - 1].index()
    }

    /// Get the capacity of a list in this pool
    #[inline(always)]
    fn get_capacity(&self, id: T) -> usize {
        self.get_capacity_ix(id.index())
    }

    #[inline(always)]
    fn is_empty(&self, id: T) -> bool {
        self.get_len(id) == 0
    }

    #[inline(always)]
    fn push(&mut self, id: T, item: T) -> Result<T, T> {
        self.extend_from_slice(id, &[item]).map_err(|_| item)
    }

    #[inline]
    fn try_push(&mut self, id: T, item: T) -> Result<(), T> {
        self.try_extend_from_slice(id, &[item]).map_err(|_| item)
    }

    #[inline]
    fn pop(&mut self, id: T) -> Option<(T, T)> {
        let ix = id.index();
        let len = self.get_len(id);
        if len == 0 {
            return None;
        }
        let popped = self.data[ix + len - 1];
        self.fixup_len_and_back(ix, len - 1, self.get_capacity(id));
        debug_assert!(if len == 1 {
            matches!(self.get_status(ix), Some(ListStatus::Reserved { .. }))
        } else {
            self.get_status(ix) == Some(ListStatus::Occupied { len: len - 1 })
        });
        Some((id, popped))
    }

    #[inline(always)]
    fn try_pop(&mut self, id: T) -> Result<Option<T>, ()> {
        Ok(self.pop(id).map(|(new_id, item)| {
            debug_assert_eq!(id.index(), new_id.index(), "Invalid pop implementation: pop should never require re-allocation in an IndexPool!");
            item
        }))
    }

    #[inline]
    fn truncate(
        &mut self,
        id: T,
        len: usize,
    ) -> T {
        let ix = id.index();
        let capacity = self.get_capacity_ix(ix);
        self.fixup_len_and_back(ix, len, capacity);
        id
    }

    #[inline]
    fn try_truncate(
        &mut self,
        id: T,
        len: usize,
    ) -> Result<(), ()> {
        let ix = id.index();
        let capacity = self.get_capacity_ix(ix);
        self.fixup_len_and_back(ix, len, capacity);
        Ok(())
    }

    #[inline]
    fn extend<I: Iterator<Item = T>>(&mut self, id: T, mut iter: I) -> Result<T, (T, I)> {
        let size_hint = iter.size_hint().0;
        let original_capacity = self.get_capacity(id);
        let Ok(mut id) = self.reserve(id, size_hint) else { return Err((id, iter)) };
        let mut ix = id.index();
        let original_len = self.get_len_ix(ix);
        let mut len = original_len;
        let mut capacity = self.get_capacity_ix(ix);
        // Push the elements of `iter` to the end of the array
        loop {
            if let Some(next) = iter.next() {
                if len >= capacity {
                    // We're out of capacity, so re-allocate!
                    self.fixup_len_and_back(ix, len, capacity);
                    debug_assert_eq!(self.get_len_ix(ix), len);
                    debug_assert_eq!(self.get_capacity_ix(ix), len);
                    if let Ok(new_id) = self.reallocate(id, capacity) {
                        id = new_id;
                        ix = new_id.index();
                        capacity = self.get_capacity_ix(ix);
                        debug_assert_eq!(self.get_len_ix(ix), len);
                    } else {
                        // Failed to re-allocate, so truncate extraneous elements, restore original capacity, and return an error
                        self.fixup_len_and_back(ix, len, capacity);
                        self.try_reallocate(id, original_capacity).expect("shrinking capacity should never move an allocation!");
                        debug_assert_eq!(self.get_len_ix(ix), original_len);
                        return Err((id, iter))
                    }
                }
                debug_assert!(len < capacity);
                // Push the element onto the back of the array
                self.data[ix + len] = next;
                len += 1;
            } else {
                // No more elements in the iterator, so fixup our allocation and exit
                self.fixup_len_and_back(ix, len, capacity);
                debug_assert_eq!(self.get_len_ix(ix), len);
                debug_assert_eq!(self.get_capacity_ix(ix), capacity);
                return Ok(id)
            }
        }
    }

    #[inline]
    fn try_extend<I: Iterator<Item = T>>(&mut self, id: T, mut iter: I) -> Result<(), I> {      
        let ix = id.index();
        let original_capacity = self.get_capacity_ix(ix);  
        let size_hint = iter.size_hint().0;
        let Ok(()) = self.try_reserve(id, size_hint) else { return Err(iter) };
        let original_len = self.get_len_ix(ix);
        let mut len = original_len;
        let mut capacity = self.get_capacity_ix(ix);
        // Push the elements of `iter` to the end of the array
        loop {
            if let Some(next) = iter.next() {
                if len >= capacity {
                    // We're out of capacity, so re-allocate!
                    self.fixup_len_and_back(ix, len, capacity);
                    debug_assert_eq!(self.get_len_ix(ix), len);
                    debug_assert_eq!(self.get_capacity_ix(ix), len);
                    if self.try_reallocate(id, capacity).is_ok() {
                        capacity = self.get_capacity_ix(ix);
                        debug_assert_eq!(self.get_len_ix(ix), len);
                    } else {
                        // Failed to re-allocate, so truncate extraneous elements and return an error
                        self.fixup_len_and_back(ix, len, capacity);
                        self.try_reallocate(id, original_capacity).expect("shrinking capacity should never move an allocation!");
                        //TODO: restore to original capacity?
                        debug_assert_eq!(self.get_len_ix(ix), original_len);
                        return Err(iter)
                    }
                }
                debug_assert!(len < capacity);
                // Push the element onto the back of the array
                self.data[ix + len] = next;
                len += 1;
            } else {
                // No more elements in the iterator, so fixup our allocation and exit
                self.fixup_len_and_back(ix, len, capacity);
                debug_assert_eq!(self.get_len_ix(ix), len);
                debug_assert_eq!(self.get_capacity_ix(ix), capacity);
                return Ok(())
            }
        }
    }

    #[inline]
    fn extend_from_range(&mut self, id: T, src: T, begin: usize, end: usize) -> Result<T, T>
    where
        T: Clone,
    {
        let id = self
            .reserve(id, end.saturating_sub(begin))
            .map_err(|_| id)?;
        self.extend_from_range_unchecked(id.index(), src.index(), begin, end)
            .unwrap();
        Ok(id)
    }

    #[inline]
    fn try_extend_from_range(&mut self, id: T, src: T, begin: usize, end: usize) -> Result<(), ()>
    where
        T: Clone,
    {
        self.try_reserve(id, end.saturating_sub(begin))?;
        self.extend_from_range_unchecked(id.index(), src.index(), begin, end)?;
        Ok(())
    }

    #[inline]
    fn extend_from_slice(&mut self, id: T, slice: &[T]) -> Result<T, T> {
        let id = self.reserve(id, slice.len()).map_err(|_| id)?;
        self.extend_slice_unchecked(id.index(), slice).unwrap();
        Ok(id)
    }

    #[inline]
    fn try_extend_from_slice(&mut self, id: T, slice: &[T]) -> Result<(), ()> {
        self.try_reserve(id, slice.len())?;
        self.extend_slice_unchecked(id.index(), slice).unwrap();
        Ok(())
    }

    #[inline]
    fn deep_clone(&mut self, id: T) -> Result<T, ()> {
        let len = self.get_len(id);
        let new = self.allocate(len)?;
        if len != 0 {
            self.extend_from_range_unchecked(new.index(), id.index(), 0, len)
                .unwrap();
        }
        Ok(new)
    }

    #[inline(always)]
    fn eq_in(&self, left: T, right: T) -> bool
    where
        T: PartialEq,
    {
        left == right || self.get_slice(left) == self.get_slice(right)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn consecutive_free_block_exercise<
        T: Clone + Eq + Debug,
        K: Copy + EntityIx,
        L: ListPool<T, K>,
    >(
        items: Vec<Vec<T>>,
        extras: Vec<T>,
        pool: &mut L,
    ) {
        let prelude = |pool: &mut L| {
            let mut lists = Vec::with_capacity(items.len());
            for items in items.iter() {
                lists.push(EntityList::from_iter(items.iter().cloned(), pool).unwrap());
                assert_eq!(lists.last().unwrap().as_slice(pool), items);
            }
            for (list, items) in lists.iter_mut().zip(&items) {
                assert_eq!(list.as_slice(pool), items);
                list.clear(false, pool);
                assert_eq!(list.as_slice(pool), &[]);
            }
            for list in lists.iter() {
                assert_eq!(list.as_slice(pool), &[])
            }
        };
        prelude(pool);
        let mut big_list = EntityList::new();
        for items in items.iter() {
            let original_len = big_list.len(pool);
            big_list.extend(items.iter().cloned(), pool).unwrap();
            debug_assert_eq!(big_list.len(pool) - original_len, items.len());
        }
        let mut big_list_template = Vec::from_iter(items.iter().flatten().cloned());
        assert_eq!(big_list.as_slice(pool), big_list_template);
        prelude(pool);
        let mut big_list_2 = EntityList::new();
        for items in items.iter() {
            let original_len = big_list_2.len(pool);
            big_list_2.extend(items.iter().cloned(), pool).unwrap();
            debug_assert_eq!(big_list_2.len(pool) - original_len, items.len());
        }
        big_list_2.extend(extras.iter().cloned(), pool).unwrap();
        big_list_template.extend(extras.iter().cloned());
        assert_eq!(big_list_2.as_slice(pool).len(), big_list_template.len());
        assert_eq!(big_list_2.as_slice(pool), big_list_template);
        prelude(pool);
        big_list.clear(false, pool);
        big_list_2.clear(false, pool)
    }

    #[test]
    fn small_extensions() {
        let mut pool: IndexPool<u32> = IndexPool::new();
        let mut list = EntityList::new();
        list.extend_from_slice(&[], &mut pool).unwrap();
        debug_assert_eq!(list.as_slice(&pool), &[]);
        list.extend(std::iter::empty(), &mut pool).unwrap();
        debug_assert_eq!(list.as_slice(&pool), &[]);
        list.extend_from_slice(&[1], &mut pool).unwrap();
        debug_assert_eq!(list.as_slice(&pool), &[1]);
        list.extend(std::iter::once(2), &mut pool).unwrap();
        debug_assert_eq!(list.as_slice(&pool), &[1, 2]);
    }

    #[test]
    fn index_pool_consecutive_free_block_tests() {
        let mut pool: IndexPool<u32> = IndexPool::new();
        consecutive_free_block_exercise(
            vec![
                Vec::from_iter(5050..5100),
                Vec::from_iter(9090..9150),
                Vec::from_iter(9350..9900),
                Vec::from_iter(7000..8200),
            ],
            Vec::from_iter(15000..16000),
            &mut pool,
        )
    }

    fn rev_list_test<T: Clone + Eq + Debug, K: Copy + EntityIx>(
        mut items: Vec<T>,
        pool: &mut impl ListPool<T, K>,
    ) {
        let mut list = EntityList::new();
        list.extend_from_slice(&items, pool).unwrap();
        assert_eq!(list.as_slice(pool), items);
        let mut list2 = EntityList::from_slice(&items, pool).unwrap();
        assert_eq!(list.as_slice(pool), items);
        assert_eq!(list2.as_slice(pool), items);
        assert!(list.eq_in(list2, pool));
        list2.clear(true, pool);
        assert_eq!(list.as_slice(pool), items);
        assert_eq!(list2.as_slice(pool), &[]);
        list2.extend(items.iter().cloned(), pool).unwrap();
        assert_eq!(list.as_slice(pool), items);
        assert_eq!(list2.as_slice(pool), items);
        list2.clear(false, pool);
        assert_eq!(list2.as_slice(pool), &[]);
        let mut rev = EntityList::new();
        while !list.is_empty(pool) {
            let elem = list.pop(pool).unwrap();
            rev.push(elem, pool).unwrap();
        }
        assert_eq!(list.as_slice(pool), &[]);
        items.reverse();
        assert_eq!(rev.as_slice(pool), items);
        list.clear(false, pool);
        rev.clear(false, pool);
    }

    #[test]
    fn index_pool_rev_tests() {
        let mut pool: IndexPool<u32> = IndexPool::new();
        rev_list_test(Vec::from_iter(500..1000), &mut pool);
    }

    #[test]
    fn index_pool_push_pop_100() {
        let mut pool: IndexPool<u32> = IndexPool::new();
        let mut v = Vec::with_capacity(100);
        let mut acc = pool.allocate(0).unwrap();
        for i in 0..100 {
            acc = pool.push(acc, i + 1000).unwrap();
            v.push(i + 1000);
            assert_eq!(pool.get_slice(acc), v);
            assert_eq!(pool.get_slice_mut(acc), v);
        }
        for i in (0..100).rev() {
            assert_eq!(pool.try_pop(acc).unwrap(), Some(i + 1000));
            v.pop();
            assert_eq!(pool.get_slice(acc), v);
            assert_eq!(pool.get_slice_mut(acc), v);
        }
        assert_eq!(pool.pop(acc), None)
    }

    #[test]
    fn index_pool_basic_usage() {
        let mut pool: IndexPool<u32> = IndexPool::new();
        let zero1 = pool.allocate(0).unwrap();
        let zero2 = pool.allocate(0).unwrap();
        assert_eq!(zero1, 0);
        assert_eq!(zero2, 0);
        assert_eq!(pool.get_len(zero1), 0);
        assert_eq!(pool.get_slice(zero1), &[]);
        assert_eq!(
            pool.get_status(zero1.index()),
            Some(ListStatus::Occupied { len: 0 })
        );
        assert_eq!(pool.total_free_blocks(), (0, 0));
        let list1 = pool.allocate(53).unwrap();
        let list2 = pool.allocate(2).unwrap();
        assert_ne!(list1, list2);
        assert_ne!(list1, 0);
        assert_ne!(list2, 0);
        assert_eq!(pool.get_capacity(list1), 63);
        assert_eq!(pool.get_capacity(list2), 3);
        let usage = pool.memory_usage();
        assert!(usage >= 4 * (64 + 4));
        assert!(usage >= pool.min_memory_usage());
        assert_eq!(pool.get_slice(list1), &[]);
        assert_eq!(pool.get_slice(list2), &[]);
        assert_eq!(pool.get_len(list1), 0);
        assert_eq!(pool.get_len(list2), 0);
        assert_eq!(pool.total_free_blocks(), (0, 0));
        pool.try_push(list1, 3).unwrap();
        assert_eq!(pool.push(list2, 3).unwrap(), list2);
        assert_eq!(pool.get_slice(list1), &[3]);
        assert_eq!(pool.get_slice(list2), &[3]);
        assert_eq!(pool.get_len(list1), 1);
        assert_eq!(pool.get_len(list2), 1);
        assert_eq!(pool.get_capacity(list1), 63);
        assert_eq!(pool.get_capacity(list2), 3);
        assert_eq!(pool.min_memory_usage(), 4 * (64 + 4));
        assert_eq!(pool.memory_usage(), usage);
        assert_eq!(pool.total_free_blocks(), (0, 0));
        pool.try_push(list2, 3).unwrap();
        assert_eq!(pool.push(list1, 7).unwrap(), list1);
        assert_eq!(pool.get_slice(list1), &[3, 7]);
        assert_eq!(pool.get_slice(list2), &[3, 3]);
        assert_eq!(pool.get_len(list1), 2);
        assert_eq!(pool.get_len(list2), 2);
        assert_eq!(pool.get_capacity(list1), 63);
        assert_eq!(pool.get_capacity(list2), 3);
        assert_eq!(pool.min_memory_usage(), 4 * (64 + 4));
        assert_eq!(pool.memory_usage(), usage);
        assert_eq!(pool.total_free_blocks(), (0, 0));
        pool.try_push(list2, 10).unwrap();
        assert_eq!(pool.push(list1, 9).unwrap(), list1);
        assert_eq!(pool.get_slice(list1), &[3, 7, 9]);
        assert_eq!(pool.get_slice(list2), &[3, 3, 10]);
        assert_eq!(pool.get_len(list1), 3);
        assert_eq!(pool.get_len(list2), 3);
        assert_eq!(pool.get_capacity(list1), 63);
        assert_eq!(pool.get_capacity(list2), 3);
        assert_eq!(pool.memory_usage(), usage);
        // This works since try_push currently allows pushing to the end. Maybe we'll change this later...
        // assert_eq!(pool.try_push(list2, 15).unwrap_err(), 15);
        // assert_eq!(pool.total_free_blocks(), (0, 0));
        let list3 = pool.push(list2, 1).unwrap();
        assert_eq!(list2, list3);
        assert_eq!(pool.total_free_blocks(), (0, 0));
        let list4 = pool.allocate(3).unwrap();
        assert_ne!(list3, list4);
        assert_eq!(pool.total_free_blocks(), (0, 0));
        pool.deallocate(list3);
        assert_eq!(pool.total_free_blocks(), (1, round_up_to_size_class(4)));
        assert_eq!(
            pool.get_status(list3 as usize),
            Some(ListStatus::Free {
                capacity: round_up_to_size_class(4),
                prev: list3 as usize,
                next: list3 as usize
            })
        );
        let list3_1 = pool.allocate(3).unwrap();
        assert_eq!(list3_1, list3);
        assert_eq!(pool.get_slice(list3_1), &[]);
        let list3_2 = pool.allocate(3).unwrap();
        assert_eq!(list3_2, list3 + 4);
        assert!(!pool.is_empty(list1));
        assert_eq!(pool.get_slice(list1), &[3, 7, 9]);
        assert_eq!(pool.get_slice_mut(list1), &mut [3, 7, 9]);
        let list4 = pool.clone_pushed(list1, 8).unwrap();
        assert_eq!(pool.get_len(list4), 4);
        assert_ne!(list1, list4);
        assert_eq!(pool.get_slice(list1), &[3, 7, 9]);
        assert_eq!(pool.get_slice(list4), &[3, 7, 9, 8]);
        assert_eq!(pool.get_slice(list3_1), &[]);
        assert_eq!(pool.get_slice(list3_2), &[]);
        assert_eq!(pool.push(list3_2, 7).unwrap(), list3_2);
        assert_eq!(pool.get_slice(list3_1), &[]);
        assert_eq!(pool.get_slice(list3_2), &[7]);
        pool.clear_list(list1, true);
        assert!(pool.is_empty(list1));
        assert_eq!(pool.get_slice(list1), &[]);
        assert_eq!(pool.clear_list(list3_2, false), 0);
        assert_eq!(pool.get_slice(list3_1), &[]);
        pool.try_clear(list3_1, false).unwrap();
        assert_eq!(pool.get_slice(list3_1), &[]);

        let mut v = Vec::with_capacity(100);
        let mut acc = list3_1;
        for i in 0..100 {
            acc = pool.push(acc, i).unwrap();
            v.push(i);
            assert_eq!(pool.get_slice(acc), v);
            assert_eq!(pool.get_slice_mut(acc), v);
        }
    }

    #[test]
    fn index_pool_u16_overflow() {
        let mut pool: IndexPool<u16> = IndexPool::new();
        let mut slice = Vec::from_iter((0..65535).map(EntityIx::new));
        for _ in 0..2 {
            let mut list = EntityList::from_iter(slice.iter().cloned(), &mut pool).unwrap();
            assert_eq!(list.as_slice(&mut pool), slice);
            list.clear(false, &mut pool);
        }
        slice.push(65535);
        let _ = EntityList::from_iter(slice.iter().cloned(), &mut pool).unwrap_err();
    }

    #[test]
    fn index_pool_u16_failed_alloc() {
        let mut pool: IndexPool<u16> = IndexPool::new();
        pool.allocate(65536).unwrap_err();
        assert_eq!(pool, IndexPool::new());
        pool.allocate_from_iter(0..=65535).unwrap_err();
        assert_eq!(pool, IndexPool::new());
        let max_alloc = pool.allocate(65535).unwrap();
        assert_eq!(pool.get_capacity(max_alloc), 65535);
        
        // Allocating zero should just return the zero pointer
        assert_eq!(0, pool.allocate(0).unwrap());
        // *But*, the pool *is* genuinely out of memory!
        pool.allocate(1).unwrap_err();

        assert_eq!(max_alloc, pool.extend(max_alloc, 0..65535).unwrap());
    }

    #[test]
    fn index_pool_u16_multiple_large_allocs() {
        let mut pool: IndexPool<u16> = IndexPool::new();
        let big_alloc_1 = pool.allocate(32767).unwrap();
        let big_alloc_2 = pool.allocate(32767).unwrap();
        assert_ne!(big_alloc_1, big_alloc_2);

        // Allocating zero should just return the zero pointer
        assert_eq!(0, pool.allocate(0).unwrap());
        // *But*, the pool *is* genuinely out of memory!
        pool.allocate(1).unwrap_err();


        assert_eq!(big_alloc_1, pool.extend(big_alloc_1, 0..32767).unwrap());
        assert_eq!(big_alloc_2, pool.extend(big_alloc_2, 0..32767).unwrap());

        pool.deallocate(big_alloc_1);
        debug_assert_eq!(pool.allocate(1).unwrap(), big_alloc_1);
        pool.reallocate(big_alloc_1, 32767).unwrap();
        assert_eq!(big_alloc_1, pool.extend(big_alloc_1, 0..32767).unwrap());

        // This is not an overflow *since*:
        // - All capacities fit in `T == u::16`
        // - All block indices fit in `T == u16`
        // *Even though* `self.data.len() > u16::MAX`!
        debug_assert_eq!(big_alloc_2, pool.reallocate(big_alloc_2, 65535).unwrap());
        assert_eq!(big_alloc_2, pool.extend(big_alloc_2, 32767..65535).unwrap());
        pool.push(big_alloc_2, 65535).unwrap_err();

        pool.deallocate(big_alloc_2);
        for _ in 0..3 {
            let _ = pool.allocate(4095).unwrap();
        }
        let tail_alloc = pool.allocate(4095).unwrap();
        assert_eq!(tail_alloc, pool.extend(tail_alloc, 0..65535).unwrap());
        assert_eq!(pool.get_len(tail_alloc), 65535);
        pool.push(tail_alloc, 65535).unwrap_err();
    }

    #[test]
    fn index_list_size_class_sanity() {
        struct SizeClassExample {
            size: usize,
            size_class_ix: u32,
            rounded_size: usize,
        }

        impl SizeClassExample {
            fn validate(&self) {
                debug_assert!(self.size <= self.rounded_size);
                debug_assert_eq!(
                    get_smallest_size_class_larger_than(self.size),
                    self.size_class_ix
                );
                debug_assert_eq!(get_size_class_len(self.size_class_ix), self.rounded_size);
                debug_assert_eq!(round_up_to_size_class(self.size), self.rounded_size);
            }
        }

        let examples = [
            SizeClassExample {
                size: 0,
                size_class_ix: 0,
                rounded_size: 3,
            },
            SizeClassExample {
                size: 1,
                size_class_ix: 0,
                rounded_size: 3,
            },
            SizeClassExample {
                size: 2,
                size_class_ix: 0,
                rounded_size: 3,
            },
            SizeClassExample {
                size: 3,
                size_class_ix: 0,
                rounded_size: 3,
            },
            SizeClassExample {
                size: 4,
                size_class_ix: 1,
                rounded_size: 7,
            },
        ];

        for example in examples {
            example.validate()
        }
    }
}
