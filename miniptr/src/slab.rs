/*!
A slab allocator, returning small pointers to pre-allocated storage of a uniformly sized type
*/
use super::*;
use std::ops::{Index, IndexMut};

/// An arena mapping indices `K` to values `V`, supporting a free list
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Slab<K, S> {
    arena: Vec<S>,
    free_head: K,
    free_len: usize,
}

impl<K, S> Default for Slab<K, S>
where
    K: EntityIx,
    S: Freeable<K>,
{
    #[inline(always)]
    fn default() -> Self {
        Self::new()
    }
}

impl<K, S> Index<K> for Slab<K, S>
where
    K: EntityIx,
    S: Freeable<K>,
{
    type Output = S::Value;

    fn index(&self, index: K) -> &Self::Output {
        self.arena[index.index()].slot_value()
    }
}

impl<K, S> IndexMut<K> for Slab<K, S>
where
    K: EntityIx,
    S: Freeable<K>,
{
    fn index_mut(&mut self, index: K) -> &mut Self::Output {
        self.arena[index.index()].slot_value_mut()
    }
}

impl<K, S> Slab<K, S>
where
    K: EntityIx,
    S: Freeable<K>,
{
    /// Create a new, empty arena
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::*;
    /// # use either::Either;
    /// #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    /// struct BasicEntity(u32);
    /// entity_impl!(BasicEntity, u32, false);
    /// let a: Slab<BasicEntity, Either<u64, BasicEntity>> = Slab::new();
    /// assert!(a.is_empty());
    /// assert_eq!(a.len(), 0);
    /// ```
    #[inline(always)]
    pub fn new() -> Slab<K, S> {
        Self::with_capacity(0)
    }

    /// Create a new, empty arena with the given capacity
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::*;
    /// # use either::Either;
    /// #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    /// struct BasicEntity(u32);
    /// entity_impl!(BasicEntity, u32, false);
    /// let a: Slab<BasicEntity, Either<u64, BasicEntity>> = Slab::with_capacity(32);
    /// assert!(a.is_empty());
    /// assert_eq!(a.len(), 0);
    /// ```
    #[inline]
    pub fn with_capacity(capacity: usize) -> Slab<K, S> {
        Slab {
            arena: Vec::with_capacity(capacity),
            free_head: K::new(0),
            free_len: 0,
        }
    }

    /// Is this arena completely empty
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::*;
    /// # use either::Either;
    /// #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    /// struct BasicEntity(u32);
    /// entity_impl!(BasicEntity, u32, false);
    /// let mut a: Slab<BasicEntity, Either<u64, BasicEntity>> = Slab::new();
    /// assert!(a.is_empty());
    /// a.insert(2);
    /// assert!(!a.is_empty());
    /// ```
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.free_len == self.arena.len()
    }

    /// Get the total number of entities in this arena
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::*;
    /// # use either::Either;
    /// #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    /// struct BasicEntity(u32);
    /// entity_impl!(BasicEntity, u32, false);
    /// let mut a: Slab<BasicEntity, Either<u64, BasicEntity>> = Slab::new();
    /// assert_eq!(a.len(), 0);
    /// let zero = a.insert(2);
    /// assert_eq!(a.len(), 1);
    /// a.insert(3);
    /// assert_eq!(a.len(), 2);
    /// a.remove(zero);
    /// assert_eq!(a.len(), 1);
    /// ```
    #[inline]
    pub fn len(&self) -> usize {
        self.arena.len() - self.free_len
    }

    /// Remove all entries from this arena, preserving its current capacity
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::*;
    /// # use either::Either;
    /// #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    /// struct BasicEntity(u32);
    /// entity_impl!(BasicEntity, u32, false);
    /// let mut a: Slab<BasicEntity, Either<u64, BasicEntity>> = Slab::new();
    /// a.insert(3);
    /// a.clear();
    /// assert!(a.is_empty());
    /// ```
    #[inline]
    pub fn clear(&mut self) {
        self.arena.clear();
        self.free_len = 0;
        self.free_head = K::new(0);
    }

    /// Get the key that will be assigned to the next inserted value
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::*;
    /// # use either::Either;
    /// #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    /// struct BasicEntity(u32);
    /// entity_impl!(BasicEntity, u32, false);
    /// let mut a: Slab<BasicEntity, Either<u64, BasicEntity>> = Slab::new();
    /// let zero = a.next_key();
    /// assert_eq!(a.insert(3), zero);
    /// a.remove(zero);
    /// let zero_ = a.next_key();
    /// assert_eq!(zero, zero_);
    /// assert_eq!(a.insert(3), zero);
    /// let one = a.next_key();
    /// assert_eq!(a.insert(3), one);
    /// let two = a.next_key();
    /// assert_ne!(two, zero);
    /// assert_ne!(two, one);
    /// a.remove(zero);
    /// assert_eq!(a.next_key(), zero);
    /// assert_eq!(a.insert(5), zero);
    /// assert_eq!(a.next_key(), two);
    /// ```
    #[inline(always)]
    pub fn next_key(&self) -> K {
        self.free_head
    }

    /// Insert `v` into the mapping, assigning a new key which is returned
    ///
    /// Panics if the mapping has run out of space
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::*;
    /// # use either::Either;
    /// #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    /// struct BasicEntity(u32);
    /// entity_impl!(BasicEntity, u32, false);
    /// let mut a: Slab<BasicEntity, Either<u64, BasicEntity>> = Slab::new();
    /// assert_eq!(a.len(), 0);
    /// let zero = a.insert(2);
    /// assert_eq!(a.len(), 1);
    /// let one = a.insert(2);
    /// assert_eq!(a.len(), 2);
    /// let two = a.insert(3);
    /// assert_eq!(a.len(), 3);
    /// assert_ne!(zero, one);
    /// assert_ne!(zero, two);
    /// assert_ne!(one, two);
    /// assert_eq!(a[zero], 2);
    /// assert_eq!(a[one], 2);
    /// assert_eq!(a[two], 3);
    /// ```
    #[inline(always)]
    pub fn insert(&mut self, v: S::Value) -> K {
        match self.try_insert(v) {
            Ok(k) => k,
            Err(_) => panic!(
                "Slab mapping out of space: current size {:?}",
                self.arena.len()
            ),
        }
    }

    /// Insert `v` into the mapping, assigning a new key which is returned
    ///
    /// Returns `v` as an error if the mapping has run out of space
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::*;
    /// # use either::Either;
    /// #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    /// struct TinyEntity(u8);
    /// entity_impl!(TinyEntity, u8, true);
    /// let mut a: Slab<TinyEntity, Either<usize, TinyEntity>> = Slab::new();
    /// // Note: we can store maximum 254 slots, *not* 255 = 2^8 - 1, since not
    /// // only do we is the reserved value `u8::MAX` an invalid index, but also
    /// // the head of the free list, which is here at the end of the array, i.e.
    /// // position 255.
    /// for i in 0..254 {
    ///     assert_eq!(a.len(), i);
    ///     let ix = a.insert(i);
    ///     assert_eq!(a[ix], i);
    /// }
    /// assert_eq!(a.try_insert(254), Err(254));
    /// ```
    #[inline]
    pub fn try_insert(&mut self, v: S::Value) -> Result<K, S::Value> {
        let old_free_head = self.free_head;
        if let Some(slot) = self.arena.get_mut(old_free_head.index()) {
            debug_assert_ne!(
                self.free_len,
                0,
                "Found free slot at head of free list ({}), but self.free_len == 0",
                self.free_head.index()
            );
            let new_free_head = slot.set_value(v).next_free();
            self.free_head = new_free_head;
            self.free_len -= 1;
            Ok(old_free_head)
        } else {
            debug_assert_eq!(
                self.free_len, 0,
                "Since there are no slots in the free list, self.free_len must be equal to 0"
            );
            debug_assert_eq!(
                self.free_head.index(),
                self.arena.len(),
                "Since there are no slots in the free list, self.free_head point one-past-the-end of the active arena"
            );
            let Some(new_free_head) = K::try_new(self.arena.len() + 1) else {
                return Err(v)
            };
            self.arena.push(S::from_slot_value(v));
            self.free_head = new_free_head;
            Ok(old_free_head)
        }
    }

    /// Delete the key `k` from the mapping, returning the old value
    ///
    /// Panics or returns an unspecified value, leaving the `Slab` in an unspecified but valid state, if `k` is invalid.
    ///
    /// # Examples
    /// ```rust
    /// # use miniptr::*;
    /// # use either::Either;
    /// #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    /// struct BasicEntity(u32);
    /// entity_impl!(BasicEntity, u32, false);
    /// let mut a: Slab<BasicEntity, Either<u64, BasicEntity>> = Slab::new();
    /// assert_eq!(a.len(), 0);
    /// let zero = a.insert(4);
    /// assert_eq!(a.len(), 1);
    /// assert_eq!(a.remove(zero), 4);
    /// assert_eq!(a.len(), 0);
    /// ```
    #[inline(always)]
    pub fn remove(&mut self, k: K) -> S::Value {
        let value = self.arena[k.index()]
            .set_slot(Either::Right(self.free_head))
            .into_slot_value();
        self.free_head = k;
        self.free_len += 1;
        value
    }

    /// Reserves capacity for at least `additional` more elements to be inserted
    #[inline(always)]
    pub fn reserve(&mut self, additional: usize) {
        self.arena.reserve(additional)
    }

    /// Reserves the minimum capacity for exactly `additional` more elements to be inserted
    #[inline(always)]
    pub fn reserve_exact(&mut self, additional: usize) {
        self.arena
            .reserve_exact(additional.saturating_sub(self.free_len))
    }

    /// Shrinks the capacity of the `Slab` as much as possible
    ///
    /// Note that the resulting capacity may still be larger than `self.len()`
    #[inline(always)]
    pub fn shrink_to_fit(&mut self) {
        self.arena.shrink_to_fit()
    }

    /// Get the total capacity of the `Slab`
    #[inline(always)]
    pub fn capacity(&self) -> usize {
        self.arena.capacity()
    }
}
