use criterion::{black_box, criterion_group, criterion_main, BenchmarkId, Criterion};
use miniptr::list::*;
use miniptr::*;

fn bench_from_slice_helper<K: EntityIx + std::fmt::Debug>(
    group_name: &str,
    sizes: impl IntoIterator<Item = usize>,
    pool: &mut impl ListPool<K, K>,
    c: &mut Criterion,
) {
    let mut g = c.benchmark_group(group_name);
    for size in sizes {
        let slice = Vec::from_iter((0..size).map(K::new));
        g.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, _| {
            b.iter(|| {
                let mut list = EntityList::from_slice(&slice, pool)
                    .ok()
                    .expect("Allocation should succeed");
                black_box(list.as_slice_mut(pool));
                assert_eq!(list.as_slice(pool), slice);
                list.clear(false, pool);
            })
        });
    }
    g.finish()
}

fn bench_from_iter_helper<K: EntityIx + std::fmt::Debug>(
    group_name: &str,
    sizes: impl IntoIterator<Item = usize>,
    pool: &mut impl ListPool<K, K>,
    c: &mut Criterion,
) {
    let mut g = c.benchmark_group(group_name);
    for size in sizes {
        let slice = Vec::from_iter((0..size).map(K::new));
        g.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, _| {
            b.iter(|| {
                let mut list = EntityList::from_iter((0..size).map(K::new), pool)
                    .ok()
                    .expect("Allocation should succeed");
                black_box(list.as_slice_mut(pool));
                assert_eq!(list.as_slice(pool), slice);
                list.clear(false, pool);
            })
        });
    }
    g.finish()
}

fn element_list_from_slice(c: &mut Criterion) {
    bench_from_slice_helper(
        "index_pool_from_slice_u16",
        [16, 256, 4096],
        &mut IndexPool::<u16>::new(),
        c,
    );
    bench_from_slice_helper(
        "index_pool_from_slice_u32",
        [16, 256, 4096, 65536 /*1048576*/],
        &mut IndexPool::<u32>::new(),
        c,
    );
    bench_from_slice_helper(
        "index_pool_from_slice_u64",
        [16, 256, 4096, 65536 /*1048576*/],
        &mut IndexPool::<u64>::new(),
        c,
    );
    bench_from_iter_helper(
        "index_pool_from_iter_u16",
        [16, 256, 4096],
        &mut IndexPool::<u16>::new(),
        c,
    );
    bench_from_iter_helper(
        "index_pool_from_iter_u32",
        [16, 256, 4096, 65536 /*1048576*/],
        &mut IndexPool::<u32>::new(),
        c,
    );
    bench_from_iter_helper(
        "index_pool_from_iter_u64",
        [16, 256, 4096, 65536 /*1048576*/],
        &mut IndexPool::<u64>::new(),
        c,
    );
}

criterion_group!(benches, element_list_from_slice);
criterion_main!(benches);
