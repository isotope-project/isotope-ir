/*!
The `isotope-core` intermediate representation
*/
#![forbid(missing_debug_implementations, missing_docs)]
use either::Either;
use miniptr::{
    entity_impl, reserved_max_impl, EntityList, Freeable, IndexPool, ReservedValue, Slab,
};
use std::fmt::Debug;

/// An `isotope_core` function
#[allow(dead_code)]
#[derive(Debug, Clone, Eq, PartialEq, Hash, Default)]
pub struct Function {
    /// This function's arguments
    args: ValList,
    /// This function's return value
    retv: ValId,
    /// The nodes in this function
    nodes: Slab<NodeId, Node>,
    /// The lists in this function
    lists: IndexPool<u32>,
}

/// An index for a list of values
type ValList = EntityList<u32>;

// /// An index for a list of types
// type TypeList = EntityList<u32>;

/// An index for a list of targets
type TargetList = EntityList<u32>;

/// An index for a list of labels
type LabelList = EntityList<u32>;

impl Function {
    /// Get this function's arguments
    #[inline]
    pub fn args(&self) -> &[ValId] {
        ValId::from_arr(self.args.as_slice(&self.lists))
    }

    /// Add a new argument to this function with a given type
    #[inline]
    pub fn add_arg(&mut self, ty: TypeId) -> ValId {
        let param = self.nodes.insert(Node::Param(ty));
        self.args
            .push(param.0, &mut self.lists)
            .expect("list pool overflow: more than 2**32 - 1 list elements");
        ValId(param.0)
    }

    /// Add a new block parameter to this function with a given type
    #[inline]
    pub fn add_param(&mut self, ty: TypeId) -> ValId {
        ValId(self.nodes.insert(Node::Param(ty)).0)
    }

    /// Add a new function call to this function
    #[inline]
    pub fn add_call(&mut self, func: FunctionId, args: &[ValId]) -> ValId {
        match args {
            // [x] => ValId(self.nodes.insert(Node::Call1(func, *x)).0),
            // [x, y] => ValId(self.nodes.insert(Node::Call2(func, *x, *y)).0),
            _ => {
                let list = self.register_list(ValId::to_arr(args));
                ValId(self.nodes.insert(Node::Call(func, list)).0)
            }
        }
    }

    /// Add a new tuple to this function
    #[inline]
    pub fn add_tuple(&mut self, elems: &[ValId], is_array: bool) -> ValId {
        let list = self.register_list(ValId::to_arr(elems));
        ValId(self.nodes.insert(Node::Tuple(list, is_array)).0)
    }

    /// Inject a value into a union
    #[inline]
    pub fn add_inj(&mut self, value: ValId, ix: u32) -> ValId {
        ValId(self.nodes.insert(Node::Inj(value, ix)).0)
    }

    /// Apply the identity function to a value
    #[inline]
    pub fn add_id(&mut self, value: ValId) -> ValId {
        ValId(self.nodes.insert(Node::Id(value)).0)
    }

    /// Construct a `u64`
    #[inline]
    pub fn add_u64(&mut self, value: u64) -> ValId {
        ValId(self.nodes.insert(Node::Bits(64, 1, value)).0)
    }

    /// Construct a `u32`
    #[inline]
    pub fn add_u32(&mut self, value: u32) -> ValId {
        ValId(self.nodes.insert(Node::Bits(32, 1, value as u64)).0)
    }

    /// Construct a `u16`
    #[inline]
    pub fn add_u16(&mut self, value: u16) -> ValId {
        ValId(self.nodes.insert(Node::Bits(16, 1, value as u64)).0)
    }

    /// Construct a `u8`
    #[inline]
    pub fn add_u8(&mut self, value: u8) -> ValId {
        ValId(self.nodes.insert(Node::Bits(8, 1, value as u64)).0)
    }

    /// Construct a `bool`
    #[inline]
    pub fn add_bool(&mut self, value: bool) -> ValId {
        ValId(self.nodes.insert(Node::Bits(1, 1, value as u64)).0)
    }

    /// Construct a break
    #[inline]
    pub fn add_br(&mut self, disc: ValId, br_true: TargetId, br_false: TargetId) -> TargetId {
        TargetId(self.nodes.insert(Node::Break(disc, br_true, br_false)).0)
    }

    /// Construct a jump
    #[inline]
    pub fn add_jmp(&mut self, ix: u32, params: &[ValId]) -> TargetId {
        match params {
            // [x] => TargetId(self.nodes.insert(Node::Jump1(ix, *x)).0),
            // [x, y] => TargetId(self.nodes.insert(Node::Jump2(ix, *x, *y)).0),
            _ => {
                let list = self.register_list(ValId::to_arr(params));
                TargetId(self.nodes.insert(Node::Jump(ix, list)).0)
            }
        }
    }

    /// Construct a label
    #[inline]
    pub fn add_label(&mut self, target: TargetId, params: &[ValId]) -> Label {
        Label(target, self.register_list(ValId::to_arr(params)))
    }

    /// Construct a theta node
    #[inline]
    pub fn add_theta(&mut self, blocks: &[Label], args: &[ValId]) -> ValId {
        let blocks = self.register_list(Label::to_arr(blocks));
        let args = self.register_list(ValId::to_arr(args));
        ValId(self.nodes.insert(Node::Theta(blocks, args)).0)
    }

    /// Remove the nth parameter of this function, returning it's type
    ///
    /// Returns an error if the parameter is invalid
    #[inline]
    pub fn remove_arg(&mut self, ix: usize) -> Result<TypeId, ()> {
        //TODO: make this a proper error
        let param = *self.args.get(ix, &self.lists).ok_or(())?;
        match self.nodes.remove(NodeId(param)) {
            Node::Param(ty) => Ok(ty),
            _ => Err(()), //TODO: make this a proper error
        }
    }

    /// Get this function's return value
    #[inline]
    pub fn retv(&self) -> ValId {
        self.retv
    }

    /// Register this function's return value.
    #[inline]
    pub fn register_retv(&mut self, mut retv: ValId) -> ValId {
        std::mem::swap(&mut self.retv, &mut retv);
        retv
    }

    /// Register a list of nodes into this function
    #[inline]
    fn register_list(&mut self, list: &[u32]) -> EntityList<u32> {
        EntityList::from_slice(list, &mut self.lists)
            .expect("list pool overflow: more than 2**32 - 1 list elements")
    }
}

/// An ID of a function in an `isotope` module
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
#[repr(transparent)]
pub struct FunctionId(u32);
entity_impl!(FunctionId, u32, false);
reserved_max_impl!(FunctionId, u32);

impl Default for FunctionId {
    #[inline(always)]
    fn default() -> Self {
        Self::reserved()
    }
}

/// An ID of a node in an `isotope_core` function
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
#[repr(transparent)]
pub struct NodeId(u32);
entity_impl!(NodeId, u32, false);
reserved_max_impl!(NodeId, u32);

impl Default for NodeId {
    #[inline(always)]
    fn default() -> Self {
        Self::reserved()
    }
}

//TODO: encapsulate as part of `EntityIx` trait?
// impl NodeId {
//     /// Create an array of [`NodeId`] from an array of `u32`
//     fn from_arr(arr: &[u32]) -> &[NodeId] {
//         unsafe { std::mem::transmute(arr) }
//     }
//     /// Create an array of `u32` from an array of [`NodeId`]
//     fn to_arr(arr: &[NodeId]) -> &[u32] {
//         unsafe { std::mem::transmute(arr) }
//     }
// }

/// An ID of a value in an `isotope_core` function
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
#[repr(transparent)]
pub struct ValId(u32);
entity_impl!(ValId, u32, false);
reserved_max_impl!(ValId, u32);

impl From<ValId> for TargetId {
    #[inline(always)]
    fn from(value: ValId) -> Self {
        TargetId(value.0)
    }
}

impl Default for ValId {
    #[inline(always)]
    fn default() -> Self {
        Self::reserved()
    }
}

//TODO: encapsulate as part of `EntityIx` trait?
impl ValId {
    /// Create an array of [`ValId`] from an array of `u32`
    fn from_arr(arr: &[u32]) -> &[ValId] {
        unsafe { std::mem::transmute(arr) }
    }
    /// Create an array of `u32` from an array of [`ValId`]
    fn to_arr(arr: &[ValId]) -> &[u32] {
        unsafe { std::mem::transmute(arr) }
    }
}

/// An ID of a type in an `isotope_core` function
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
#[repr(transparent)]
pub struct TypeId(u32);
entity_impl!(TypeId, u32, false);
reserved_max_impl!(TypeId, u32);

impl Default for TypeId {
    #[inline(always)]
    fn default() -> Self {
        Self::reserved()
    }
}

//TODO: encapsulate as part of `EntityIx` trait?
// impl TypeId {
//     /// Create an array of [`TypeId`] from an array of `u32`
//     fn from_arr(arr: &[u32]) -> &[TypeId] {
//         unsafe { std::mem::transmute(arr) }
//     }
//     /// Create an array of `u32` from an array of [`TypeId`]
//     fn to_arr(arr: &[TypeId]) -> &[u32] {
//         unsafe { std::mem::transmute(arr) }
//     }
// }

/// A label for an `isotope_core` function, consisting of a target and a list of parameters
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
#[repr(C)]
pub struct Label(TargetId, TargetList);

impl Label {
    /// Create an array of [`Label`] from an array of `u32`
    fn from_arr(arr: &[u32]) -> &[Label] {
        //SAFETY: any valid pair of `u32` in memory form a valid `Label`
        unsafe { std::mem::transmute(&arr[..arr.len() - arr.len() % 2]) }
    }
    /// Create an array of `u32` from an array of [`Label`]
    fn to_arr(arr: &[Label]) -> &[u32] {
        unsafe { std::mem::transmute(arr) }
    }
}

/// An ID of a type in an `isotope_core` function
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
#[repr(transparent)]
pub struct TargetId(u32);
entity_impl!(TargetId, u32, false);
reserved_max_impl!(TargetId, u32);

impl Default for TargetId {
    #[inline(always)]
    fn default() -> Self {
        Self::reserved()
    }
}

//TODO: encapsulate as part of `EntityIx` trait?
impl TargetId {
    /// Create an array of [`TargetId`] from an array of `u32`
    fn from_arr(arr: &[u32]) -> &[TargetId] {
        unsafe { std::mem::transmute(arr) }
    }
    /// Create an array of `u32` from an array of [`TargetId`]
    fn to_arr(arr: &[TargetId]) -> &[u32] {
        unsafe { std::mem::transmute(arr) }
    }
}

/// A node in an `isotope_core` function
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
enum Node {
    // == Values ==
    // === Parameters ===
    /// A function parameter, annotated with it's index.
    ///
    /// This should be a singleton!
    Param(TypeId),

    // === Operations ===
    // /// A function call with a single argument
    // Call1(FunctionId, ValId),
    // /// A function call with a two arguments
    // Call2(FunctionId, ValId, ValId),
    /// A function call with a list of arguments
    Call(FunctionId, ValList),
    /// A tuple, tagged with whether it represents an array
    Tuple(ValList, bool),
    /// An injection into a union type
    Inj(ValId, u32),
    /// The identity morphism applied to another node
    Id(ValId),

    // === Control flow ===
    // /// A theta node with a single block and no arguments
    // Theta10(TargetId, ValList),
    // /// A theta node with a single block and one argument
    // Theta11(TargetId, ValList, ValId),
    // /// A theta node with no arguments
    // Theta0(LabelList),
    // /// A theta node with one argument
    // Theta1(LabelList, ValId),
    // /// A theta node with two arguments
    // Theta2(LabelList, ValId, ValId),
    /// A theta node with a list of arguments
    Theta(LabelList, ValList),
    // /// A jump to a label in a theta node, along with a single argument
    // Jump1(u32, ValId),
    // /// A jump to a label in a theta node, along with two arguments
    // Jump2(u32, ValId, ValId),
    /// A jump to a label in a theta node, along with a list of arguments
    Jump(u32, ValList),
    /// A break instruction
    Break(ValId, TargetId, TargetId),

    // === Constants ===
    /// A small bitvector of length at most 256, with at most 64 nontrivial bits of data
    ///
    /// The unsigned `u8` is the length, and the signed `i8` is the sign bit
    Bits(u8, i8, u64),
    // /// A large bitvector, represented as an array of `u32` limbs.
    // ///
    // /// The unsigned `u32` is the length, and the signed `i8` is the sign bit
    // BigBits(u32, i8, EntityList<u32>),

    // // == Types ==
    // /// The type of bitvectors of length `n`
    // Bitv(u8),
    // /// A product of types
    // ///
    // /// The product of nothing is the unit type
    // Prod(TypeList),
    // /// A union (coproduct) of types
    // ///
    // /// The union of nothing is the empty type
    // Union(TypeList),
    // /// An array of length `n` containing a given type
    // Array(u32, TypeId),

    // == Memory management utilities ==
    /// A free slot for a node
    Free(NodeId),
}

impl Freeable<NodeId> for Node {
    type Value = Node;

    #[inline(always)]
    fn from_slot_value(value: Self::Value) -> Self {
        value
    }

    #[inline(always)]
    fn from_key(key: NodeId) -> Self {
        Node::Free(key)
    }

    #[inline(always)]
    fn into_slot_value(self) -> Self::Value {
        self
    }

    #[inline(always)]
    fn slot_value(&self) -> &Self::Value {
        self
    }

    #[inline(always)]
    fn slot_value_mut(&mut self) -> &mut Self::Value {
        self
    }

    #[inline(always)]
    fn next_free(&self) -> NodeId {
        match self {
            Node::Free(next) => *next,
            node => panic!("node {:?} is not free", node),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn id_func() {
        let mut id = Function::default();
        assert_eq!(id.args(), &[]);
        assert_eq!(id.retv(), ValId::reserved());
        let ty = TypeId::reserved(); //TODO: fix this
        id.add_arg(ty);
        assert_eq!(id.args().len(), 1);
        let param = id.args()[0];
        assert_eq!(id.retv(), ValId::reserved());
        assert_ne!(param, ValId::reserved());
        assert_eq!(id.register_retv(param), ValId::reserved());
        assert_eq!(id.retv(), param);
    }
}
